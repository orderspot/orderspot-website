import React, { Component, createContext } from "react"

const DialogContext = createContext()
const { Provider } = DialogContext

class DialogProvider extends Component {
  state = {
    isModalVisible: false,
  }
  actions = {
    openModal: () => {
      this.setState({
        isModalVisible: true,
      })
    },
    closeModal: () => {
      this.setState({
        isModalVisible: false,
      })
    },
  }
  render() {
    const { state, actions } = this
    const value = { state, actions }
    return <Provider value={value}>{this.props.children}</Provider>
  }
}

export { DialogProvider }
export default DialogContext
