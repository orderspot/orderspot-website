import React, { Component, createContext } from "react"

const AlertDialogContext = createContext()
const { Provider } = AlertDialogContext

const defaultState = {
  isDialogOpen: false,
  dialogText: "We're in progress!",
}

class AlertDialogProvider extends Component {
  state = {
    isDialogOpen: false,
    dialogText: defaultState.dialogText,
  }
  actions = {
    openDialog: ({ dialogText }) => {
      this.setState({
        isDialogOpen: true,
        dialogText
      })
    },
    closeDialog: () => {
      this.setState({
        isDialogOpen: false,
        dialogText: "",
      })
    },
  }

  render() {
    const { state, actions } = this
    const value = { state, actions }
    return <Provider value={value}>{this.props.children}</Provider>
  }
}

export { AlertDialogProvider }
export default AlertDialogContext
