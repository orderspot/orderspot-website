import dataContext from "./dataContext";

const storeReducer = (state, action) => {
  switch (action.type) {
    case "UPDATE_FORM_VALUE":
      return { ...state, storeData: { ...state.storeData, ...action.payload } };
    default:
      return state;
  }
};


const handleSubmit = (dispatch) => (formValues, handleNavigateUser) => {
  dispatch({ type: "UPDATE_FORM_VALUE", payload: formValues });
  handleNavigateUser();
};

export const { Provider, Context } = dataContext(
  storeReducer,
  {
    handleSubmit
  },
  {
    storeData: { restaurantName: "", email: "" }
  }
);
