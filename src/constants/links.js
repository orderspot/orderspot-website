import {
  IN_VENUE_ORDERING,
  PICKUP_DELIVERY,
  MARKETING_INSIGHTS,
  SMALL_SIZED_BUSINESSES,
  MEDIUM_SIZED_ENTERPRISES,
  HOSPITALITY_VENUES,
  MULTI_VENDOR,
  BLOGS,
  FAQS,
  TERMS_OF_USE,
  PRIVACY_POLICY,
} from "./constants"

export const APRIL_CALENDLY_LINK =
  "https://calendly.com/april-orderspot/orderspot-product-demo"
export const JESSICA_CALENDLY_LINK =
  "https://calendly.com/jessica-mazing/orderspot-demo?hide_landing_page_details=1&hide_gdpr_banner=1"

export const productLinks = [
  { id: 1, text: "In-Venue Ordering", url: IN_VENUE_ORDERING, notReady: true },
  { id: 2, text: "Pickup & Delivery", url: PICKUP_DELIVERY, notReady: true },
  {
    id: 3,
    text: "Marketing Insights",
    url: MARKETING_INSIGHTS,
    notReady: true,
  },
]

export const solutionLinks = [
  { id: 4, text: "Small Business", url: SMALL_SIZED_BUSINESSES },
  { id: 5, text: "Medium Sized Enterprises", url: MEDIUM_SIZED_ENTERPRISES },
  { id: 6, text: "Hospitality Venues", url: HOSPITALITY_VENUES },
  { id: 7, text: "Food Halls & Multi-Vendor", url: MULTI_VENDOR },
]

export const resourcesLinks = [
  { id: 8, text: "Blog", url: BLOGS },
  { id: 9, text: "FAQs/How to Use", url: FAQS },
]

export const legalLinks = [
  { id: 10, text: "Terms of Use", url: TERMS_OF_USE },
  { id: 11, text: "Privacy Policy", url: PRIVACY_POLICY },
]

export const ENUM_EXTERNAL_LINKS = {
  FACEBOOK: "https://www.facebook.com/orderspotapp/",
  INSTAGRAM: "https://www.instagram.com/orderspot_app/",
  LINKEDIN: "https://www.linkedin.com/company/orderspotapp",
  TWITTER: "https://twitter.com/orderspotapp",
}
