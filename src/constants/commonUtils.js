import { bgColors } from './constants';

export const handleGetBgColor = (index) => {
  if ( !index || index > 3 )
    return bgColors[Math.floor(Math.random() * 4)];
  
  return bgColors[index];
};
