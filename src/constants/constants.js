//Routes
export const HOME_PAGE = "/"
export const MENU_PREVIEW_PAGE = "/menu-preview"
export const FAQS = "/faqs"
export const BLOGS = "/blog"
export const SMALL_SIZED_BUSINESSES = "/small-sized-businesses"
export const MEDIUM_SIZED_ENTERPRISES = "/medium-sized-enterprises"
export const HOSPITALITY_VENUES = "/hospitality-venues"
export const MULTI_VENDOR = "/multi-vendor"
export const TERMS_OF_USE = "/terms-of-use"
export const PRIVACY_POLICY = "/privacy-policy"
export const IN_VENUE_ORDERING = "/in-venue-ordering"
export const PICKUP_DELIVERY = "/pickup-delivery"
export const MARKETING_INSIGHTS = "/marketing-insights"

//Breakpoints
export const TABLET_PORTRAIT = 991

// CTA Button
export const CTA_TYPO = "Start Free Trial"
export const CTA_TYPO_2 = "Create my store"

// Marketing
export const PHONE_NUM = "323-570-1698";

// etc
export const SIGN_IN_LINK = "https://partner.orderspot.app"
// export const YOUTUBE_LINK = "https://youtu.be/_k7xiXtWWEk"
export const YOUTUBE_LINK = "https://youtu.be/_k7xiXtWWEk"
export const SIGN_UP_LINK = "https://partner.orderspot.app/register/account"
export const FORMSTACK_REGISTRATION_FORM = "https://orderspot.formstack.com/forms/partner_application"
export const GOOGLE_CLIENT_ID =
  "586542505603-1drh099qkem3bijntpofg6u0nkut8ncd.apps.googleusercontent.com"

//Blog categories blocks bg colors
const brownGradientColor =
  "linear-gradient(89.9deg, rgba(232, 166, 124, 0.7) 48.61%, rgba(255, 240, 231, 0.7) 104.07%)"
const yellowGradientColor =
  "linear-gradient(89.9deg, rgba(255, 209, 47, 0.7) 48.61%, rgba(255, 240, 231, 0.7) 104.07%)"
const purpleGradientColor =
  "linear-gradient(89.9deg, rgba(171, 174, 255, 0.7) 48.61%, rgba(255, 240, 231, 0.7) 104.07%)"
const melonGradientColor =
  "linear-gradient(89.9deg, rgba(255, 186, 171, 0.7) 48.61%, rgba(255, 240, 231, 0.7) 104.07%)"
export const bgColors = [
  { background: brownGradientColor, borderColor: "#EBB18C" },
  { background: yellowGradientColor, borderColor: "#FFB61D" },
  { background: purpleGradientColor, borderColor: "#5667FF" },
  { background: melonGradientColor, borderColor: "#F3A696" }
]
