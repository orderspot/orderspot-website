export const ENUM_EVENT_CATEGORIES = {
  HEADER: "Header",
  MAIN_PAGE: "MainPage",
  FOOTER: "Footer",
  SMALL_BUSINESS: "SmallBusiness",
  MEDIUM_ENTERPRISE: "MediumEnterprise",
  HOSPITALITY_VENUE: "HospitalityVenue",
  FOODHALL_MULTI_VENDOR: "FoodHallMultiVendor",
  BLOG: "Blog",
  FAQ: "FAQ",
  MENU_PREVIEW: "MenuPreview",
  PRIVACY_POLICY: "PrivacyPolicy",
  TERMS_OF_USE: "TermsOfUse",
  SCHEDULE_DEMO: "Schedule_Demo",
}

export const ENUM_EVENT_LABELS = {
  MAIN_PAGE: {},
}

export const ENUM_EVENT_ACTIONS = {
  PAGE_ENTER: "PageEnter",
  PAGE_LEAVE: "PageLeave",
  CLICK: "Click",
}

export const ENUM_EVENT_VALUE = {}
