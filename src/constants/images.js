//This file is responsible for importing all the images and export them. so that its easy to change image from one place
//==========  HOME PAGE ===========
//Contactless Dining
// export const girlImg = "https://s3-us-west-2.amazonaws.com/assets.orderspot.app/images/os_website_assets/girl-img.png";
export const boyAndGirlImg = require("../assets/home-page/boy-girl-img.png")
export const girlBgLines =
  "https://s3-us-west-2.amazonaws.com/assets.orderspot.app/images/os_website_assets/lines.png"
export const dineInBgOneMob =
  "https://s3-us-west-2.amazonaws.com/assets.orderspot.app/images/os_website_assets/dine-in-bg-skin.png"
export const dineInBgTwoMob =
  "https://s3-us-west-2.amazonaws.com/assets.orderspot.app/images/os_website_assets/dine-in-bg-2.png"
//restaurants logs section
export const osHorizontalLogoWhite =
  "https://s3-us-west-2.amazonaws.com/assets.orderspot.app/images/os_website_assets/logos/horizontal-logo-white.svg"
// export const osHorizontalLogoBlue =
//   "https://s3-us-west-2.amazonaws.com/assets.orderspot.app/images/os_website_assets/logos/horizontal-logo.svg"
export const osHorizontalLogoBlue = require("../assets/orderspot-logo@2x.png")
export const hcmLogo =
  "https://s3-us-west-2.amazonaws.com/assets.orderspot.app/images/os_website_assets/logos/hcm-logo.png"
export const emcLogo =
  "https://s3-us-west-2.amazonaws.com/assets.orderspot.app/images/os_website_assets/logos/emc-logo.png"
export const firewingsLogo =
  "https://s3-us-west-2.amazonaws.com/assets.orderspot.app/images/os_website_assets/logos/firewings-logo.png"
export const laLogo =
  "https://s3-us-west-2.amazonaws.com/assets.orderspot.app/images/os_website_assets/logos/la-logo.png"
//Features Section
export const orderImg = require("../assets/order-img.png")
export const menuEditImg = require("../assets/menu-edit-img.png")
export const womanImg = require("../assets/woman-img.png")
export const blockOneDotImg = require("../assets/block-1-dot.svg")
export const blockTwoDotImg = require("../assets/block-2-dot.svg")
export const blockThreeDotImg = require("../assets/block-3-dot.svg")
//Customer Reviews
export const customer1Img = require("../assets/temp/customer-1-img.png")
export const customer2Img = require("../assets/temp/customer-2-img.png")
//OrderType Section
export const contactLessMenuImg =
  "https://s3-us-west-2.amazonaws.com/assets.orderspot.app/images/os_website_assets/contact-less-menu-img.png"
export const seamLessImg =
  "https://s3-us-west-2.amazonaws.com/assets.orderspot.app/images/os_website_assets/seam-less-dive-in-img.png"
export const nextGenImg =
  "https://s3-us-west-2.amazonaws.com/assets.orderspot.app/images/os_website_assets/menu-edit-img.png"
export const itemWithCart =
  "https://s3-us-west-2.amazonaws.com/assets.orderspot.app/images/os_website_assets/item-with-cart-img.svg"
export const appleWithGooglePayBtn = require("../assets/home-page/pickup-and-delivery-item1@2x.png")
export const orderTypeMenuEditImg =
  "https://s3-us-west-2.amazonaws.com/assets.orderspot.app/images/os_website_assets/menu-edit-img-new.svg"
//WeDoEverything Section
export const endToEnd = require("../assets/end-to-end-img.svg")
export const highQalityImg = require("../assets/high-quality-mobile-order-img.svg")
export const dedicatedImg = require("../assets/dedicated-account-img.svg")
//Item included Section
export const tableQrSingleIconBlack = require("../assets/icons/table-qr-sinage-icon-black.png")
export const tableQrSingleIconWhite = require("../assets/icons/table-qr-sinage-icon-white.png")
export const qrCodeKioskBlackIcon = require("../assets/icons/qr-code-kiosk-black.png")
export const qrCodeKioskWhiteIcon = require("../assets/icons/qr-code-kiosk-white.png")
export const orderManagementTabletIconBlack = require("../assets/icons/order-management-tablet-black.png")
export const orderManagementTabletIconWhite = require("../assets/icons/order-management-tablet-white.png")
export const kitchenPrinterBlackIcon = require("../assets/icons/kitchen-printer-icon-black.png")
export const kitchenPrinterWhiteIcon = require("../assets/icons/kitchen-printer-icon-white.png")
export const tableQRImage =
  "https://s3-us-west-2.amazonaws.com/assets.orderspot.app/images/os_website_assets/tabletop-qr-signage-complimentary-1.png"
export const stickerQRImage =
  "https://s3-us-west-2.amazonaws.com/assets.orderspot.app/images/os_website_assets/tabletop-qr-signage-complimentary-3.png"
export const standQRImage =
  "https://s3-us-west-2.amazonaws.com/assets.orderspot.app/images/os_website_assets/tabletop-qr-signage-complimentary-2.png"
export const printerPurchaseImg =
  "https://s3-us-west-2.amazonaws.com/assets.orderspot.app/images/os_website_assets/kitchen-printer-for-purchase-img.png"
export const printerActivationImg =
  "https://s3-us-west-2.amazonaws.com/assets.orderspot.app/images/os_website_assets/kitchen-printer-for-activation-img.png"
export const orderTabletImg =
  "https://s3-us-west-2.amazonaws.com/assets.orderspot.app/images/os_website_assets/order-management-tablet-img.png"
export const dot1 = require("../assets/item-include-top-left.svg")
export const dot2 = require("../assets/item-include-top-right.svg")
export const dot3 = require("../assets/item-include-top-right.svg")
export const arrowImg = require("../assets/icons/arrow-right-white-icon.svg")
//How to get started section
export const signupFormImg =
  "https://s3-us-west-2.amazonaws.com/assets.orderspot.app/images/os_website_assets/signup-form-img.png"
export const signupFormImgMob =
  "https://s3-us-west-2.amazonaws.com/assets.orderspot.app/images/os_website_assets/sign-up-form-img-mob.png"
export const selectPlanImg =
  "https://s3-us-west-2.amazonaws.com/assets.orderspot.app/images/os_website_assets/pricing-img.png"
export const osBoxImg =
  "https://s3-us-west-2.amazonaws.com/assets.orderspot.app/images/os_website_assets/orderspot-box-img.png"
//Signup form section
export const signupFormPcImage = require("../assets/home-page/bg-for-main-page@2x.png")
export const signupFormDotsTopTablet = require("../assets/sign-up-form-dots-top-tablet.svg")
export const signupFormDotsBottomTablet = require("../assets/sign-up-form-dots-bottom-tablet.svg")
//========== Menu Preview Page ==========
export const storeLoadingLoader =
  "https://s3-us-west-2.amazonaws.com/assets.orderspot.app/images/os_website_assets/page-loader.gif"
export const menuPreviewPageHeaderBgImg = require("../assets/temp/bg.jpg")
export const foodImg1 = require("../assets/temp/food-1.png")
export const foodImg2 = require("../assets/temp/food-2.png")
export const foodImg3 = require("../assets/temp/food-3.png")
export const foodImg4 = require("../assets/temp/food-4.png")
// ========== Blogs Page ==========
export const blogHeaderBgImg = require("../assets/blog-header-img.png")
// ========== Faqs Page ==========
export const faqHeaderBgImg = require("../assets/temp/faqs-header-bg-img.png")
// ========== Small/Medium Sized Businesses ==========
export const restaurantAndBarsImg = require("../assets/small-medium-sized-businesses/small-medium-sized-businesses-img.png")
export const hexagon = require("../assets/small-medium-sized-businesses/hexagon.svg")
export const dots = require("../assets/small-medium-sized-businesses/dots.svg")
export const circleSimple = require("../assets/small-medium-sized-businesses/circle-simple.svg")
export const circleGreen = require("../assets/small-medium-sized-businesses/circle-green.svg")
export const personImage = require("../assets/small-medium-sized-businesses/person-img.png")
export const SMALL_MEDIUM_SIZED_ASSET = {
  featureImg1: require("../assets/small-medium-sized-businesses/feature-1.png"),
  featureImg2: require("../assets/small-medium-sized-businesses/feature-2.png"),
  featureImg3: require("../assets/small-medium-sized-businesses/feature-3.png"),
  mediumFeatureImg3: require("../assets/small-medium-sized-businesses/medium-feature3.png"),
  slideImgPc1: require("../assets/small-medium-sized-businesses/small-medium-slide-pc-1@2x.png"),
  slideImgT1: require("../assets/small-medium-sized-businesses/small-medium-slide-t-1@2x.png"),
  slideImgM1: require("../assets/small-medium-sized-businesses/small-medium-slide-m-1@3x.png"),
  slideImgPc2: require("../assets/small-medium-sized-businesses/small-medium-slide-pc-2@2x.png"),
  slideImgT2: require("../assets/small-medium-sized-businesses/small-medium-slide-t-2@2x.png"),
  slideImgM2: require("../assets/small-medium-sized-businesses/small-medium-slide-m-2@3x.png"),
  slideImgPc3: require("../assets/small-medium-sized-businesses/small-medium-slide-pc-3@2x.png"),
  slideImgT3: require("../assets/small-medium-sized-businesses/small-medium-slide-t-3@2x.png"),
  slideImgM3: require("../assets/small-medium-sized-businesses/small-medium-slide-m-3@3x.png"),
  topImagePc: require("../assets/solution/medium-sized-top-pc@2x.png"),
  topImageT: require("../assets/solution/medium-sized-top-t@2x.png"),
  topImageM: require("../assets/solution/medium-sized-top-m@3x.png"),
}
// ========== Hospitality venues ===========
export const HOSPITALITY_VENUES_ASSET = {
  slideImage1Pc: require("../assets/solution/textAndImageSection/hospitality-slide-1-pc@2x.png"),
  slideImage2Pc: require("../assets/solution/textAndImageSection/hospitality-slide-2-pc@2x.png"),
  slideImage3Pc: require("../assets/solution/textAndImageSection/hospitality-slide-3-pc@2x.png"),
  slideImage1T: require("../assets/solution/textAndImageSection/hospitality-slide-1-t@2x.png"),
  slideImage2T: require("../assets/solution/textAndImageSection/hospitality-slide-2-t@2x.png"),
  slideImage3T: require("../assets/solution/textAndImageSection/hospitality-slide-3-t@2x.png"),
  slideImage1M: require("../assets/solution/textAndImageSection/hospitality-slide-1-m@3x.png"),
  slideImage2M: require("../assets/solution/textAndImageSection/hospitality-slide-2-m@3x.png"),
  slideImage3M: require("../assets/solution/textAndImageSection/hospitality-slide-3-m@3x.png"),
  topImagePc: require("../assets/solution/hospitality-venues-pc@2x.png"),
  topImageT: require("../assets/solution/hospitality-venues-t@2x.png"),
  topImageM: require("../assets/solution/hospitality-venues-m@3x.png"),
  featureImg1: require("../assets/solution/feature-hospitality-pc-1@2x.png"),
  featureImg2: require("../assets/solution/feature-hospitality-pc-2@2x.png"),
  featureImg3: require("../assets/solution/feature-hospitality-pc-3@2x.png"),
}
// ========== FoodHall & Multi vendor =========
export const MULTI_VENDOR_ASSET = {
  slideImage1Pc: require("../assets/solution/textAndImageSection/multi-vendor-slide-pc-1@2x.png"),
  slideImage2Pc: require("../assets/solution/textAndImageSection/multi-vendor-slide-pc-2@2x.png"),
  slideImage3Pc: require("../assets/solution/textAndImageSection/multi-vendor-slide-pc-3@2x.png"),
  slideImage1T: require("../assets/solution/textAndImageSection/multi-vendor-slide-t-1@2x.png"),
  slideImage2T: require("../assets/solution/textAndImageSection/multi-vendor-slide-t-2@2x.png"),
  slideImage3T: require("../assets/solution/textAndImageSection/multi-vendor-slide-t-3@2x.png"),
  slideImage1M: require("../assets/solution/textAndImageSection/multi-vendor-slide-m-1@3x.png"),
  slideImage2M: require("../assets/solution/textAndImageSection/multi-vendor-slide-m-2@3x.png"),
  slideImage3M: require("../assets/solution/textAndImageSection/multi-vendor-slide-m-3@3x.png"),
  topImagePc: require("../assets/solution/multi-vendor-top-pc@2x.png"),
  topImageT: require("../assets/solution/multi-vendor-top-t@2x.png"),
  topImageM: require("../assets/solution/multi-vendor-top-m@3x.png"),
  featureImg1: require("../assets/solution/feature-multi-vendor-pc-1@2x.png"),
  featureImg2: require("../assets/solution/feature-multi-vendor-pc-2@2x.png"),
  featureImg3: require("../assets/solution/feature-multi-vendor-pc-3@2x.png"),
}
// ========== Common ===========
export const restaurantImg =
  "https://s3-us-west-2.amazonaws.com/assets.orderspot.app/images/os_website_assets/restaurant-img.png"
