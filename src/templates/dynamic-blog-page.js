import React from "react"
import Layout from "../components/Layout"
import BlogTop from "../components/BlogPage/BlogTop"
import BlogGetStartedComponent from "../components/BlogPage/BlogGetStartedComponent"
import { graphql } from "gatsby"
import _get from "lodash/get"
import { documentToReactComponents } from "@contentful/rich-text-react-renderer"
import "./styles/styles.scss"
import SEO from "../components/common/seo"

const BlogPost = ({ data }) => {
  const contentfulBlogPost = _get(data, "contentfulBlogpost", {})
  const {
    mainTitle,
    subTitleBlack,
    publishDate,
    featuredImage,
    excerpt,
    body,
    title
  } = contentfulBlogPost
  const blogImg = _get(featuredImage, "fluid", data.qrBgImageFile.qrBgImage.fluid)
  const blogData = {
    featuredImage: blogImg,
    mainTitle,
    subTitleBlack,
    publishDate,
    excerpt,
    title
  }
  const options = {
    renderNode: {
      "embedded-asset-block": node => {
        const imgObj = _get(node, `data.target.fields.file["en-US"]`, {})
        const alt = _get(node, `data.target.fields.title["en-US"]`, "")
        const url = imgObj.url
        return <img className="blog-img" alt={alt} src={url} />
      }
    }
  }
  
  return (
    <Layout className="blog" isHeaderFixed>
      <SEO title={title} image={blogImg} description={excerpt} />
      <BlogTop {...blogData} />
      <div className="container content-wrapper">
        {documentToReactComponents(body.json, options)}
      </div>
      <BlogGetStartedComponent />
    </Layout>
  )
}

export const query = graphql`
  query($slug: String!) {
    contentfulBlogpost(slug: { eq: $slug }) {
      title
      id
      slug
      subTitleBlack
      publishDate
      excerpt
      featuredImage {
            fluid {
            base64
            sizes
            aspectRatio
            srcSet
            src
          }
          }
      body {
        json
      }
    }
   
    qrBgImageFile:file(relativePath: {eq: "temp/bg.jpg"}){
      qrBgImage:childImageSharp{
        fluid{
          ...GatsbyImageSharpFluid
        }
      }
    }
  }
`

export default BlogPost
