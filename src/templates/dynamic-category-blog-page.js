import React from "react"
import Layout from "../components/Layout"
import BlogTItleBlock from "../components/BlogsPage/BlogTItleBlock"
import BlogSingle from "../components/BlogsPage/BlogSingle"
import { handleGetBgColor } from "../constants/commonUtils"
import "./styles/dynamic-category-blog-page.scss"

const BlogCategoryPage = props => {
  const { pageContext } = props
  return (
    <Layout className="category-blogs-page" isHeaderFixed>
      <div className="category-blogs-page--contennt-wrapper container">
        <BlogTItleBlock
          style={handleGetBgColor()}
          title={pageContext.categoryTitle}
          text={pageContext.categoryText}
        />
        <div className="blogs-wrapper row">
          {pageContext.blogs
            .filter(blog => blog.node.category.slug === pageContext.slug)
            .map(post => (
              <BlogSingle key={post.node.slug} post={post} />
            ))}
        </div>
      </div>
    </Layout>
  )
}

export default BlogCategoryPage
