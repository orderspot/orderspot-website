import React from "react"
import PropTypes from "prop-types"
import styled from "styled-components"
import ScheduleDemo from "./ScheduleDemo"
import ImageWithScheduleImageComponent from "./ImageWithScheduleImageComponent"

const ImageWithSchedule = (props) => {
  const {
    title,
    textOptions,
    padding,
    margin,
    imageOptions,
    scheduleDemoOptions
  } = props
  return (
    <ImageWithScheduleContainer className="container">
      <ContentWrapper
        margin={margin}
        padding={padding}
        imageOptions={imageOptions}
      >
        <Title>{title}</Title>
        <ImageWithScheduleImageComponent imageOptions={imageOptions} />
        <Text textOptions={textOptions}>{textOptions.text}</Text>
        <ScheduleDemoWrapper scheduleDemoOptions={scheduleDemoOptions}>
          <ScheduleDemo isSmall />
        </ScheduleDemoWrapper>
      </ContentWrapper>
    </ImageWithScheduleContainer>
  )
}

const ImageWithScheduleContainer = styled.article``

const ContentWrapper = styled.div`
  position: relative;

  @media screen and (max-width: 767px) {
    text-align: center;
    margin-top: ${props => props.margin.m.top};
    margin-bottom: ${props => props.margin.m.bottom};
  }

  @media screen and (min-width: 768px) and (max-width: 1199px) {
    padding-top: ${props => props.padding.t.top};
    padding-bottom: ${props => props.padding.t.bottom};
    margin-top: ${props => props.margin.t.top};
    margin-bottom: ${props => props.margin.t.bottom};
    padding-right: ${props => {
  return (
    parseFloat(props.imageOptions.t.width) +
    parseFloat(props.imageOptions.t.marginLeft)
  )
}}rem;
  }

  @media screen and (min-width: 1200px) {
    padding-top: ${props => props.padding.pc.top};
    padding-bottom: ${props => props.padding.pc.bottom};
    margin-top: ${props => props.margin.pc.top};
    margin-bottom: ${props => props.margin.pc.bottom};
    padding-right: ${props => {
  return (
    parseFloat(props.imageOptions.pc.width) +
    parseFloat(props.imageOptions.pc.marginLeft)
  )
}}rem;
  }
`

const Title = styled.h1`
  color: #2c2c46;
  white-space: pre-line;
  font-weight: bold;

  @media screen and (max-width: 767px) {
    font-size: 2.6rem;
  }

  @media screen and (min-width: 768px) and (max-width: 1199px) {
    font-size: 4.2rem;
  }

  @media screen and (min-width: 1200px) {
    font-size: 4.8rem;
  }
`
const Text = styled.p`
  margin: 0;
  font-weight: 300;
  letter-spacing: 0.02px;
  color: #68687b;

  @media screen and (max-width: 767px) {
    font-size: 1.3rem;
  }

  @media screen and (min-width: 768px) and (max-width: 1199px) {
    margin-top: ${props => props.textOptions.t.marginTop}rem;
    font-size: 1.7rem;
  }

  @media screen and (min-width: 1200px) {
    margin-top: ${props => props.textOptions.pc.marginTop}rem;
    font-size: 1.9rem;
  }
`

const ScheduleDemoWrapper = styled.div`
  @media screen and (max-width: 767px) {
    margin-top: ${props => props.scheduleDemoOptions.m.marginTop}rem;
  }
  @media screen and (min-width: 768px) and (max-width: 1199px) {
    margin-top: ${props => props.scheduleDemoOptions.t.marginTop}rem;
    max-width: ${props => props.scheduleDemoOptions.t.maxWidth}rem;
  }
  @media screen and (min-width: 1200px) {
    margin-top: ${props => props.scheduleDemoOptions.pc.marginTop}rem;
  }
`

ImageWithSchedule.propTypes = {
  title: PropTypes.string.isRequired,
  padding: PropTypes.object.isRequired,
  textMarginTop: PropTypes.string,
  formMarginTop: PropTypes.string,
  imageStyleInfo: PropTypes.object
}

export default ImageWithSchedule
