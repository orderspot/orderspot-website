import React from "react"
import styled from "styled-components"
import ResponsiveStyled from "../../utils/ResponsiveStyledComponent"
import { useStaticQuery, graphql } from "gatsby"
import { useMediaQuery } from "react-responsive"
import Img from "gatsby-image"

const MultiVendorPartnerSays = () => {
  const data = useStaticQuery(query)
  const PartnerImagePc = data.partnerImgDFile.partnerImgD.fluid
  const PartnerImageT = data.partnerImgTFile.partnerImgT.fluid
  const PartnerImageM = data.partnerImgMFile.partnerImgM.fluid
  
  const isPc = useMediaQuery({
    minWidth: 1200
  })
  const isTablet = useMediaQuery({
    minWidth: 768,
    maxWidth: 1199
  })
  
  return (
    <CustomContainer className="container">
      <PartnerSaysContainer>
        <Title>What our Partners Say</Title>
        <ImageContainer>
          <Img
            fluid={isPc ? PartnerImagePc : isTablet ? PartnerImageT : PartnerImageM}
            alt="our-partner"
          />
        </ImageContainer>
        <TextContainer>
          <Text>
            Orderspot has been absolutely amazing for my restaurant and my business. They are very easy to work with and
            very accommodating if issues arise. They also have been able to keep my customers and employees safe during
            this tough time. The increase in sales for my business is just the cherry on top. I highly recommend
            Orderspot because of their professionalism and their pursuit to make their service seamless with your
            operation.
          </Text>
          <PartnerName>Haven City Market</PartnerName>
        </TextContainer>
        <PercentageContainer>
          <PercentageWrapper>
            <Percentage>23k</Percentage>
            <PercentageText>
              Number of monthly in-person scans and menu views
            </PercentageText>
          </PercentageWrapper>
          
          <PercentageWrapper>
            <Percentage>$11k</Percentage>
            <PercentageText>
              Average monthly contactless revenue with Orderspot
            </PercentageText>
          </PercentageWrapper>
        </PercentageContainer>
      </PartnerSaysContainer>
    </CustomContainer>
  )
}

const query = graphql`
  query{
    partnerImgDFile:file(relativePath: {eq: "solution/multi-vendor-partner-says-pc@2x.png"}){
      partnerImgD:childImageSharp{
        fluid{
          ...GatsbyImageSharpFluid
        }
      }
    }
    partnerImgTFile:file(relativePath: {eq: "solution/multi-vendor-partner-says-t@2x.png"}){
      partnerImgT:childImageSharp{
        fluid{
          ...GatsbyImageSharpFluid
        }
      }
    }
    partnerImgMFile:file(relativePath: {eq: "solution/multi-vendor-partner-says-m@3x.png"}){
      partnerImgM:childImageSharp{
        fluid{
          ...GatsbyImageSharpFluid
        }
      }
    }
  }
`

const CustomContainer = styled.div`
  @media screen and (max-width: 767px) {
    padding-top: 8rem;
    padding-bottom: 12.2rem;
  }
  @media screen and (min-width: 768px) and (max-width: 1199px) {
    padding-top: 12rem;
    padding-bottom: 17rem;
  }
  @media screen and (min-width: 1200px) {
    padding-top: 15rem;
    padding-bottom: 13rem;
  }
`

const PartnerSaysContainer = ResponsiveStyled({
  elem: "article",
  common: {
    position: "relative"
  }
})

const Title = ResponsiveStyled({
  elem: "h1",
  common: {
    "font-weight": "bold",
    color: "#2c2c46",
    "letter-spacing": "0.23px"
  },
  mobile: {
    "text-align": "center",
    "font-size": "2rem"
  },
  tablet: {
    "font-size": "3.8rem"
  },
  pc: {
    "font-size": "4.5rem"
  }
})

const ImageContainer = ResponsiveStyled({
  elem: "div",
  common: {
    "z-index": "-1"
  },
  mobile: {
    display: "block",
    "max-width": "84%",
    margin: "3.2rem auto 0"
  },
  tablet: {
    position: "absolute",
    width: "39.6rem",
    top: "-3.3rem",
    right: 0
  },
  pc: {
    position: "absolute",
    width: "55.8rem",
    right: 0,
    top: "-1.8rem"
  }
})

const TextContainer = ResponsiveStyled({
  elem: "div",
  common: {
    "background-color": "#2c2c46"
  },
  mobile: {
    "background-color": "transparent",
    "margin-top": "2rem"
  },
  tablet: {
    "margin-top": "3.6rem",
    "max-width": "57rem",
    padding: "4.5rem 3.8rem"
  },
  pc: {
    "margin-top": "3rem",
    "max-width": "73.5rem",
    padding: "6.8rem 7.4rem"
  }
})

const Text = ResponsiveStyled({
  elem: "p",
  common: {
    "font-weight": "300",
    "line-height": "1.4",
    color: "#fdfdfd",
    "margin-bottom": 0
  },
  mobile: {
    color: "#2c2c46",
    "font-size": "1.3rem",
    "text-align": "center"
  },
  tablet: {
    "font-size": "1.7rem"
  },
  pc: {
    "font-size": "1.9rem"
  }
})

const PartnerName = ResponsiveStyled({
  elem: "span",
  common: {
    display: "block",
    color: "#fdfdfd",
    "font-weight": "bold"
  },
  mobile: {
    color: "#2c2c46",
    "margin-top": "1.5rem",
    "font-size": "1.3rem",
    "text-align": "center"
  },
  tablet: {
    "margin-top": "2.3rem",
    "font-size": "1.7rem"
  },
  pc: {
    "margin-top": "4rem",
    "font-size": "1.9rem"
  }
})

const PercentageContainer = ResponsiveStyled({
  elem: "div",
  common: {
    "font-size": 0
  },
  mobile: {
    display: "none"
  },
  tablet: {
    "text-align": "center",
    margin: "10rem auto 0"
  },
  pc: {
    "text-align": "left",
    margin: "4rem auto 0"
  }
})

const PercentageWrapper = ResponsiveStyled({
  elem: "div",
  common: {
    display: "inline-block",
    "box-sizing": "content-box",
    "text-align": "center"
  },
  mobile: {
    display: "none"
  },
  tablet: {
    padding: "0 1.6rem",
    "max-width": "29rem"
  },
  pc: {
    padding: "0 1.6rem",
    "max-width": "29rem"
  }
})

const Percentage = ResponsiveStyled({
  elem: "span",
  common: {
    display: "block",
    margin: "0 auto",
    "font-weight": "600",
    "line-height": "1.2"
  },
  tablet: {
    "font-size": "4rem"
  },
  pc: {
    "font-size": "4rem"
  }
})
const PercentageText = ResponsiveStyled({
  elem: "p",
  common: {
    "margin-top": "1.2rem",
    "margin-bottom": 0,
    "line-height": "1.24",
    "font-weight": 300
  },
  tablet: {
    "font-size": "1.8rem"
  },
  pc: {
    "font-size": "1.8rem"
  }
})

export default MultiVendorPartnerSays
