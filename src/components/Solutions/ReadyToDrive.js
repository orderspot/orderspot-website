import React from "react"
import PropTypes from "prop-types"
import styled from "styled-components"

import ScheduleDemo from "./ScheduleDemo"

const ReadyToDrive = () => {
  return (
    <ReadyToDriveContainer>
      <FlexContainer className="container">
        <TextWrapper>
          <ReadyToDriveHead>
            Ready to drive sales with delightful guest experiences?
          </ReadyToDriveHead>
          <ReadyToDriveText>
            Leave us your email and our hospitality specialist will be in touch
            to answer your questions!
          </ReadyToDriveText>
        </TextWrapper>
        <ScheduleWrapper>
          <ScheduleDemo />
        </ScheduleWrapper>
      </FlexContainer>
    </ReadyToDriveContainer>
  )
}

const FlexContainer = styled.div`
  @media screen and (min-width: 1200px) {
    display: flex;
    align-items: center;
    justify-content: space-between;
  }
`

const ReadyToDriveContainer = styled.article`
  position: relative;
  width: 100%;
  background-color: #f6dece;
  background-repeat: no-repeat;
  @media screen and (max-width: 767px) {
    background-color: transparent;
    margin-bottom: 14.8rem;
  }
  @media screen and (min-width: 768px) and (max-width: 1199px) {
    text-align: center;
    padding: 9.2rem 0;
    margin-bottom: 12.8rem;
    background-size: 19.1rem;
    background-position: top left 7.3rem;
  }
  @media screen and (min-width: 1200px) {
    padding: 15.2rem 0;
    margin-bottom: 14.8rem;
    background-size: 22.3rem;
    background-position: top left 10.5rem;
  }
`

const TextWrapper = styled.div`
  @media screen and (max-width: 767px) {
    text-align: center;
  }
  @media screen and (min-width: 768px) and (max-width: 1199px) {
    margin: 0 auto;
  }

  @media screen and (min-width: 1200px) {
    width: 48.6rem;
  }
`
const ReadyToDriveHead = styled.h1`
  display: inline-block;
  font-size: 3.4rem;
  font-weight: bold;
  color: #2c2c46;

  @media screen and (max-width: 767px) {
    margin: 0 auto;
    text-align: center;
    font-size: 2rem;
  }

  @media screen and (min-width: 768px) and (max-width: 1199px) {
    width: 66.5rem;
  }

  @media screen and (min-width: 1200px) {
  }
`
const ReadyToDriveText = styled.p`
  margin: 0;
  display: inline-block;
  font-size: 1.6rem;
  font-weight: 500;
  color: #2c2c46;
  margin-top: 1.9rem;
  @media screen and (max-width: 767px) {
    margin: 0 auto;
    margin-top: 0.6rem;
    font-size: 1.3rem;
  }
`

const ScheduleWrapper = styled.div`
  @media screen and (max-width: 767px) {
    margin-top: 1.6rem;
  }
  @media screen and (min-width: 768px) and (max-width: 1199px) {
    margin-top: 2.8rem;
  }
  @media screen and (min-width: 1200px) {
    flex: 1;
    max-width: 72rem;
    margin-left: 2.6rem;
  }
`

export default ReadyToDrive
