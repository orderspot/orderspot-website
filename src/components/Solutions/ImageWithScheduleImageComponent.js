import React from "react"
import Img from "gatsby-image"

const ImageWithScheduleImageComponent = ({ imageOptions }) => {
  return (
    <section className="image-with-schedule-image-component">
      <div style={{
        width: `${imageOptions.pc.width}rem`,
        height: `${imageOptions.pc.height}rem`
      }} className="image-with-schedule--d-img">
        <Img fluid={imageOptions.pc.src} />
      </div>
      
      <div style={{
        width: `${imageOptions.t.width}rem`,
        height: `${imageOptions.t.height}rem`
      }} className="image-with-schedule--t-img">
        <Img fluid={imageOptions.t.src} />
      </div>
      
      <div style={{
        marginTop: imageOptions.m.marginTop,
        marginBottom: imageOptions.m.marginBottom
      }} className="image-with-schedule--m-img">
        <Img fluid={imageOptions.m.src} />
      </div>
    </section>
  )
}

ImageWithScheduleImageComponent.propTypes = {}

export default ImageWithScheduleImageComponent
