import React, { useState } from "react"
import PropTypes from "prop-types"
import styled from "styled-components"

import Input from "../common/Input/Input"
import Button from "../common/Button/Button"

import { JESSICA_CALENDLY_LINK } from "../../constants/links"
import { gtagPluginEventTracker } from "../../utils/googleAnalyticsHelper"
import {
  ENUM_EVENT_CATEGORIES,
  ENUM_EVENT_ACTIONS,
} from "../../constants/enumEvents"

import { PopupText, openPopupWidget } from "react-calendly"

const pageSettings = {
  backgroundColor: "ffffff",
  hideEventTypeDetails: false,
  hideLandingPageDetails: false,
  primaryColor: "00a2ff",
  textColor: "4d5055",
}

const ScheduleDemo = ({ isSmall }) => {
  const [emailInput, setEmailInput] = useState("")
  const handleInput = e => {
    const { value } = e.target
    setEmailInput(value)
  }
  const openCalendly = e => {
    e.preventDefault()
    gtagPluginEventTracker({
      category: ENUM_EVENT_CATEGORIES.SCHEDULE_DEMO,
      action: ENUM_EVENT_ACTIONS.CLICK,
      label: `${ENUM_EVENT_CATEGORIES.SCHEDULE_DEMO}_${emailInput}`,
    })
    openPopupWidget({
      prefill: {
        email: emailInput,
      },
      url: JESSICA_CALENDLY_LINK,
      pageSettings,
    })
    if (process.env.IS_GA_RUNNING == "false") return
    // window.gtag_report_conversion(`?${emailInput}`)
  }
  return (
    <ScheduleDemoWrapper
      className="schedule-demo-wrapper"
      onSubmit={openCalendly}
    >
      <Input
        isSmall={isSmall}
        value={emailInput}
        onChange={handleInput}
        type={"email"}
        placeholder={"Enter your email"}
        isHotjarAllow
        isRequired
      />
      <CustomButton type="submit" isSmall={isSmall} text="Schedule Demo" />

      <PopupText
        prefill={{
          email: "test@test.com",
          name: "Jon Snow",
        }}
      />
    </ScheduleDemoWrapper>
  )
}

const ScheduleDemoWrapper = styled.form`
  @media screen and (min-width: 768px) and (max-width: 1199px) {
    max-width: 67.3rem;
    margin: 0 auto;
  }
  @media screen and (min-width: 1200px) {
    display: flex;
    align-items: center;
    justify-content: center;
  }
`

const CustomButton = styled(Button)`
  @media screen and (max-width: 767px) {
    width: 100%;
    height: 5rem;
    line-height: 5rem;
    margin-top: 1.5rem;
  }
  @media screen and (min-width: 768px) and (max-width: 1199px) {
    width: 100%;
    height: 5.6rem;
    line-height: 5.6rem;
    margin-top: 1rem;
  }
  @media screen and (min-width: 1200px) {
    height: ${props => (props.isSmall ? "5.6rem" : "6.4rem")};
    line-height: ${props => (props.isSmall ? "5.6rem" : "6.4rem")};
    flex-basis: 31%;
    margin-left: 1.3rem;
    min-width: 15.2rem;
  }
`

ScheduleDemo.propTypes = {
  isSmall: PropTypes.bool,
}
ScheduleDemo.defaultProps = {
  isSmall: false,
}

export default ScheduleDemo
