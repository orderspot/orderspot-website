import React, { useContext } from "react"
import { GoogleLogin, GoogleLogout } from "react-google-login"
import {
  CTA_TYPO_2,
  FORMSTACK_REGISTRATION_FORM,
  GOOGLE_CLIENT_ID,
} from "../../constants/constants"
import Text from "../common/Text/Text"
import googleIcon from "../../assets/icons/google-icon.png"
import styled from "styled-components"
import LoginForm from "../SmallMediumSizedBusinessesPage/LoginForm"
import PropTypes from "prop-types"
import AlertDialogContext from "../../context/AlertDialogContext"
import Button from "../common/Button/Button"
import scrollTo from "gatsby-plugin-smoothscroll"

const LoginFormAndLoginGoogle = ({ title, subText }) => {
  const useAlert = useContext(AlertDialogContext) || {}
  const openDialog = useAlert.actions ? useAlert.actions.openDialog : {}
  const isSignupForm = true

  const googleOnSuccess = res => {
    console.log("on succuess", res)
    openDialog({ dialogText: "We're in progress!" })
  }
  const googleOnFailure = res => {
    console.log("on failure", res)
  }

  const moveToPartner = () => {
    if (window.location.pathname === "/") {
      scrollTo("#formstackEmbedSection")
    } else {
      window.location.href = window.location.origin + "#formstackEmbedSection"
    }
    // window.open(FORMSTACK_REGISTRATION_FORM, "_blank")
  }

  return (
    <section className="login-form-and-login-google">
      <Text
        className="login-form-and-login-google--title"
        align="center"
        variant={"title4"}
        color="indigo-color"
      >
        {title}
      </Text>
      <Text
        className="login-form-and-login-google--text"
        align="center"
        variant={"body4"}
        color="indigo-color"
      >
        {subText}
      </Text>

      {/* TODO::need to implement signup as well based on the prop passed. */}
      {/* <FormWrapper className="login-form-and-login-google--wrapper">
        <GoogleLogin
          clientId={GOOGLE_CLIENT_ID}
          render={renderProps => (
            <button
              onClick={renderProps.onClick}
              className="login-with-google-btn"
            >
              <img src={googleIcon} alt="google" className="google-icon" />
              <Text className="w-700" variant="body6">
                {isSignupForm ? "Sign up with Google" : "Log in with Google"}
              </Text>
            </button>
          )}
          buttonText="Login"
          onSuccess={googleOnSuccess}
          onFailure={googleOnFailure}
          cookiePolicy={"single_host_origin"}
        />

        <LoginFormOrText
          align="center"
          variant="body4"
          color="indigo-3"
          className="login-form-or-text w-500"
        >
          or
        </LoginFormOrText>

        <CreateStoreFormWrapper>
          <LoginForm
            notReadyFunc={() => {
              openDialog({ dialogText: "We're in progress!" })
            }}
            isSignupForm={isSignupForm}
          />
        </CreateStoreFormWrapper>
      </FormWrapper> */}
      <Button
        onClick={moveToPartner}
        className="full-width-btn temporary-btn create-store-btn"
        text={CTA_TYPO_2}
        type="button"
      />
    </section>
  )
}

const FormWrapper = styled.div`
  max-width: 56rem;
  margin: 6.8rem auto 0;
`

const LoginFormOrText = styled(Text)`
  margin-top: 3rem;
  margin-bottom: 4.4rem;
`

const CreateStoreFormWrapper = styled.div``

LoginFormAndLoginGoogle.propTypes = {
  title: PropTypes.string.isRequired,
  subText: PropTypes.string,
}

LoginFormAndLoginGoogle.defaultProps = {
  title: "Drive sales with seamless mobile ordering experiences",
  subText: "Get your restaurant online in just 5 minutes, no payment required",
}

export default LoginFormAndLoginGoogle
