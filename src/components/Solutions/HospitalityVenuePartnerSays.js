import React from "react"
import ResponsiveStyled from "../../utils/ResponsiveStyledComponent"
import { useMediaQuery } from "react-responsive"
import Img from "gatsby-image"

const HospitalityVenuePartnerSays = (props) => {
  const { PartnerImagePc, PartnerImageT, PartnerImageM } = props
  const isPc = useMediaQuery({
    minWidth: 1200
  })
  const isTablet = useMediaQuery({
    minWidth: 768,
    maxWidth: 1199
  })
  
  return (
    <div className="container">
      <PartnerSaysContainer>
        <Title>What our Partners Say</Title>
        <ContentContainer>
          <ImageWrapper>
            <Img
              fluid={
                isPc ? PartnerImagePc : isTablet ? PartnerImageT : PartnerImageM
              }
              alt="Partner Image"
            />
          </ImageWrapper>
          <PartnerText>
            Orderspot has been absolutely amazing for my restaurant and my business. They are very easy to work with and
            very accommodating if issues arise. They also have been able to keep my customers and employees safe during
            this tough time. The increase in sales for my business is just the cherry on top. I highly recommend
            Orderspot because of their professionalism and their pursuit to make their service seamless with your
            operation.
          </PartnerText>
          <PartnerName>Andrew, Hollywood Roosevelt Hotel</PartnerName>
        </ContentContainer>
        
        <PercentageContainer>
          <PercentageWrapper>
            <Percentage>540</Percentage>
            <PercentageText>
              Over 500 monthly poolside and dine-in orders from hotel guests
            </PercentageText>
          </PercentageWrapper>
          
          <PercentageWrapper>
            <Percentage>800</Percentage>
            <PercentageText>
              Average monthly QR scans and menu views
            </PercentageText>
          </PercentageWrapper>
        </PercentageContainer>
      </PartnerSaysContainer>
    </div>
  )
}

const PartnerSaysContainer = ResponsiveStyled({
  elem: "article",
  mobile: {
    padding: "10rem 0 12rem"
  },
  tablet: {
    padding: "4rem 0 7rem"
  },
  pc: {
    padding: "6rem 4rem 15rem"
  }
})
const Title = ResponsiveStyled({
  elem: "h1",
  common: {
    "font-weight": "bold",
    color: "#2c2c46",
    "letter-spacing": "0.23px"
  },
  mobile: {
    "font-size": "2rem",
    "text-align": "center"
  },
  tablet: {
    "font-size": "4.5rem"
  },
  pc: {
    "font-size": "4.5rem"
  }
})

const ContentContainer = ResponsiveStyled({
  elem: "div",
  common: {
    position: "relative",
    "background-color": "#2c2c46"
  },
  mobile: {
    "background-color": "transparent"
  },
  tablet: {
    "margin-top": "2.8rem",
    padding: "5.2rem 6.2rem 5.2rem 25.8rem"
  },
  pc: {
    "margin-top": "2.4rem",
    padding: "4.6rem 9rem 4.6rem 30.3rem"
  }
})

const PartnerText = ResponsiveStyled({
  elem: "p",
  common: {
    "font-weight": "300",
    "line-height": "1.4",
    color: "#fdfdfd",
    "margin-bottom": 0
  },
  mobile: {
    "margin-top": "3.2rem",
    "font-size": "1.2rem",
    color: "#2c2c46",
    "text-align": "center"
  },
  tablet: {
    "font-size": "1.5rem"
  },
  pc: {
    "font-size": "1.9rem"
  }
})

const PartnerName = ResponsiveStyled({
  elem: "span",
  common: {
    "font-weight": "500",
    "line-height": "1.3",
    color: "#fdfdfd"
  },
  mobile: {
    display: "block",
    "margin-top": "1.6rem",
    "font-size": "1.2rem",
    color: "#2c2c46",
    "text-align": "center"
  },
  tablet: {
    display: "block",
    "font-size": "1.6rem",
    "margin-top": "1.6rem"
  },
  pc: {
    display: "block",
    "margin-top": ".8rem",
    "font-size": "1.9rem"
  }
})

const ImageWrapper = ResponsiveStyled({
  elem: "div",
  mobile: {
    display: "block",
    "max-width": "86%",
    width: "100%",
    margin: "1.8rem auto 0"
  },
  tablet: {
    position: "absolute",
    top: "5.2rem",
    left: "6.2rem",
    width: "14.7rem",
    height: "14.7rem"
  },
  pc: {
    position: "absolute",
    top: "4rem",
    left: "9rem",
    width: "17rem",
    height: "17rem"
  }
})

const PercentageContainer = ResponsiveStyled({
  elem: "div",
  common: {
    "font-size": 0,
    "text-align": "center"
  },
  tablet: {
    margin: "3rem auto 0"
  },
  pc: {
    margin: "4rem auto 0"
  }
})

const PercentageWrapper = ResponsiveStyled({
  elem: "div",
  common: {
    display: "inline-block",
    "box-sizing": "content-box"
  },
  mobile: {
    display: "none"
  },
  tablet: {
    padding: "0 4.8rem",
    "max-width": "27.5rem"
  },
  pc: {
    padding: "0 1.9rem",
    "max-width": "28rem"
  }
})

const Percentage = ResponsiveStyled({
  elem: "span",
  common: {
    display: "block",
    margin: "0 auto",
    "font-weight": "600",
    "line-height": "1.2"
  },
  tablet: {
    "font-size": "5rem"
  },
  pc: {
    "font-size": "5rem"
  }
})
const PercentageText = ResponsiveStyled({
  elem: "p",
  common: {
    "text-align": "left",
    "margin-bottom": 0
  },
  tablet: {
    "margin-top": "2rem",
    "font-size": "1.5rem"
  },
  pc: {
    "margin-top": "2rem",
    "font-size": "1.5rem"
  }
})

export default HospitalityVenuePartnerSays
