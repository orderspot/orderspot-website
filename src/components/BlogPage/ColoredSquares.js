import React from 'react';

const ColoredSquares = () => {
  
  return (
    <div className="colored-squares">
      <div className="blue-square"/>
      <div className="skin-square"/>
    </div>
  );
};

ColoredSquares.propTypes = {};

export default ColoredSquares;
