import React from "react"
import Text from "../common/Text/Text"
import SocialIcons from "../common/SocialIcons/SocialIcons"
import ColoredSquares from "./ColoredSquares"
import BackgroundImage from "gatsby-background-image"
import { graphql, useStaticQuery } from "gatsby"

const BlogTop = (props) => {
  const {
    featuredImage,
    subTitleBlack,
    excerpt,
    title
  } = props
  const data = useStaticQuery(query)
  
  const textBox = (
    <>
      <div className="blog-bg-backdrop" />
      <div className="blog-top--title-box">
        <Text className="blog-top--sub-title" variant="title2" color="indigo-color">
          {title}
        </Text>
      </div>
      
      <div className="excerpt-text-wrapper">
        <Text variant="title4" className="blog-top--info-text w-500" color="indigo-3">
          {subTitleBlack}
        </Text>
      </div>
      {excerpt && <div className="excerpt-text-wrapper">
        <Text variant="body4" className="blog-top--date w-500" color="indigo-4">
          {excerpt}
        </Text>
      </div>}
    </>
  )
  
  return (
    <section className="blog-top">
      <div className="container">
        <div className="row">
          <div className="col-xl-6 col-lg-12 col-md-12">
            <BackgroundImage
              Tag="div"
              className="blog-bg-img"
              fluid={featuredImage}
              backgroundColor={`#040e18`}
            />
          </div>
          <div className="col-xl-6 col-lg-12 col-md-12 blog-top-text-wrapper">
            <ColoredSquares />
            <div className="blog-top--text-box">
              
              <div
                className="blog-top-mob-bg-color-box hide-on-mobile"
              >
                {textBox}
              </div>
              
              <BackgroundImage
                className="blog-top-mob-bg-color-box hide-on-tablet"
                Tag="section"
                fluid={data.mobBgImageFile.mobBgImage.fluid}
                backgroundColor={`#040e18`}
              >
                {textBox}
              </BackgroundImage>
              <div className="blog-top--social-wrapper">
                <SocialIcons />
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  )
}

const query = graphql`
  query {
     mobBgImageFile:file(relativePath: {eq: "mobile-blog-background.png"}){
      mobBgImage:childImageSharp{
        fluid{
          ...GatsbyImageSharpFluid
        }
      }
    }
  }
`

BlogTop.propTypes = {}


export default BlogTop
