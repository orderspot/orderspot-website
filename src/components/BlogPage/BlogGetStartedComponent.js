import React, { useContext } from "react"
import Text from "../common/Text/Text"
import Button from "../common/Button/Button"
import {
  CTA_TYPO,
  FORMSTACK_REGISTRATION_FORM,
} from "../../constants/constants"
import { googleAdsEventConversion } from "../../utils/googleAnalyticsHelper"
import scrollTo from "gatsby-plugin-smoothscroll"
import DialogContext from "../../context/DialogContext"

const BlogGetStartedComponent = () => {
  const useDialog = useContext(DialogContext) || {}
  // const openModal = useDialog.actions ? useDialog.actions.openModal : {}

  const openPartnerPage = () => {
    googleAdsEventConversion()
    if (window.location.pathname === "/") {
      scrollTo("#formstackEmbedSection")
    } else {
      window.location.href = window.location.origin + "#formstackEmbedSection"
    }
    // window.open(FORMSTACK_REGISTRATION_FORM, "_blank")
  }

  return (
    <div className="container">
      <div className="get-started-component">
        <div className="get-started--text-box">
          <Text color="indigo-color" className="get-started--title w-700">
            Launch 360 Ordering Service in just{" "}
            <span className="blue-text">5 Minutes</span>
          </Text>
          <Text
            variant="body4"
            color="indigo-3"
            className="get-started--text w-500"
          >
            Don’t miss out on a free QR Poster!
          </Text>
        </div>
        <Button
          onClick={openPartnerPage}
          style={{ width: "19rem", height: "5.6rem" }}
          text={CTA_TYPO}
        />
      </div>
    </div>
  )
}

BlogGetStartedComponent.propTypes = {}

export default BlogGetStartedComponent
