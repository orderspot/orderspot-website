import React from "react"
import { Formik } from "formik"
import * as Yup from "yup"
import InputField from "../common/InputField/InputField"
import Button from "../common/Button/Button"
import Text from "../common/Text/Text"

const LoginForm = ({ notReadyFunc, isSignupForm }) => {
  const loginFormSchema = Yup.object().shape({
    email: Yup.string().email().required("Enter your Email"),
    password: Yup.string().required("Enter your Password"),
  })

  const handleSubmit = values => {
    if (notReadyFunc) notReadyFunc()
  }

  return (
    <Formik
      initialValues={{ restaurantName: "", email: "" }}
      onSubmit={handleSubmit}
      validationSchema={loginFormSchema}
    >
      {({ values, handleChange, handleSubmit, errors }) => {
        const { email, password } = values

        return (
          <form onSubmit={handleSubmit} className="login-form">
            <div className="input-wrapper">
              <InputField
                label="Email"
                id="email"
                value={email}
                name="email"
                placeholder="Enter email address"
                handleChange={handleChange}
                helperText={errors.email}
                error={Boolean(errors.email)}
                type="email"
                isRequired={true}
                isSolution={true}
              />
            </div>

            <div className="input-wrapper">
              <InputField
                label="Password"
                id="password"
                value={password}
                name="password"
                placeholder="Enter Password"
                handleChange={handleChange}
                helperText={errors.password}
                error={Boolean(errors.password)}
                type={"password"}
                isRequired={true}
                isSolution={true}
              />
            </div>
            <div className="form-wrapper--btn">
              <Button
                className="full-width-btn"
                text="Create my store"
                type="submit"
              />
            </div>
            {isSignupForm ? (
              <Text className="login-form-terms-conditions">
                By clicking “create my store,” I agree to Orderspot’s{" "}
                <a href="/terms-of-use">Terms</a> &{" "}
                <a href="/privacy-policy"> Privacy Policy</a>
              </Text>
            ) : (
              <Text className="login-form-terms-conditions">
                By clicking “create my store,” I agree to Orderspot’s{" "}
                <a href="/terms-of-use">Terms</a> &{" "}
                <a href="/privacy-policy"> Privacy Policy</a>
              </Text>
            )}
          </form>
        )
      }}
    </Formik>
  )
}

LoginForm.propTypes = {}

export default LoginForm
