import React from "react"
import Text from "../common/Text/Text"
import Img from "gatsby-image"

const Features = ({ features }) => {
  return (
    <section className="features">
      <div className="container">
        <div className="row">
          {features.map((feature, index) => (
            <div key={index} className="col-md-4">
              <div className="feature">
                <div
                  className="feature--img"
                  data-sal="fade"
                  data-sal-duration="900"
                  data-sal-delay={150 * ( index + 1 )}
                  data-sal-easing="ease"
                >
                  <Img
                    fluid={feature.img}
                    alt="features-section"
                  />
                </div>
                <div className="feature--text-box">
                  <Text
                    variant="title4"
                    color="black-text"
                    className="feature--text-main w-500"
                  >
                    {feature.title}
                  </Text>
                  <Text
                    variant="body7"
                    color="indigo-3"
                    className="feature--text w-500"
                  >
                    {feature.text}
                  </Text>
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
    </section>
  )
}

Features.propTypes = {}

export default Features
