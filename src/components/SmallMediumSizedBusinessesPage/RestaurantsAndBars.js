import React, { useContext } from "react"
import {
  YOUTUBE_LINK,
  CTA_TYPO,
  FORMSTACK_REGISTRATION_FORM,
} from "../../constants/constants"
import Text from "../common/Text/Text"
import { circleGreen, circleSimple, dots } from "../../constants/images"
import Button from "../common/Button/Button"
import ButtonWithIcon from "../common/ButtonWithIcon/ButtonWithIcon"
import playIconBlue from "../../assets/icons/play-icon-blue.svg"
import DialogContext from "../../context/DialogContext"
import BackgroundImage from "gatsby-background-image"
import { googleAdsEventConversion } from "../../utils/googleAnalyticsHelper"
import scrollTo from "gatsby-plugin-smoothscroll"

const RestaurantsAndBars = ({ rightImg }) => {
  const useDialog = useContext(DialogContext) || {}
  // const openModal = useDialog.actions ? useDialog.actions.openModal : {}
  const openVideo = () => {
    window.open(YOUTUBE_LINK, "_blank")
  }

  const openPartnerPage = () => {
    googleAdsEventConversion()
    if (window.location.pathname === "/") {
      scrollTo("#formstackEmbedSection")
    } else {
      window.location.href = window.location.origin + "#formstackEmbedSection"
    }
    // window.open(FORMSTACK_REGISTRATION_FORM, "_blank")
  }

  return (
    <section className="restaurants-and-bars">
      <div className="container">
        <div className="row">
          <div className="col-lg-6 col-md-6">
            <div className="restaurants-and-bars--text-box">
              <Text
                variant="heading2"
                color="indigo-color"
                className="restaurants-and-bars--title"
              >
                Restaurants & Bars
              </Text>
              {/*<Text*/}
              {/*  color="indigo-3"*/}
              {/*  className="restaurants-and-bars--text w-300 hide-on-mobile"*/}
              {/*>*/}
              <Text
                color="indigo-3"
                className="restaurants-and-bars--text w-300 restaurant-and-bars--hide-on-mobile"
              >
                Seamless online ordering system for dine-in, takeout and
                delivery. Built in marketing and sales tools. All in one place.
              </Text>

              {/*<div className="restaurants-and-bars--action-btns hide-on-mobile">*/}
              <div className="restaurants-and-bars--action-btns restaurant-and-bars--hide-on-mobile">
                <Button
                  onClick={openPartnerPage}
                  className="restaurants-and-bars--btn"
                  text={CTA_TYPO}
                />
                <ButtonWithIcon
                  icon={playIconBlue}
                  onClick={openVideo}
                  text="Watch Video"
                  textClassName="restaurants-and-bars--btn-text"
                />
              </div>
            </div>
          </div>
          <div className="col-lg-6 col-md-6">
            <div className="restaurants-and-bars--img-wrapper">
              <BackgroundImage
                Tag="section"
                fluid={rightImg}
                backgroundColor={"#C0C0C0"}
                className="restaurants-and-bars--bg-img"
              />

              <div className="restaurants-and-bars--img-bg-color">
                <img
                  src={circleGreen}
                  alt="circle"
                  className="restaurant-and-bars--obj circle-green"
                />
                {/*<img src={hexagon} alt="hexagon" className="restaurant-and-bars--obj hexagon-img"/>*/}
                <img
                  src={circleSimple}
                  alt="circle"
                  className="restaurant-and-bars--obj circle-simple"
                />
                <img
                  src={dots}
                  alt="dots"
                  className="restaurant-and-bars--obj restaurants-and-bars-dots"
                />
              </div>
            </div>

            <div className="restaurants-and-bars--text-box restaurant-and-bars--hide-on-tablet">
              <Text
                color="indigo-3"
                className="restaurants-and-bars--text w-300"
              >
                Seamless online ordering system for dine-in, takeout and
                delivery. Built in marketing and sales tools. All in one place.
              </Text>

              <div className="restaurants-and-bars--action-btns">
                <Button
                  onClick={openPartnerPage}
                  className="restaurants-and-bars--btn"
                  text="Start Free Trial"
                />
                <ButtonWithIcon
                  icon={playIconBlue}
                  onClick={openVideo}
                  text="Watch Video"
                  textClassName="restaurants-and-bars--btn-text"
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  )
}

RestaurantsAndBars.propTypes = {}

export default RestaurantsAndBars
