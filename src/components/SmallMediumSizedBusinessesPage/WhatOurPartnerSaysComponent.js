import React from "react"
import Text from "../common/Text/Text"
import Img from "gatsby-image"

const WhatOurPartnerSaysComponent = ({ pageName, partnerImg }) => {
  const personImg = (
    <div className="person-img-wrapper">
      <Img fluid={partnerImg} alt={`Out partner`} />
    </div>
  )
  return (
    <section
      className={pageName === "small" ? "what-our-partner-says-component merigold-bg" : "what-our-partner-says-component"}>
      <div className="container">
        <div className="row">
          <div className="col-lg-7 col-md-12">
            <div className="what-our-partner-says-component--text-box">
              <Text
                color="indigo-color"
                className="what-our-partner-says-component--main-text"
              >
                What our Partners Say
              </Text>
              <div className="person-img-container hide-on-tablet">
                {personImg}
              </div>
              <Text
                color="black-text"
                className="what-our-partner-says-component--text"
              >
                {pageName === "small"
                  ? `We chose Orderspot because of how user friendly it is. It was very easy to set up and made transitioning into mobile ordering easy. Orderspot also allows our customers to order from their phones instead of waiting in line. Our customers can look at our menu at their pace and order when they feel they are ready. Orderspot is also convenient because it sends a text to the customer when their order is ready so there is little chance of error and no middle man between our restaurant and the customer so we can still have our normal interactions which we so need during times like this.`
                  : pageName === "medium"
                    ? `We chose Orderspot because of how user friendly it is. It was very easy to set up and made transitioning into mobile ordering easy. Orderspot also allows our customers to order from their phones instead of waiting in line. Our customers can look at our menu at their pace and order when they feel they are ready. Orderspot is also convenient because it sends a text to the customer when their oder is ready so there is little chance of error and no middle man between our restaurant and the customer so we can still have our normal interactions which we so need during times like this.`
                    : ""}
              </Text>
              <Text
                color="black-text"
                className="what-our-partner-says-component--text-bold"
              >
                {pageName === "small" ? "Phil, Shrimp Shack" : ""}
                {pageName === "medium" ? "Charles, Firewings" : ""}
              </Text>
              <div className="what-our-partner-says-component--percentage-text">
                <div className="what-our-partner-says-component--percent-single">
                  <Text
                    color="black-text"
                    className="percent-single--text-main"
                  >
                    {pageName === "small" ? "$24" : ""}
                    {pageName === "medium" ? "400+" : ""}
                  </Text>
                  <Text color="black-text" className="percent-single--text">
                    {pageName === "small" ? "Average check size with Orderspot" : ""}
                    {pageName === "medium" ? "Over 400 orders per location monthly" : ""}
                  </Text>
                </div>
                
                <div className="what-our-partner-says-component--percent-single">
                  <Text
                    color="black-text"
                    className="percent-single--text-main"
                  >
                    {pageName === "small" ? "$1300" : ""}
                    {pageName === "medium" ? "$1500" : ""}
                  </Text>
                  <Text color="black-text" className="percent-single--text">
                    {pageName === "small" ? "Monthly average savings with Orderspot" : ""}
                    {pageName === "medium" ? "Monthly average savings per location" : ""}
                  </Text>
                </div>
              </div>
            </div>
          </div>
          
          <div className="person-img-container col-lg-5 col-md-12">
            <div className="hide-on-mobile">{personImg}</div>
          </div>
        </div>
      </div>
    </section>
  )
}

WhatOurPartnerSaysComponent.propTypes = {}

export default WhatOurPartnerSaysComponent
