import React, { useContext } from "react"
import Text from "../common/Text/Text"
import Button from "../common/Button/Button"
import Img from "gatsby-image"
import DialogContext from "../../context/DialogContext"

const OrderTypeSingle = ({ data, openVideo }) => {
  const {
    title,
    info,
    details,
    mainImg,
    subImgInfo,
    typeTitleBlock,
    isBetaBtn,
    tabName
  } = data
  const { subImgUrl, subImgStyles, className } = subImgInfo
  const useDialog = useContext(DialogContext) || {}
  const openModal = useDialog.actions ? useDialog.actions.openModal : {}

  
  return (
    <section className="order-type-single">
      <div className="row">
        <div
          style={{ display: "flex", alignItems: "center" }}
          className="col-xl-5 col-lg-6 col-md-12 order-type-single--text-wrapper"
        >
          <div className="order-type-single--text-box">
            {isBetaBtn && (
              <Button text="BETA" className="order-type-single--beta-btn" />
            )}
            <Text
              color="indigo-color"
              variant="title2"
              className="order-type-single--title"
            >
              {title}
            </Text>
            <Text
              color="indigo-3"
              variant="body6"
              className="order-type-single--info w-500 bm-6"
            >
              {info}
            </Text>
            <ul className="text-box-details">
              {details.map(detail => (
                <li className="order-type-single--detail" key={detail}>
                  {detail}
                </li>
              ))}
            </ul>
            
            <div className="action-buttons">
              <button className="play-video-button" onClick={openVideo}>
                <i />
                <span>Watch Video</span>
              </button>
              {/* <ButtonWithIcon
                text={"Watch Video"}
                onClick={openVideo}
                icon={playIconBlue}
              /> */}
            </div>
          </div>
        </div>
        <div className="col-xl-7 col-lg-5 col-md-12 img-row-wrapper">
          <div className="img-row-wrapper--title-wrapper hide-on-tablet">
            <Text color="indigo-color" variant="body9" className="w-700">
              {typeTitleBlock}
            </Text>
          </div>
          
          <div className="order-type-single--img-wrapper">
            <Img
              className="order-type-single--img"
              fluid={mainImg}
              alt={"bg-img"}
            />
            <div
              className={`order-type-single--sub-img ${className}`}
              style={subImgStyles || {}}
            >
              <Img fluid={subImgUrl} alt="sub-img" />
            </div>
          </div>
        </div>
      </div>
    </section>
  )
}

OrderTypeSingle.propTypes = {}

export default OrderTypeSingle
