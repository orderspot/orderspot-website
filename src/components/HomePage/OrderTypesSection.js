import React from "react"
import OrderTypeSingle from "./OrderTypeSingle"
import Carousel from "react-bootstrap/Carousel"
import Text from "../common/Text/Text"
import ScheduleDemo from "../Solutions/ScheduleDemo"
import {
  ENUM_EVENT_CATEGORIES,
  ENUM_EVENT_ACTIONS,
} from "../../constants/enumEvents"
import { YOUTUBE_LINK } from "../../constants/constants"
import ButtonWithIcon from "../common/ButtonWithIcon/ButtonWithIcon"
import playIconBlue from "../../assets/icons/play-icon-blue-circle.svg"
import VideoModal from "../common/VideoModal/VideoModal"
import { useMediaQuery } from "react-responsive"

import { gtagPluginEventTracker } from "../../utils/googleAnalyticsHelper"

const OrderTypeTabNames = [
  "QR Table Ordering",
  "Pickup & Delivery",
  "Marketing Insights",
]

const OrderTypesSection = props => {
  const {
    appleWithGooglePayBtn,
    contactLessMenuImg,
    itemWithCart,
    nextGenImg,
    orderTypeMenuEditImg,
    seamLessImg,
  } = props
  const [isOpen, setIsOpen] = React.useState(false)
  const orderTypes = [
    {
      id: "1",
      tabName: "QR Table Ordering",
      title: "Supercharge your Staff",
      info:
        "Upsell  your star items with beautiful digital menus. Free up staff to provide a personal touch in every guest interaction",
      details: [
        "Display beautiful photos that delight customers",
        "Instantly update menu items, specials and promotions",
        "Complimentary QR Codes shipped to your door",
      ],
      typeTitleBlock: "QR Table Ordering",
      mainImg: contactLessMenuImg,
      subImgInfo: {
        subImgUrl: itemWithCart,
        className: "item-with-cart-img",
        subImgStyles: {
          position: "absolute",
          left: "-5.5rem",
          bottom: "-10rem",
        },
      },
    },
    {
      id: "2",
      tabName: "Pickup & Delivery",
      title: "Offer Commission-Free Ordering",
      info:
        "Grow your business without growing your fees. Offer your customers the option to order takeout and delivery directly through your website or Instagram. Always commission-free",
      details: [
        "Checkout process designed to convert sales",
        "Optimized for desktop, mobile and tablet usage",
        "Programmable happy hours, specials and events",
      ],
      typeTitleBlock: "Marketing Insights",
      mainImg: seamLessImg,
      subImgInfo: {
        subImgUrl: appleWithGooglePayBtn,
        className: "apple-with-google-pay-btn",
        subImgStyles: {
          position: "absolute",
          left: "-2.9rem",
          bottom: "-9rem",
          width: "50.75rem",
        },
      },
    },
    {
      id: "3",
      tabName: "Marketing Insights",
      title: "Reach more Customers",
      info:
        "Turn restaurant visitors into loyal regulars. Own customer relationships and reach more customers.",
      details: [
        "Create SMS/Email lists to promote your events",
        "Build customer profiles that helps you understand your guests’ preferences",
        "Received tailored customer insights",
      ],
      typeTitleBlock: "QR Table Ordering",
      mainImg: nextGenImg,
      isBetaBtn: true,
      subImgInfo: {
        subImgUrl: orderTypeMenuEditImg,
        className: "next-gen-img",
        subImgStyles: {
          position: "absolute",
          left: "-3.5rem",
          bottom: "-13rem",
        },
      },
    },
  ]

  const isTablet = useMediaQuery({
    minWidth: 768,
  })

  const [index, setIndex] = React.useState(0)
  const handleChangeIndex = i => {
    if (index === i) return
    gtagPluginEventTracker({
      category: ENUM_EVENT_CATEGORIES.MAIN_PAGE,
      action: ENUM_EVENT_ACTIONS.CLICK,
      label: `${OrderTypeTabNames[i]} Tab`,
    })
    setIndex(i)
  }
  const getClassname = i => {
    if (i === index) return "navigation-text active"
    return "navigation-text"
  }
  const handleSelect = (selectedIndex, e) => {
    setIndex(selectedIndex)
  }

  const openVideo = () => {
    window.open(YOUTUBE_LINK, "_blank")
    gtagPluginEventTracker({
      category: ENUM_EVENT_CATEGORIES.MAIN_PAGE,
      action: ENUM_EVENT_ACTIONS.CLICK,
      label: `Mobile_WatchVideo`,
    })
  }

  const openVideoModal = () => {
    setIsOpen(true)
    gtagPluginEventTracker({
      category: ENUM_EVENT_CATEGORIES.MAIN_PAGE,
      action: ENUM_EVENT_ACTIONS.CLICK,
      label: `Mobile_WatchVideo`,
    })
  }

  const closeVideo = () => {
    setIsOpen(false)
  }

  return (
    <section className="order-types-section">
      <VideoModal isOpen={isOpen} closeFunc={closeVideo} />
      <div className="order-types-section--btns-text-wrapper hide-on-mobile">
        <div className="container order-types-section--text-box">
          <Text
            color="indigo-color"
            className="order-types-section--title w-700"
          >
            Get your Restaurant Online
          </Text>
          <Text
            color="indigo-color"
            className="order-types-section--text w-500"
          >
            Offer direct mobile ordering for dine-in, takeout and delivery. Use
            customer insights to turn visitors into regulars.
          </Text>

          {/*hide on mobile*/}
          <div className="navigation-texts">
            <button
              onClick={() => handleChangeIndex(0)}
              className={getClassname(0)}
            >
              QR Table Ordering
            </button>
            <button
              onClick={() => handleChangeIndex(1)}
              className={getClassname(1)}
            >
              Pickup & Delivery
            </button>
            <button
              onClick={() => handleChangeIndex(2)}
              className={getClassname(2)}
            >
              Marketing Insights
            </button>
          </div>
        </div>
      </div>
      <div className="container">
        <div className="your-res--text-box hide-on-tablet">
          <Text className="your-res--title">
            Your restaurant, your customers
          </Text>
          <Text className="your-res--text">
            Web-based ordering app that lets you do more with your data
          </Text>
        </div>
        <Carousel
          fade={true}
          controls={false}
          indicators={false}
          activeIndex={index}
          onSelect={handleSelect}
          className="hide-on-mobile"
        >
          {orderTypes.map(data => (
            <Carousel.Item key={data.id}>
              <OrderTypeSingle
                data={data}
                openVideo={isTablet ? openVideoModal : openVideo}
                closeVideo={closeVideo}
              />
            </Carousel.Item>
          ))}
        </Carousel>

        <div id="scheduleDemoContainer">
          <ScheduleDemo />
        </div>

        {/*  hide on tablet and desktop*/}
        <div className="order-types-wrapper hide-on-tablet">
          {orderTypes.map(data => (
            <OrderTypeSingle key={data.id} data={data} />
          ))}

          <ScheduleDemo />
          <ButtonWithIcon
            text={"Watch Video"}
            onClick={isTablet ? openVideoModal : openVideo}
            icon={playIconBlue}
          />
        </div>
      </div>
    </section>
  )
}

OrderTypesSection.propTypes = {}

export default OrderTypesSection
