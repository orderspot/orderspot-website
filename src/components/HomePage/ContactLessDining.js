import React, { useState, useEffect } from "react"
import Text from "../common/Text/Text"
import StorePreviewForm from "../common/StorePreviewForm/StorePreviewForm"
import Img from "gatsby-image"
import ScheduleDemo from "../Solutions/ScheduleDemo"
import PhoneMockUp from "../../assets/abtest/phone-mock-up.png"
import scrollTo from "gatsby-plugin-smoothscroll"
import watchVideoIcon from "../../assets/ic-watch-video.png"
import { YOUTUBE_LINK } from "../../constants/constants"

const mainHeadTitles = ["CONTACTLESS", "BRANDED", "INTUITIVE"]

const ContactLessDining = ({ phoneImg, dineInBgOneMob, dineInBgTwoMob }) => {
  const [intervalCount, setIntervalCount] = useState(0)
  const [textContent, setTextContent] = useState(mainHeadTitles[0])
  const [animationFire, setAnimationFire] = useState(false)
  useEffect(() => {
    const textInterval = setInterval(() => {
      let count = JSON.parse(JSON.stringify(intervalCount))
      setTextContent(
        mainHeadTitles[parseInt(count / 2, 10) % mainHeadTitles.length]
      )
      if (count % 2 !== 0) {
        setAnimationFire(true)
      } else {
        setAnimationFire(false)
      }
      setIntervalCount(count + 1)
      // if (count === mainHeadTitles.length *2) {
      //   setIntervalCount(0);
      // }
    }, 1000)

    return () => {
      clearInterval(textInterval)
    }
  })
  return (
    <section className="contactless-dining">
      <div className="contactless-dining--content-box">
        <div className="bg-flat-line hide-on-mobile" />

        <div className="mobile-bg-imgs hide-on-tablet">
          <div className="mob-bg-1">
            <Img fluid={dineInBgOneMob} alt="bg" />
          </div>
          <div className="mob-bg-2">
            <Img fluid={dineInBgTwoMob} alt="bg" />
          </div>
        </div>
        {/*930*/}
        <div className="container">
          <div className="row">
            <div className="col-xl-6 col-lg-6 col-md-12 contactless-dining--text-box">
              <Text
                variant="heading1"
                className={`text-box-title--blue text-uppercase ${
                  animationFire ? " fade-in" : ""
                } `}
              >
                {textContent}
              </Text>

              <Text
                color="indigo-color"
                className="text-box-title--black"
                variant="heading1"
              >
                Mobile Ordering
              </Text>
              <Text
                variant="body4"
                className="small-text w-500 last-text"
                color="indigo-color"
              >
                {/*Launch online ordering in just <span className="small-text--bold">5 Minutes</span>*/}
                Online ordering system for dine-in, takeout and delivery.
                Marketing tools & customer insights. All in one place.
              </Text>
              {/* <StorePreviewForm /> */}
              {/* <ScheduleDemo id="topScheduleDemo" /> */}
              <div id="testWrap">
                <button
                  className="os-btn"
                  onClick={() => {
                    scrollTo("#formstackEmbedSection")
                  }}
                >
                  Schedule Demo
                </button>
                <button
                  className="img-btn"
                  onClick={() => {
                    window.open(YOUTUBE_LINK, "_blank")
                  }}
                >
                  <img src={watchVideoIcon} />
                </button>
              </div>

              <Text
                variant="body4"
                className="small-text w-500 last-text agree-to-email"
                color="indigo-3"
              >
                Try Orderspot free for 30 days. By entering your email, you
                agree to receive marketing emails from Orderspot.
              </Text>
            </div>

            <div className="col-xl-6 col-lg-6 col-md-6 hide-on-mobile">
              <div className="image-wrapper">
                <Img fluid={phoneImg} alt="Gatsby Docs are awesome" />
              </div>

              <div className="image-wrapper test-b" style={{ display: "none" }}>
                <img
                  src={PhoneMockUp}
                  alt="Phone mock up image"
                  style={{ width: "100%" }}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  )
}

ContactLessDining.propTypes = {}

export default ContactLessDining
