import React, { useState } from "react"
import Text from "../common/Text/Text"
import Logo from "../common/Logo/Logo"
import Carousel from "react-bootstrap/Carousel"

const RestaurantsWithLogos = () => {
  const [index, setIndex] = useState(0)

  const handleSelect = (selectedIndex, e) => {
    setIndex(selectedIndex)
  }
  return (
    <section className="restaurants-with-logos">
      <div className="title-with-line">
        <div className="title-with-line--line" />
        <Text
          variant="body3"
          color="indigo-color"
          className="title-with-line--title desktop-title"
        >
          Over
          <span> 150+ Restaurants </span>
          Trust Orderspot with Mobile Ordering
        </Text>

        <Text
          variant="body3"
          color="indigo-color"
          className="title-with-line--title tablet-title"
        >
          Join
          <span> 150+ Restaurants </span>
          Seeing Results with Orderspot
        </Text>
      </div>

      <div className="container logos-wrapper hide-on-mobile">
        <div className="row">
          <div className="col-md-3 logo-wrapper">
            <Logo logo="hcm-logo" />
          </div>
          <div className="col-md-3 logo-wrapper">
            <Logo logo="emc-logo" />
          </div>
          <div className="col-md-3 logo-wrapper">
            <Logo logo="firewings-logo" />
          </div>
          <div className="col-md-3 logo-wrapper">
            <Logo logo="la-logo" />
          </div>
        </div>
      </div>

      <div className="hide-on-tablet">
        <Carousel
          controls={false}
          /*fade={true}*/ interval={4000}
          activeIndex={index}
          onSelect={handleSelect}
        >
          <Carousel.Item>
            <div className="logo-wrapper">
              <Logo logo="hcm-logo" />
            </div>
          </Carousel.Item>
          <Carousel.Item>
            <div className="logo-wrapper">
              <Logo logo="firewings-logo" />
            </div>
          </Carousel.Item>
          <Carousel.Item>
            <div className="logo-wrapper">
              <Logo logo="emc-logo" />
            </div>
          </Carousel.Item>
          <Carousel.Item>
            <div className="logo-wrapper">
              <Logo logo="la-logo" />
            </div>
          </Carousel.Item>
        </Carousel>
      </div>
    </section>
  )
}

RestaurantsWithLogos.propTypes = {}

export default RestaurantsWithLogos
