import React from "react"
import StorePreviewForm from "../common/StorePreviewForm/StorePreviewForm"
import SignupFeatures from "../common/SignupFeatures/SignupFeatures"
import Img from "gatsby-image"
import { JESSICA_CALENDLY_LINK } from "../../constants/links"
import { InlineWidget } from "react-calendly"

const SignupFormSection = ({ signupFormPcImage }) => {
  return (
    <section className="signup-form-section">
      <div className="container">
        <div className="signup-form-section--form-wrapper">
          <SignupFeatures />
          {/* <StorePreviewForm notShowTitle={true} /> */}
          <InlineWidget
            styles={{ height: "830px" }}
            url={JESSICA_CALENDLY_LINK}
          />
        </div>

        <div className="signup-form-section-bg-img-wrapper">
          <div className="signup-form-pc-bg-img">
            <Img fluid={signupFormPcImage} alt="background-image for signup" />
          </div>
        </div>
      </div>
    </section>
  )
}

SignupFormSection.propTypes = {}

export default SignupFormSection
