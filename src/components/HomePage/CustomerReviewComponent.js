import React from "react"
import Text from "../common/Text/Text"
import quotes from "../../assets/icons/double-quotes-blue.svg"
import Img from "gatsby-image"

const CustomerReviewComponent = ({ review }) => {
  const { customerImg, customerName, customerMessage } = review
  return (
    <section className="customer-review-component">
      <div className="name-and-image">
        <div className="img-wrapper">
          <div className="customer-img">
            <Img fluid={customerImg} alt="customer" />
          </div>
        </div>
        {customerName}
        {/*<Text color="indigo-color" className="name-and-image--text tm-6 w-500" variant="body8">{customerName}</Text>*/}
      </div>
      
      <div className="customer-comments">
        <div className="quotes">
          <img src={quotes} alt="double-quotes" />
        </div>
        
        <div>
          <Text className="w-500" color="black-text" variant="body5">{customerMessage}</Text>
        </div>
      </div>
    </section>
  )
}

CustomerReviewComponent.propTypes = {}

export default CustomerReviewComponent
