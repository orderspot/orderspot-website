import React from 'react';
import Text from "../common/Text/Text";
import { dedicatedImg, endToEnd, highQalityImg } from "../../constants/images";


const WeDoEverythingSection = () => {
  
  return (
    <section className="we-do-everything">
      <div className="container">
        <div className="we-do-everything--text-box">
          <Text variant="title3" color="os-blue-color" className="w-700">Keep growing your business. We’ve got your
            back.</Text>
          
          {/*<Text variant="title3" color="os-blue-color" className="w-700">We do it everything for you.</Text>*/}
          {/*<Text variant="title3" color="os-blue-color" className="w-700">you can focus on your restaurant!</Text>*/}
        </div>
        
        <div className="we-do-everything--tasks">
          <div className="row">
            {/*task-1*/}
            <div className="col-md-4">
              
              <div className="we-do-everything--task">
                <div className="task-icon">
                  <img className="task-img task-img-1" src={endToEnd} alt="End-to-end service"/>
                </div>
                <div className="task-text end-to-end-text">
                  <Text variant="body6" color="indigo-color" className="w-500">
                    <span className="os-blue-text">End-to-end service </span>
                    from design all the way to launch</Text>
                </div>
              </div>
            </div>
            {/*task-2*/}
            <div className="col-md-4">
              <div className="we-do-everything--task">
                <div className="task-icon">
                  <img className="task-img task-img-2" src={dedicatedImg} alt="End-to-end service"/>
                </div>
                <div className="task-text">
                  <Text variant="body6" color="indigo-color" className="w-500">
                    White glove customer service with
                    <span className="os-blue-text"> dedicated account management</span>
                  </Text>
                </div>
              </div>
            </div>
            
            {/*task-3*/}
            <div className="col-md-4">
              <div className="we-do-everything--task">
                <div className="task-icon">
                  <img className="task-img task-img-3" src={highQalityImg} alt="End-to-end service"/>
                </div>
                <div className="task-text best-in-class-text">
                  <Text variant="body6" color="indigo-color" className="w-500">
                    Highest quality mobile ordering solution with
                    <span className="os-blue-text"> best-in-class user experience</span>
                  </Text>
                </div>
              </div>
            </div>
          </div>
        </div>
      
      </div>
    </section>
  );
};

WeDoEverythingSection.propTypes = {};

export default WeDoEverythingSection;
