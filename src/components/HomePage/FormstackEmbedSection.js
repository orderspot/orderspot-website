import React from "react"
import Button from "../common/Button/Button"

const externalFormstackLink =
  "https://orderspot.formstack.com/forms/partner_application"

const FormstackEmbedSection = () => {
  const openExternalFormstack = () => {
    const qs = window.location.search
    window.open(`${externalFormstackLink}${qs.length ? qs : ""}`, "_blank")
  }
  return (
    <section className="main-page-section" id="formstackEmbedSection">
      <h1 className="section-title">Get Started Today!</h1>
      <p className="section-text">
        No contracts, setup costs or commissions
        <br />
        <span>(We hate those too)</span>
      </p>

      <Button
        className="full-width-btn"
        text="Get Started"
        type="button"
        style={{ fontWeight: 700, fontSize: "2rem", marginTop: "32px" }}
        onClick={openExternalFormstack}
      />
    </section>
  )
}

export default FormstackEmbedSection
