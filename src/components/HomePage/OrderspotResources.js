import React from "react"
import _get from "lodash/get"
import Text from "../common/Text/Text"
import { Link, graphql, useStaticQuery } from "gatsby"
import { BLOGS, FAQS } from "../../constants/constants"
import moment from "moment"

import { gtagPluginEventTracker } from "../../utils/googleAnalyticsHelper"

import {
  ENUM_EVENT_CATEGORIES,
  ENUM_EVENT_ACTIONS,
} from "../../constants/enumEvents"
import BackgroundImage from "gatsby-background-image"

const OrderspotResources = ({ isHomepage }) => {
  const data = useStaticQuery(
    graphql`
      query {
        allContentfulBlogpost {
          edges {
            node {
              title
              excerpt
              slug
              readTime
              publishDate
              featuredImage {
                fluid {
                  base64
                  sizes
                  aspectRatio
                  srcSet
                  src
                }
              }
            }
          }
        }
        qrBgImageFile: file(relativePath: { eq: "temp/bg.jpg" }) {
          qrBgImage: childImageSharp {
            fluid {
              ...GatsbyImageSharpFluid
            }
          }
        }
      }
    `
  )

  const blogPost = _get(data, "allContentfulBlogpost.edges[0].node", {})
  const { title, publishDate, slug, featuredImage } = blogPost
  const backgroundImg = _get(
    featuredImage,
    "fluid",
    data.qrBgImageFile.qrBgImage.fluid
  )

  const readMoreEvent = () => {
    gtagPluginEventTracker({
      category: isHomepage ? ENUM_EVENT_CATEGORIES.MAIN_PAGE : "NOT_HOMEPAGE",
      action: ENUM_EVENT_ACTIONS.CLICK,
      label: `${
        isHomepage ? ENUM_EVENT_CATEGORIES.MAIN_PAGE : "NOT_HOMEPAGE"
      }_BlogReadMore`,
    })
  }
  const learnMoreEvent = item => {
    gtagPluginEventTracker({
      category: isHomepage ? ENUM_EVENT_CATEGORIES.MAIN_PAGE : "NOT_HOMEPAGE",
      action: ENUM_EVENT_ACTIONS.CLICK,
      label: `${
        isHomepage ? ENUM_EVENT_CATEGORIES.MAIN_PAGE : "NOT_HOMEPAGE"
      }_${item}_LearnMore`,
    })
  }

  return (
    <section
      className={
        isHomepage ? "orderspot-resources" : "orderspot-resources not-home-page"
      }
    >
      <div className="container">
        <Text
          className="orderspot-resources--title"
          variant="heading2"
          align="center"
        >
          Orderspot <span style={{ color: "#2c2c46" }}> Resources</span>
        </Text>

        <div className="row">
          <div className="col-xl-7 col-md-7">
            <BackgroundImage
              Tag="div"
              fluid={backgroundImg}
              backgroundColor={"#C0C0C0"}
              className="img-text-wrapper"
            >
              <div className="img-text-wrapper--overlay" />
              <div className="text-box">
                <Text
                  variant="title3"
                  color="white-text"
                  className="text-box--title"
                >
                  {title}
                </Text>
                <Text
                  variant="body2"
                  color="white-text"
                  className="text-box--date"
                >
                  {moment(publishDate).format("MMMM D, YYYY")}
                </Text>
                <Link
                  className="text-box--read-more"
                  to={`/blog/${slug}`}
                  onClick={readMoreEvent}
                >
                  Read More
                </Link>
              </div>
            </BackgroundImage>
          </div>
          <div
            style={{ display: "flex", alignItems: "center" }}
            className="col-xl-5 col-md-5"
          >
            <div className="orderspot-resources--text-boxes">
              <OrderspotResourcesTextBox
                text="Resources to help your reach more customers, improve your operations and get your restaurant online!"
                mainTitle="BLOG"
                link={BLOGS}
                eventTrackFunc={learnMoreEvent}
              />
              <OrderspotResourcesTextBox
                text="Answers to all your frequently asked questions about Orderspot online ordering"
                mainTitle="FAQ"
                link={FAQS}
                eventTrackFunc={learnMoreEvent}
              />
            </div>
          </div>
        </div>
      </div>
    </section>
  )
}

const OrderspotResourcesTextBox = ({
  mainTitle,
  text,
  link,
  eventTrackFunc,
}) => {
  return (
    <div className="orderspot-resources--text-box">
      <p className="main-title">{mainTitle}</p>
      <Text
        color="headline_indigo"
        variant="body4"
        className="sub-title w-500 bm-6"
      >
        {text}
      </Text>
      <Link
        className="learn-more-text"
        to={link}
        onClick={() => {
          eventTrackFunc(mainTitle)
        }}
      >
        Learn More
      </Link>
    </div>
  )
}

OrderspotResources.propTypes = {}

export default OrderspotResources
