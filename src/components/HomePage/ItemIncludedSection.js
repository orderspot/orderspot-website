import React from "react"
import Text from "../common/Text/Text"
import {
  kitchenPrinterBlackIcon,
  kitchenPrinterWhiteIcon,
  orderManagementTabletIconBlack,
  orderManagementTabletIconWhite,
  qrCodeKioskBlackIcon,
  qrCodeKioskWhiteIcon,
  tableQrSingleIconBlack,
  tableQrSingleIconWhite,
} from "../../constants/images"
import { useMediaQuery } from "react-responsive"
import { TABLET_PORTRAIT } from "../../constants/constants"
import { gtagPluginEventTracker } from "../../utils/googleAnalyticsHelper"
import {
  ENUM_EVENT_CATEGORIES,
  ENUM_EVENT_ACTIONS,
} from "../../constants/enumEvents"
import Carousel from "react-bootstrap/Carousel"
import Img from "gatsby-image"

const ItemTabTitle = [
  "Tabletop QR Signage",
  "QR Code Kiosk",
  "Order Management Tablet",
  "Kitchen Printer",
]

const ItemIncludedSection = ({
  tableQRImage,
  standQRImage,
  orderTabletImg,
  printerActivationImg,
}) => {
  const isTabPort = useMediaQuery({ maxWidth: TABLET_PORTRAIT })
  const [index, setIndex] = React.useState(0)
  const [active, setActive] = React.useState(0)
  const handleClick = i => {
    if (active === i) return
    gtagPluginEventTracker({
      category: ENUM_EVENT_CATEGORIES.MAIN_PAGE,
      action: ENUM_EVENT_ACTIONS.CLICK,
      label: `${ItemTabTitle[i]}_HardwareTab`,
    })
    setActive(i)
  }

  const getClassName = i => {
    if (active === i) return "active"
    return ""
  }

  const handleSelect = (selectedIndex, e) => {
    setIndex(selectedIndex)
    setActive(selectedIndex)
  }

  const renderContent = () => {
    switch (active) {
      case 0:
        return (
          <div className="img-wrapper">
            <div className="img-container">
              <Img className="img-1" fluid={tableQRImage} alt="img" />
            </div>
          </div>
        )
      case 1:
        return (
          <div className="img-wrapper">
            <div className="img-container">
              <Img className="img-1" fluid={standQRImage} alt="img" />
            </div>
          </div>
        )
      case 2:
        return (
          <div className="img-wrapper">
            <div className="img-container">
              <Img className="img-1" fluid={orderTabletImg} alt="img" />
            </div>
          </div>
        )
      default:
        return (
          <div className="img-wrapper">
            <div className="img-container">
              <Img className="img-1" fluid={printerActivationImg} alt="img" />
            </div>
          </div>
        )
    }
  }

  const renderItemIncludedButton = () => {
    switch (active) {
      case 0:
        return (
          <IconWithTextAndBtn
            cn={getClassName(0)}
            iconImg={
              active === 0 && !isTabPort
                ? tableQrSingleIconWhite
                : tableQrSingleIconBlack
            }
            text="Tabletop QR Signage"
            onClick={() => handleClick(0)}
          />
        )
      case 1:
        return (
          <IconWithTextAndBtn
            cn={getClassName(1)}
            iconImg={
              active === 1 && !isTabPort
                ? qrCodeKioskWhiteIcon
                : qrCodeKioskBlackIcon
            }
            text="QR Code Kiosk"
            onClick={() => handleClick(1)}
          />
        )
      case 2:
        return (
          <IconWithTextAndBtn
            cn={getClassName(2)}
            iconImg={
              active === 2 && !isTabPort
                ? orderManagementTabletIconWhite
                : orderManagementTabletIconBlack
            }
            text="Order Management Tablet"
            onClick={() => handleClick(2)}
          />
        )
      default:
        return (
          <IconWithTextAndBtn
            cn={getClassName(3)}
            iconImg={
              active === 3 && !isTabPort
                ? kitchenPrinterWhiteIcon
                : kitchenPrinterBlackIcon
            }
            text="Kitchen Printer"
            onClick={() => handleClick(3)}
          />
        )
    }
  }

  return (
    <section className="item-included-section">
      {/*Background-dots*/}
      {/*<img src={dot1} className="dot-1" alt="dots"/>*/}
      {/*<img src={dot2} className="dot-2" alt="dots"/>*/}
      {/*<img src={dot3} className="dot-3" alt="dots"/>*/}
      <div className="container">
        {/*Only show on Desktop and tablet-landscape*/}
        <div className="container item-included-section--text-box hide-on-mobile">
          <Text
            color="indigo-color"
            className="item-included-section--title w-700"
          >
            Hardware Designed for Independent Restaurants
          </Text>
          <Text
            color="indigo-color"
            className="item-included-section--text desktop-title"
          >
            Choose only what works with your brand and operations
          </Text>

          <Text
            color="indigo-color"
            className="item-included-section--text tablet-title"
          >
            Choose only the tools that you need to improve your operations
          </Text>
        </div>

        {/*Only show on tablet-portrait and mobile*/}
        <div className="container item-included-section--text-box hide-on-tablet">
          <Text
            color="indigo-color"
            className="item-included-section--title w-700"
          >
            System that Scales with your Needs
          </Text>
          <Text
            color="indigo-color"
            className="item-included-section--text w-500"
          >
            Choose only the tools that you need to improve your operations
          </Text>
        </div>

        <div className="row hide-on-tablet">
          <div className="col-xl-7 col-lg-7 col-md-12 item-included-section--img-wrapper">
            <Carousel
              fade={true}
              controls={false}
              indicators={true}
              activeIndex={index}
              onSelect={handleSelect}
            >
              <Carousel.Item>
                {renderItemIncludedButton()}
                {renderContent()}
              </Carousel.Item>
              <Carousel.Item>
                {renderItemIncludedButton()}
                {renderContent()}
              </Carousel.Item>
              <Carousel.Item>
                {renderItemIncludedButton()}
                {renderContent()}
              </Carousel.Item>
              <Carousel.Item>
                {renderItemIncludedButton()}
                {renderContent()}
              </Carousel.Item>
            </Carousel>
          </div>
        </div>

        <div className="row hide-on-mobile">
          <div className="col-xl-7 col-lg-7 col-md-12 item-included-section--img-wrapper">
            <div className="hide-on-tablet">{renderItemIncludedButton()}</div>
            {renderContent()}
          </div>

          <div className="col-xl-5 col-lg-5 col-md-6 hide-on-mobile">
            <IconWithTextAndBtn
              cn={getClassName(0)}
              iconImg={
                active === 0 && !isTabPort
                  ? tableQrSingleIconWhite
                  : tableQrSingleIconBlack
              }
              title="Tabletop QR Signage"
              text="Give guests the gift of convenience. Let guests order contactlessly via their mobile phones via signage for every table."
              onClick={() => handleClick(0)}
            />

            <IconWithTextAndBtn
              cn={getClassName(1)}
              iconImg={
                active === 1 && !isTabPort
                  ? qrCodeKioskWhiteIcon
                  : qrCodeKioskBlackIcon
              }
              title="QR Code Kiosk"
              text="Use QR kiosks to shorten wait times, or enable self-service ordering. No complicated or expensive installations required."
              onClick={() => handleClick(1)}
            />

            <IconWithTextAndBtn
              cn={getClassName(2)}
              title="Order Management Tablet"
              iconImg={
                active === 2 && !isTabPort
                  ? orderManagementTabletIconWhite
                  : orderManagementTabletIconBlack
              }
              text="Receive dine-in orders, takeout and delivery one on tablet. Customize alerts, SMS notifications and availability to fit your operations."
              onClick={() => handleClick(2)}
            />

            <IconWithTextAndBtn
              cn={getClassName(3)}
              iconImg={
                active === 3 && !isTabPort
                  ? kitchenPrinterWhiteIcon
                  : kitchenPrinterBlackIcon
              }
              title="Kitchen Printer"
              text="Reduce customer wait times and improve staff efficiency. Print mobile orders directly to a kitchen printer for back of the house staff."
              onClick={() => handleClick(3)}
            />
          </div>
        </div>
      </div>
    </section>
  )
}

const IconWithTextAndBtn = ({ cn, iconImg, title, text, onClick }) => {
  return (
    <button onClick={onClick} className={`icon-with-text-btn ${cn}`}>
      <img src={iconImg} className="btn-icon" alt="os-icon" />
      <div className="icon-with-text-btn--text-box">
        <Text variant="body4" className={`btn-text--main`}>
          {title}
        </Text>
        <Text className="btn-text--sub">{text}</Text>
      </div>
    </button>
  )
}

ItemIncludedSection.propTypes = {}

export default ItemIncludedSection
