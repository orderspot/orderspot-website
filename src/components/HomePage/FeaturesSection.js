import React from "react"
import FeatureSingle from "./FeatureSingle"
import {
  blockOneDotImg,
  blockThreeDotImg,
  blockTwoDotImg
} from "../../constants/images"

const FeaturesSection = ({ orderImg, menuEditImg, womanImg }) => {
  
  return (
    <section className="features-section">
      <div className="container features-section--wrapper">
        <div className="row">
          <FeatureSingle
            mainTextColor="#FFBF39"
            mainText={"+97%"}
            subText="Increase in average check size"
            subTextColor="#2C2C46"
            blockBgImage="#68687B"
            bgImage={orderImg}
          >
            <div className="block-1-dots">
              <img className="block-1-dots--img" src={blockOneDotImg} alt={"dots"} />
            </div>
          </FeatureSingle>
          <FeatureSingle
            mainText={"+58%"}
            mainTextColor="#5667FF"
            subTextColor="#2C2C46"
            blockBgImage="#F6BCB1"
            bgImage={menuEditImg}
            subText="Increase in daily average revenue"
          >
            <div className="block-2-dots">
              <img className="block-2-dots--img" src={blockTwoDotImg} alt={"dots"} />
              <img className="block-2-dots--img" src={blockTwoDotImg} alt={"dots"} />
            </div>
          </FeatureSingle>
          <FeatureSingle
            mainText={"+14%"}
            mainTextColor="#2C2C46"
            subTextColor="#2C2C46"
            blockBgImage="#FFD172"
            bgImage={womanImg}
            subText="Improvement in Customer Satisfaction"
          >
            <div className="block-3-dots">
              <img className="block-3-dots--img" src={blockThreeDotImg} alt={"dots"} />
            </div>
          </FeatureSingle>
        </div>
      </div>
    </section>
  )
}

FeaturesSection.propTypes = {}

export default FeaturesSection
