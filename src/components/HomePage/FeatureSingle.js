import React from "react"
import Img from "gatsby-image"

const FeatureSingle = (props) => {
  const {
    mainTextColor,
    subTextColor,
    blockBgImage,
    bgImage,
    mainText,
    subText,
    children
  } = props
  return (
    <section className="col-xl-4 col-lg-4 col-md-12 feature-single">
      {children}
      <div className="feature-single--wrapper">
        <div style={{ backgroundColor: blockBgImage }} className="feature-single--bg" />
        <div className="feature-single--sub-wrapper">
          <div className="feature-single--img">
            <Img
              fluid={bgImage}
              alt="img"
            />
          </div>
          {/*<img src={bgImage} alt="img" className="feature-single--img"/>*/}
          <p style={{ color: mainTextColor }} className="feature-single--text-main">{mainText}</p>
          <p style={{ color: subTextColor }} className="feature-single--text-sub">{subText}</p>
        </div>
      </div>
    </section>
  )
}

FeatureSingle.propTypes = {}

export default FeatureSingle
