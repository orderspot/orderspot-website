import React from "react"
import { PHONE_NUM } from "../../constants/constants"

const ReadyToGetStarted = () => {
  return (
    <section className="ready-to-get-started-section os-hide">
      <div className="container">
        <h1>Ready to Get Started?</h1>
        <p>Let's Connect!</p>
        <a href={`tel:+1 ${PHONE_NUM}`}>{PHONE_NUM}</a>
      </div>
    </section>
  )
}

ReadyToGetStarted.propTypes = {}

export default ReadyToGetStarted
