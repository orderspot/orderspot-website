import React, { useState } from "react"
import CustomerReviewComponent from "./CustomerReviewComponent"
import Carousel from "react-bootstrap/Carousel"

const CustomerName = ({ name, storeName }) => <div className="customer-review--name">
  <span>{name}</span>
  <span>{storeName}</span>
</div>

const CustomerReviewsCarouselSection = ({ customer1Img, customer2Img }) => {
  const [index, setIndex] = useState(0)
  const data = [
    {
      id: "1",
      customerImg: customer1Img,
      customerName: <CustomerName name="Phil" storeName="- Shrimp Shack" />,
      customerMessage: "We chose Orderspot because of how user friendly it is. It was very easy to set up and made transitioning into mobile ordering easy. Orderspot also allows our customers to order from their phones instead of waiting in line. Our customers can look at our menu at their pace and order when they feel they are ready. Orderspot is also convenient because it sends a text to the customer when their oder is ready so there is little chance of error and no middle man between our restaurant and the customer so we can still have our normal interactions which we so need during times like this."
    },
    {
      id: "2",
      customerImg: customer2Img,
      customerName: <CustomerName name="Charles" storeName="- Fire Wings" />,
      customerMessage: "Orderspot has been absolutely amazing for my restaurant and my business. They are very easy to work with and very accommodating if issues arise. They also have been able to keep my customers and employees safe during this tough time. The increase in sales for my business is just the cherry on top. I highly recommend Orderspot because of their professionalism and their pursuit to make their service seamless with your operation."
    },
  ]
  const handleSelect = (selectedIndex, e) => {
    setIndex(selectedIndex)
  }
  return (
    <section className="customer-reviews-caroused-section">
      <div className="container">
        <Carousel
          controls={false}
          fade={true}
          interval={100000}
          activeIndex={index}
          onSelect={handleSelect}
        >
          {data.map(review => (
            <Carousel.Item key={review.id}>
              <CustomerReviewComponent review={review} />
            </Carousel.Item>
          ))}
        </Carousel>
      </div>
    </section>
  )
}

CustomerReviewsCarouselSection.propTypes = {}

export default CustomerReviewsCarouselSection
