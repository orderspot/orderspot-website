import React from "react"
import FaqsTabs from "./FaqsTabs"
import FaqTabContents from "./FaqTabContents"
import FaqsTabsMobile from "./FaqsTabsMobile"
import { faqsTitle, faqs } from "./faqsData"

const FaqsSection = () => {
  const [value, setValue] = React.useState(0)
  
  const handleChange = (event, newValue) => {
    setValue(newValue)
  }
  
  const handleSetActive = (to) => {
    setValue(Number(to))
  }
  
  return (
    <section className="faqs-section">
      <div className="container">
        <div className="row">
          <div className="col-xl-4 col-lg-4 col-md-12">
            <FaqsTabs
              handleSetActive={handleSetActive}
              handleChange={handleChange}
              value={value}
              data={faqsTitle}
            />
            
            <FaqsTabsMobile
              handleSetActive={handleSetActive}
              handleChange={handleChange}
              value={value}
              data={faqsTitle}
            />
          
          
          </div>
          <div className="col-xl-8 col-lg-8 col-md-12">
            <FaqTabContents faqsTitles={faqsTitle} faqs={faqs} />
          </div>
        </div>
      </div>
    </section>
  )
}

FaqsSection.propTypes = {}

export default FaqsSection
