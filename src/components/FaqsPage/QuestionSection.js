import React from "react"
import Text from "../common/Text/Text"
import { Formik } from "formik"
import * as Yup from "yup"
import InputField from "../common/InputField/InputField"
import Button from "../common/Button/Button"
import topDots from "../../assets/faq-section-form-top-dot-img.svg"
import bottomDots from "../../assets/faq-section-form-bottom-dot-img.svg"

const QuestionSection = () => {
  const storePreviewFormSchema = Yup.object().shape({
    restaurantName: Yup.string().required("Enter your Restaurant Name"),
    email: Yup.string().email().required("Enter your Email"),
  })

  const handleSubmit = () => {
    console.log("heello")
  }

  return (
    <div className="question-section">
      <div className="container">
        {/*DOTS*/}
        <img
          src={topDots}
          alt="top-dots"
          className="question-section--top-dots"
        />
        <img
          src={bottomDots}
          alt="top-dots"
          className="question-section--bottom-dots"
        />
        <Formik
          initialValues={{ name: "", email: "", question: "" }}
          onSubmit={handleSubmit}
          validationSchema={storePreviewFormSchema}
        >
          {({ values, handleChange, handleSubmit, errors }) => {
            const { name, email, question } = values

            return (
              <form onSubmit={handleSubmit} className="faq-section-form">
                <Text
                  variant="title3"
                  className="faq-section-form--main-text"
                  color="indigo-color"
                >
                  Do you have a question that isn’t answered in our FAQ sheet?
                </Text>
                <Text
                  variant="body4"
                  className="faq-section-form--sub-text w-500"
                  color="indigo-color"
                >
                  Please leave your name, email and question. Our team will get
                  back to you in 1-2 business days!
                </Text>
                <div className="input-wrapper">
                  <InputField
                    label="Name"
                    id="name"
                    value={name}
                    name="name"
                    placeholder="Your Name"
                    handleChange={handleChange}
                    helperText={errors.name}
                    error={Boolean(errors.name)}
                    type="text"
                  />
                </div>

                <div className="input-wrapper">
                  <InputField
                    label="Email"
                    id="email"
                    value={email}
                    name="email"
                    placeholder="myemail@example.com"
                    handleChange={handleChange}
                    helperText={errors.email}
                    error={Boolean(errors.email)}
                    type={email}
                  />
                </div>

                <div className="input-wrapper">
                  <InputField
                    label="Question"
                    id="question"
                    value={question}
                    name="question"
                    placeholder="How long will it take to receive my Orderspot box?"
                    handleChange={handleChange}
                    helperText={errors.question}
                    error={Boolean(errors.question)}
                    type="text"
                  />
                </div>

                <div className="question-section--btn">
                  <Button
                    className="full-width-btn"
                    text="Submit"
                    type="submit"
                  />
                </div>
              </form>
            )
          }}
        </Formik>
        ;
      </div>
    </div>
  )
}

QuestionSection.propTypes = {}

export default QuestionSection
