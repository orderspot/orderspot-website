import React from "react"
import FaqSingle from "./FaqSingle"
import PropTypes from "prop-types"

const FaqTabContents = ({ faqsTitles, faqs }) => {
  return (
    <section className="faq-tab-content">
      {faqsTitles.map((faq, index) => <FaqSingle
          key={String(faq.id)}
          index={index}
          faq={faq}
          faqs={faqs}
        />
      )}
    </section>
  )
}

FaqTabContents.propTypes = {
  faqsTitles: PropTypes.array.isRequired,
  faqs: PropTypes.array.isRequired
}

export default FaqTabContents
