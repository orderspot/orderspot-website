import React from 'react';
import Text from "../common/Text/Text";
import { Collapse } from "@material-ui/core";
import smallArrowBlue from '../../assets/icons/small-arrow-top-blue.svg';

const FaqSingleCollapse = ({ handleToggleCollapse, collapseSingle, open }) => {
  const { title, content, id } = collapseSingle;
  
  const iconClassName = open ? "blue-icon-img rotated-arrow" : "blue-icon-img";
  return (
    <section className="faq-single-collapse">
      <button onClick={() => handleToggleCollapse(id)} className="faq-single-collapse--btn">
        <Text variant="title4" className="faq-single-collapse--title w-700">{title}</Text>
        <img src={smallArrowBlue} alt="blue-arrow" className={iconClassName}/>
      </button>
      <Collapse in={open} timeout="auto" unmountOnExit>
        <div className="faq-single-collapse--content">
          <Text variant="body4" color="indigo-color" className="w-300">{content}</Text>
        </div>
      </Collapse>
    </section>
  );
};

FaqSingleCollapse.propTypes = {};

export default FaqSingleCollapse;
