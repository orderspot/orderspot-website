import React from 'react';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import PropTypes from 'prop-types';
import { Link } from "react-scroll";

function a11yProps(index) {
  return {
    id: `vertical-tab-${index}`,
    'aria-controls': `vertical-tabpanel-${index}`,
  };
}

const FaqsTabs = ({ value, handleChange, data, handleSetActive, containerId }) => {
  
  return (
    <div className="faqs-tabs hide-on-mobile">
      <Tabs
        orientation="vertical"
        variant="scrollable"
        value={value}
        onChange={handleChange}
        aria-label="Vertical tabs example"
      >
        {data.map((faq, index) => {
          
          const link = ( <Link
            activeClass="active"
            className={"navigation-link"}
            to={String(index)}
            spy={true}
            smooth={true}
            offset={-100}
            duration={700}
            onSetActive={handleSetActive}
            containerId={containerId}
            isDynamic={true}
          >
            {faq.faqTitle}
          </Link> );
          
          return <Tab
            key={faq.id}
            disableRipple
            label={link}
            {...a11yProps(index)}
          />;
        })}
      </Tabs>
    </div>
  );
};

FaqsTabs.propTypes = {
  data: PropTypes.array.isRequired,
  handleChange: PropTypes.func.isRequired,
  handleSetActive: PropTypes.func.isRequired,
  value: PropTypes.number.isRequired,
};

export default FaqsTabs;
