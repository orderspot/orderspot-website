import { v4 as uuidv4 } from "uuid"

export const faqsTitle = [
  {
    id: 1,
    faqTitle: "SIGNING UP FOR ORDERSPOT"
  },
  {
    id: 2,
    faqTitle: "CUSTOMER DATA"
  },
  {
    id: 3,
    faqTitle: "MANAGING ORDERS"
  },
  {
    id: 4,
    faqTitle: "RESTAURANT OPERATIONS"
  },
  {
    id: 5,
    faqTitle: "ACCOUNT MANAGEMENT & PAYMENT"
  }
]

export const faqs = [
  {
    categoryId: 1,
    id: uuidv4(),
    title: "How much is it to activate Orderspot?",
    content: "The restaurant Setup Fee is $189 per location. Setup includes account activation, menu setup, QR signage and your Orderspot tablet. Orderspot Kitchen printers are available for an additional cost of $200/printer"
  },
  {
    categoryId: 1,
    id: uuidv4(),
    title: "How do I setup Orderspot for my restaurant?",
    content: "First, sign up for an Orderspot account at partner.orderspot.app/register/account. Within 1-2 business days an Orderspot onboarding specialist will be in touch to help with menu setup, determine QR signage needs and launch your account!"
  },
  {
    categoryId: 2,
    id: uuidv4(),
    title: "What kind of customer information is shared with restaurants?",
    content: "Orderspot makes it easy for restaurants to access customer insights and shares contact information, Gender, Age, Channel Source, Item Information, Menu Impressions, and Transaction History with our restaurant partners. All customer data is handled according to Orderspot’s Privacy Policy and Terms of Use and includes the following information."
  },
  {
    categoryId: 2,
    id: uuidv4(),
    title: "Is Orderspot PCI Compliant?",
    content: "Orderspot is partnered with payment processing companies that are validated level 1 PCI DSS compliant service providers"
  },
  {
    categoryId: 3,
    id: uuidv4(),
    title: "How do customers process payments?",
    content: "For dine-in, customers can order & pay contactlessly and conveniently from their phones through the Orderspot QR code (no app download required). They will be able to pay with mobile wallets (ApplePay, GooglePay), credit cards and debit cards (Visa, Mastercard Maestro, American Express, Discover, JCM, UnionPay)."
  },
  {
    categoryId: 3,
    id: uuidv4(),
    title: "How can I use my Orderspot store link?",
    content: "You can use your Orderspot store for pre-orders, catering and takeout. We provide a QR for in-venue orders and you can also take website and Instagram orders using Orderspot (direct delivery is coming soon!)"
  },
  {
    categoryId: 3,
    id: uuidv4(),
    title: "What can I do on my orders management tablet?",
    content: "You will be able to receive orders across your channels, customize your business hours, provide order updates to customers with an SMS notification system, and pause or resume your online store."
  },
  {
    categoryId: 3,
    id: uuidv4(),
    title: "How can I refund my customers?",
    content: "You can refund customers through your orders manager through your Orderspot tablet. Simply click the “refund” button and select the items you’d like to refund. The customer will receive an SMS notification saying that they’ve been refunded."
  },
  {
    categoryId: 4,
    id: uuidv4(),
    title: "How can I receive orders from my customers?",
    content: "For dine-in, customers can order & pay contactlessly and conveniently from their phones through the Orderspot QR code (no app download required). They will be able to pay with mobile wallets (ApplePay, GooglePay), credit cards and debit cards (Visa, Mastercard Maestro, American Express, Discover, JCM, UnionPay)."
  },
  {
    categoryId: 4,
    id: uuidv4(),
    title: "Do you have a kitchen printer that can print orders?",
    content: "You can also choose to print orders directly to the Orderspot kitchen printer."
  },
  {
    categoryId: 4,
    id: uuidv4(),
    title: "How does TIPS reporting work?",
    content: "Tips are included in the restaurant’s Orderspot balance and disbursed to the restaurant bank account every business day. Sales reports can be downloaded as a CSV file and include item, subtotal, tip, total and processing rates for the restaurant. We are also able to set a fixed tip for restaurants."
  },
  {
    categoryId: 4,
    id: uuidv4(),
    title: "Do you offer POS Integrations?",
    content: "In select cases, we can work with you to secure integrations directly to your on-premise POS system. Please reach out to the sales team to learn more about integrations."
  },
  {
    categoryId: 5,
    id: uuidv4(),
    title: "How much does Orderspot cost?",
    content: "Orderspot charges a flat monthly subscription of $49/month as well as an online ordering fee to diners. All vendors receive a 30 day free trial during their first month. To support restaurant vendors during these difficult times, Orderspot will be waiving the monthly fee through March 31, 2021."
  },
  {
    categoryId: 5,
    id: uuidv4(),
    title: "How much is the credit card processing fee?",
    content: "Vendors are responsible for the 2.9% + 30 cents/order for credit card processing to processing partner Stripe. Orderspot also offers cashback program based on customer loyalty"
  },
  {
    categoryId: 5,
    id: uuidv4(),
    title: "Is there a balance disbursement fee from Orderspot to my bank account?",
    content: "Vendors are responsible for a 0.25% payment disbursement fee for unlimited payouts"
  }
]
