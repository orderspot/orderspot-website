import React from "react"
import Text from "../common/Text/Text"
import FaqSingleCollapse from "./FaqSingleCollapse"
import { Element } from "react-scroll"

const FaqSingle = ({ faq, index, faqs }) => {
  const [openedFaqs, setOpenedFaqs] = React.useState([])
  const handleToggleCollapse = id => {
    if ( openedFaqs.includes(id) )
      setOpenedFaqs(openedFaqs.filter(faq => faq !== id))
    else setOpenedFaqs([...openedFaqs, id])
  }
  const { faqTitle, id } = faq
  
  const collapseData = faqs.filter((faq) => faq.categoryId === id)
  
  return (
    <Element name={String(index)} className="faq-single">
      <Text color="indigo-color" className="faq-single--title w-700">
        {faqTitle} :
      </Text>
      {collapseData.map(collapseSingle => (
        <FaqSingleCollapse
          key={collapseSingle.id}
          collapseSingle={collapseSingle}
          handleToggleCollapse={handleToggleCollapse}
          open={openedFaqs.includes(collapseSingle.id)}
        />
      ))}
    </Element>
  )
}

FaqSingle.propTypes = {}

export default FaqSingle
