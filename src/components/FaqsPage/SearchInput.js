import React from "react"
import InputField from "../common/InputField/InputField"
import Button from "../common/Button/Button"
import { Formik } from "formik"
import * as Yup from "yup"
import searchIconBlack from "../../assets/icons/search-icon-black.svg"

const SearchInput = ({ handleSubmit, getSearchTerm }) => {
  // const searchInputSchema = Yup.object().shape({
  //   searchTerm: Yup.string()
  //     .required("Please enter a value")
  // })
  
  
  return (
    <Formik
      initialValues={{ searchTerm: "" }}
      onSubmit={handleSubmit}
      // validationSchema={searchInputSchema}
    >
      {
        ({
           values,
           handleChange,
           handleSubmit,
           errors
         }) => {
          const { searchTerm } = values
          
          return (
            <form onSubmit={handleSubmit} className="search-input-form">
              <div className="input-wrapper">
                <InputField
                  label="Search"
                  id="restaurantName"
                  value={searchTerm}
                  name="searchTerm"
                  placeholder="Search"
                  handleChange={(...arg) => {
                    console.log("data");
                    getSearchTerm(...arg)
                    handleChange(...arg)
                  }}
                  helperText={errors.searchTerm}
                  error={Boolean(errors.searchTerm)}
                  type="text"
                />
                <img src={searchIconBlack} alt="search" className="input-wrapper--search-icon" />
              </div>
              
              <div className="form-wrapper--btn">
                <Button
                  className="search-input--btn"
                  text="SEARCH"
                  type="submit"
                />
              </div>
            </form>
          )
        }
      }
    </Formik>
  )
}

SearchInput.propTypes = {}

export default SearchInput
