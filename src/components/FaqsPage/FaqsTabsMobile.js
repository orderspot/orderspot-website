import React from 'react';
import { Link } from "react-scroll";
import PropTypes from "prop-types";

const FaqsTabsMobile = ({ data, handleSetActive, containerId }) => {
  
  return (
    <section className="faqs-tabs-mobile hide-on-tablet">
      {data.map((faq, index) => {
        return (
          <Link
            activeClass="active"
            className={"navigation-link"}
            to={String(index)}
            spy={true}
            smooth={true}
            offset={-100}
            duration={700}
            onSetActive={handleSetActive}
            containerId={containerId}
            isDynamic={true}
            key={index}
          >
            {faq.faqTitle}
          </Link>
        );
      })}
    </section>
  );
};

FaqsTabsMobile.propTypes = {
  data: PropTypes.array.isRequired,
  handleSetActive: PropTypes.func.isRequired,
  value: PropTypes.number.isRequired,
};

export default FaqsTabsMobile;
