import React from "react"
import Text from "../common/Text/Text"
import Img from "gatsby-image"
import FaqSingleCollapse from "./FaqSingleCollapse"
import { Element } from "react-scroll"

const FaqsSearchResult = ({ noSearchResultFaceImg, searchTerm, searchedFaqs }) => {
  const [openedFaqs, setOpenedFaqs] = React.useState([])
  const handleToggleCollapse = id => {
    if ( openedFaqs.includes(id) )
      setOpenedFaqs(openedFaqs.filter(faq => faq !== id))
    else setOpenedFaqs([...openedFaqs, id])
  }
  
  const searchResultText = (
    <Text className="faqs-search-result--text">
      Result for
      <span className="faqs-search-result--text-bold">{`“${searchTerm}”`}</span>
    </Text>
  )
  
  
  if ( !searchedFaqs.length )
    return (
      <Element name={"search-result"} className="container faqs-search-result">
        <div className="row">
          <div className="col-md-12">
            {searchResultText}
            <div className="not-search-match-component">
              <div className="not-search-match-component--img-wrapper">
                <Img fluid={noSearchResultFaceImg} alt="not-found" className="not-search-match-component--img" />
              </div>
              <Text color={"indigo-3"} className="not-search-match-component--text">No search result found</Text>
            </div>
          </div>
        </div>
      </Element>
    )
  
  return (
    <Element name={"search-result"} className="container faqs-search-result">
      <div className="row">
        <div className="col-lg-12 col-md-12 col-12">
          {searchResultText}
          {searchedFaqs.map((collapseSingle) => {
            return <FaqSingleCollapse
              key={collapseSingle.id}
              collapseSingle={collapseSingle}
              handleToggleCollapse={handleToggleCollapse}
              open={openedFaqs.includes(collapseSingle.id)}
            />
          })}
        </div>
      </div>
    </Element>
  )
}

FaqsSearchResult.propTypes = {}

export default FaqsSearchResult
