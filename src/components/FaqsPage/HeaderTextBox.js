import React from "react"
import Text from "../common/Text/Text"
import SearchInput from "./SearchInput"

const HeaderTextBox = ({ handleSubmit, getSearchTerm }) => {
  
  return (
    <div className="container">
      <div className="header-text--box">
        <Text variant="title3" color="indigo-color" className="header-text--box--text-medium">Restaurant
          FAQs</Text>
        <Text color="indigo-color" className="header-text--box--text-small">
          Find answers to all your Orderspot related questions here, Can’t find an answer? Drop us a message at the
          bottom of the page and we’ll get back to you soon!
        </Text>
        <div className="header-text--box--search-bar">
          <SearchInput handleSubmit={handleSubmit} getSearchTerm={getSearchTerm} />
        </div>
      </div>
    </div>
  )
}

HeaderTextBox.propTypes = {}

export default HeaderTextBox
