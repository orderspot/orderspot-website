import React from "react"
import styled from "styled-components"
import orangeCheckIcon from "../../assets/ic-oragne-check.png"

const PlanSection = () => {
  return (
    <PlanSectionContainer id="planSectionContainer">
      <div className="container">
        <PlanItemContainer>
          <PlanItemWrapper>
            <PlanItem>
              <PlanNameSection>
                <PlanCost>FREE</PlanCost>
                <PlanName style={{ color: "#FFBF39" }}>Starter Plan</PlanName>
                <PlanDescription>
                  Our basic starter plan designed to keep your business from
                  paying large commission fees to 3rd parties.
                </PlanDescription>
              </PlanNameSection>
              <PlanInfoSection>
                <PlanInfoList>
                  <PlanInfoListItem>
                    <PlanInfoText>Commission Free</PlanInfoText>
                  </PlanInfoListItem>
                  <PlanInfoListItem>
                    <PlanInfoText>Diners pay 2.5% Ordering Fee</PlanInfoText>
                  </PlanInfoListItem>
                  <PlanInfoListItem>
                    <PlanInfoText>Online Ordering</PlanInfoText>
                  </PlanInfoListItem>
                  <PlanInfoListItem>
                    <PlanInfoText>SMS Receipts</PlanInfoText>
                  </PlanInfoListItem>
                  <PlanInfoListItem>
                    <PlanInfoText>Self Serve</PlanInfoText>
                  </PlanInfoListItem>
                </PlanInfoList>
              </PlanInfoSection>
            </PlanItem>
          </PlanItemWrapper>

          <PlanItemWrapper>
            <PlanItem>
              <PlanNameSection>
                <PlanCost>$69/month</PlanCost>
                <PlanName style={{ color: "#1DABF2" }}>Growth Plan</PlanName>
                <PlanDescription>
                  We give you access to tools and data to help grow your
                  business. Re-engage your customers with loyalty and
                  promotions!
                </PlanDescription>
              </PlanNameSection>
              <PlanInfoSection>
                <PlanInfoList>
                  <PlanInfoListItem>
                    <PlanInfoText>Everything in Starter Plan</PlanInfoText>
                  </PlanInfoListItem>
                  <PlanInfoListItem>
                    <PlanInfoText>Loyalty and Promotional Tools</PlanInfoText>
                  </PlanInfoListItem>
                  <PlanInfoListItem>
                    <PlanInfoText>Basic Reports</PlanInfoText>
                  </PlanInfoListItem>
                  <PlanInfoListItem>
                    <PlanInfoText>Marketing Tools</PlanInfoText>
                  </PlanInfoListItem>
                  <PlanInfoListItem>
                    <PlanInfoText>24/7 SMS and Email Support</PlanInfoText>
                  </PlanInfoListItem>
                </PlanInfoList>
              </PlanInfoSection>
            </PlanItem>
          </PlanItemWrapper>

          <PlanItemWrapper>
            <PlanItem>
              <PlanNameSection>
                <PlanCost>$299/month</PlanCost>
                <PlanName style={{ color: "#250E7D" }}>
                  Enterprise Plan
                </PlanName>
                <PlanDescription>
                  For hotels, food halls, or businesses with multiple
                  locations–- we’ll customize the setup for what your business
                  needs.
                </PlanDescription>
              </PlanNameSection>
              <PlanInfoSection>
                <PlanInfoList>
                  <PlanInfoListItem>
                    <PlanInfoText>Everything in Growth Plan</PlanInfoText>
                  </PlanInfoListItem>
                  <PlanInfoListItem>
                    <PlanInfoText>Customized Set Up</PlanInfoText>
                  </PlanInfoListItem>
                  <PlanInfoListItem>
                    <PlanInfoText>Customized Reporting</PlanInfoText>
                  </PlanInfoListItem>
                  <PlanInfoListItem>
                    <PlanInfoText>Automated Marketing</PlanInfoText>
                  </PlanInfoListItem>
                  <PlanInfoListItem>
                    <PlanInfoText>Dedicated AE</PlanInfoText>
                  </PlanInfoListItem>
                </PlanInfoList>
              </PlanInfoSection>
            </PlanItem>
          </PlanItemWrapper>
        </PlanItemContainer>
      </div>
    </PlanSectionContainer>
  )
}

const PlanSectionContainer = styled.section`
  padding-top: 160px;
  padding-bottom: 160px;
  background-color: rgba(250, 250, 250);

  @media screen and (max-width: 991px) {
    padding-top: 60px;
    padding-bottom: 60px;
  }

  &.os-hide {
    display: none;
  }
`

const PlanItemContainer = styled.div`
  font-size: 0;
  margin-left: -20px;
  margin-right: -20px;
`
const PlanItemWrapper = styled.div`
  display: inline-block;

  &:nth-child(even) > div {
    background-color: #fff;
  }

  @media screen and (max-width: 991px) {
    width: 100%;
    max-width: 330px;
    display: block;
    margin: 0 auto 24px;
  }

  @media screen and (min-width: 992px) {
    width: 33.333%;
    padding: 0 16px;
  }
  @media screen and (min-width: 1200px) {
    width: 33.333%;
    padding: 0 20px;
  }
`

const PlanItem = styled.div`
  border-radius: 16px;
  box-shadow: 0 10px 20px rgba(0, 0, 0, 0.19), 0 6px 6px rgba(0, 0, 0, 0.23);

  @media screen and (max-width: 991px) {
    padding: 0 26px;
  }

  @media screen and (min-width: 992px) {
    padding: 0 18px;
  }
  @media screen and (min-width: 1200px) {
    padding: 0 26px;
  }
`

const PlanNameSection = styled.div`
  padding-bottom: 16px;
  border-bottom: 1px solid #ebeced;

  @media screen and (max-width: 991px) {
    padding-top: 68px;
  }

  @media screen and (min-width: 992px) {
    padding-top: 52px;
  }
  @media screen and (min-width: 1200px) {
    padding-top: 82px;
  }
`

const PlanInfoSection = styled.div`
  padding-top: 40px;
  padding-bottom: 46px;
  @media screen and (min-width: 992px) {
    padding-top: 32px;
    padding-bottom: 36px;
  }
`

const PlanInfoList = styled.ul``
const PlanInfoListItem = styled.li`
  margin-bottom: 20px;
  padding-left: 30px;
  background-image: url(${orangeCheckIcon});
  background-repeat: no-repeat;
  background-size: 18px 14.59px;
  background-position: left center;
  &:last-of-type {
    margin-bottom: 0;
  }
`
const PlanInfoText = styled.p`
  font-size: 16px;
  margin: 0;
  color: #6e798c;
`

const PlanCost = styled.h1`
  font-size: 48px;

  @media screen and (min-width: 992px) {
    font-size: 44px;
  }
`

const PlanName = styled.h2`
  font-size: 28px;
  margin-top: 20px;

  @media screen and (min-width: 992px) {
    font-size: 26px;
    margin-top: 16px;
  }
`

const PlanDescription = styled.p`
  margin-top: 10px;
  font-size: 16px;
  line-height: 25px;
  max-width: 253px;
  color: #92969d;
  @media screen and (min-width: 992px) {
    margin-top: 8px;
    font-size: 14px;
  }
`

export default PlanSection
