import React from "react"
import LoginFormAndLoginGoogle from "../Solutions/LoginFormAndLoginGoogle"
import { graphql, useStaticQuery } from "gatsby"
import BackgroundImage from "gatsby-background-image"

const SignupSection = () => {
  const data = useStaticQuery(query)
  
  return (
    <BackgroundImage
      className="signup-section"
      Tag="div"
      fluid={data.restaurantImgFile.restaurantImg.fluid}
      backgroundColor={`#040e18`}
    >
      <div className="signup-section--overlay" />
      <LoginFormAndLoginGoogle />
    </BackgroundImage>
  )
}

SignupSection.propTypes = {}

const query = graphql`
      query {
        restaurantImgFile:file(relativePath: {eq: "restaurant-img.png"}){
         restaurantImg:childImageSharp{
            fluid{
          ...GatsbyImageSharpFluid
        }
      }
    }
    }`

export default SignupSection
