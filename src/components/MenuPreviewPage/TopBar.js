import React from 'react';
import Text from "../common/Text/Text";
import cartIcon from '../../assets/icons/cart-black.svg';

const TopBar = ({ title }) => {
  return (
    <section className="top-bar hide-on-mobile">
      <Text variant="body4" color="black-text" className="w-700" align="center">{title}</Text>
      <div className="simple-cart-wrapper">
        <Text className="simple-cart-wrapper--text rm-2 w-700" variant="body4">Cart</Text>
        <div className="simple-cart-icon-wrapper">
          <img className="simple-cart-icon" src={cartIcon} alt="cart"/>
        </div>
      </div>
    </section>
  );
};

TopBar.propTypes = {};

export default TopBar;
