import React from "react"
import StoreProductItem from "./StoreProductItem"
import Text from "../common/Text/Text"
import { graphql, useStaticQuery } from "gatsby"

const CategoryAndProductSections = () => {
  const data = useStaticQuery(query)
  return (
    <section className="category-and-product-section">
      <div className="container">
        <div className="category-items-wrapper">
          <div className="category-wrapper">
            <span className="category active">Main Dishes</span>
            <span className="category">Desserts</span>
            <span className="category">Sides</span>
            <span className="category">Extra</span>
          </div>
        </div>
        <div className="product-wrapper">
          <Text className="product-wrapper--title">Main Dishes</Text>
          <div className="row">
            <div className="col-md-6">
              <StoreProductItem
                price={"15.00"}
                imageUrl={data.food1File.food1.fluid}
                description={"Lorem ipsum dolor sit amet, consecte tur adipiscing elit, sed do eiusmod tem por incididunt ut labore et dolore magn a aliqua. Ut enim ad minim veniam, quis"}
                name={"Cheese Bacon Burger"}
              />
            </div>
            <div className="col-md-6">
              <StoreProductItem
                price={"15.00"}
                imageUrl={data.food2File.food2.fluid}
                description={"Lorem ipsum dolor sit amet, consecte tur adipiscing elit, sed do eiusmod tem por incididunt ut labore et."}
                name={"Poached Egg & Avocado Salad"}
              />
            </div>
            <div className="col-md-6">
              <StoreProductItem
                price={"15.00"}
                imageUrl={data.food3File.food3.fluid}
                description={"Lorem ipsum dolor sit amet, consecte tur adipiscing elit, sed do eiusmod tem por incididunt ut labore et dolore magn a aliqua. Ut enim ad minim veniam, quis"}
                name={"Eggs in the Hell"}
              />
            </div>
            <div className="col-md-6">
              <StoreProductItem
                price={"15.00"}
                imageUrl={data.food4File.food4.fluid}
                description={"Lorem ipsum dolor sit amet, consecte tur adipiscing elit, sed do eiusmod tem por incididunt ut labore et dolore magn a aliqua. Ut enim ad minim veniam, quis"}
                name={"Salad Pizza"}
              />
            </div>
          </div>
        </div>
      </div>
    </section>
  )
}

CategoryAndProductSections.propTypes = {}

const query = graphql`
      query {
        food1File:file(relativePath: {eq: "temp/food-1.png"}){
         food1:childImageSharp{
            fluid{
          ...GatsbyImageSharpFluid
        }
      }
    }
     food2File:file(relativePath: {eq: "temp/food-2.png"}){
         food2:childImageSharp{
            fluid{
          ...GatsbyImageSharpFluid
        }
      }
    }
     food3File:file(relativePath: {eq: "temp/food-3.png"}){
         food3:childImageSharp{
            fluid{
          ...GatsbyImageSharpFluid
        }
      }
    }
     food4File:file(relativePath: {eq: "temp/food-4.png"}){
         food4:childImageSharp{
            fluid{
          ...GatsbyImageSharpFluid
        }
      }
    }
      }
    `

export default CategoryAndProductSections
