import React, { useContext } from "react"
import { Context as StoreContext } from "../../context/StoreContext"
import Layout from "../Layout"
import TopBar from "./TopBar"
import StoreVendorHeader from "./VendorHeader"
import CategoryAndProductSections from "./CategoryAndProductSections"
import HowToGetStartedSection from "../common/HowToGetStartedSection/HowToGetStartedSection"
import SignupSection from "./SignupSection"
import OrderspotResources from "../HomePage/OrderspotResources"
import StoreCreationLoadingScreen from "../common/StoreCreationLoadingScreen/StoreCreationLoadingScreen"

const initialState = { state: { storeData: {} } }
const MenuPreviewPage = ({ menuPreviewPageHeaderBgImg }) => {
  const [loading, setLoading] = React.useState(true)
  const [progress, setProgress] = React.useState(0)
  const { state: { storeData } } = useContext(StoreContext) || initialState
  const { restaurantName } = storeData
  React.useEffect(() => {
    const interval = setInterval(() => {
      setProgress((prev) => prev + 8.34)
    }, 200)
    setTimeout(() => {
      clearInterval(interval)
    }, 2500)
    
    setTimeout(() => {
      setLoading(false)
    }, 3000)
    
    //  eslint-disable-next-line
  }, [])
  
  const getClassName = () => {
    if ( progress > 50 ) return "green-bg"
    return "blue-bg"
  }
  
  
  if ( loading ) return <StoreCreationLoadingScreen getClassName={getClassName} progress={progress} />
  return (
    <Layout className="menu-preview" isHeaderFixed={true}>
      <TopBar title={restaurantName || "Orderspot Test Kitchen"} />
      <StoreVendorHeader
        description="We are renowned for our fresh ingredients and friendly atmosphere"
        backgroundImage={menuPreviewPageHeaderBgImg}
        restaurantName={restaurantName || "Orderspot Test Kitchen"}
      />
      <CategoryAndProductSections />
      <HowToGetStartedSection />
      <SignupSection />
      <OrderspotResources />
    </Layout>
  )
}

MenuPreviewPage.propTypes = {}

export default MenuPreviewPage
