import React from "react"
import PropTypes from "prop-types"
import Text from "../common/Text/Text"
import Img from "gatsby-image"
/*
 * AVAILABLE PROPS
 * name
 * price
 * description (sliced description 20 characters)
 * onClick
 * imageUrl
 * */

const StoreProductItem = ({ name, price, description, imageUrl }) => {
  return (
    <div className="store-product-item">
      <div className="store-product-item--horizontal-line hide-on-tablet" />
      <div className="store-product-item--img-wrapper center-align-vh">
        <div className="food-img-wrapper">
          <Img fluid={imageUrl} alt="img" />
        </div>
      </div>
      <div className="store-product-item--text-box lm-6">
        <div>
          <Text className="store-product-item--name w-700" variant="body6">
            {name}
          </Text>
          <Text
            className="store-product-item--description w-300"
            color="black-text"
            variant="body9"
          >
            {description}
          </Text>
        </div>
        <div>
          <Text color="black-text" className="w-700" variant="body6">
            $ {price}
          </Text>
        </div>
      </div>
    </div>
  )
}

StoreProductItem.propTypes = {
  name: PropTypes.string.isRequired,
  price: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  imageUrl: PropTypes.string.isRequired
}

export default StoreProductItem
