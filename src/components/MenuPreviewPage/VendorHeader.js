import React from "react"
import PropTypes from "prop-types"
import Text from "../common/Text/Text"
import whiteLogo from "../../assets/small-white-logo.svg"
import BackgroundImage from "gatsby-background-image"
/*
* AVAILABLE PROPS
* restaurantName
* description
* backgroundImage
* */

const StoreVendorHeader = ({ restaurantName, description, backgroundImage }) => {
  return (
    
    <BackgroundImage
      className='store-vendor-header'
      Tag="div"
      fluid={backgroundImage}
      backgroundColor={"#C0C0C0"}
    >
      <div className="store-vendor-header--backdrop" />
      <div className='vendor-header-text-box'>
        <Text className="main-text text-shadow-1" color="white-text" variant="title4">Welcome to</Text>
        <Text className="main-text-2 text-shadow-1" variant="title4">{restaurantName}</Text>
        <Text className="description-text text-shadow-1 tm-7 mb-2" variant="body6" color="white-text">
          {description}
        </Text>
      </div>
      
      {/*white logo*/}
      <div className="powered-by-os">
        <Text color="white-text" variant="body9" className="w-700">Powered By</Text>
        <img src={whiteLogo} className="horizontal-logo-white" alt="white-logo" />
      </div>
    </BackgroundImage>
  )
}

StoreVendorHeader.propTypes = {
  restaurantName: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  backgroundImage: PropTypes.string.isRequired
}

export default StoreVendorHeader
