import React from "react"
import Text from "../common/Text/Text"
import { Link } from "gatsby"
import arrowIconBold from "../../assets/icons/arrow-icon-blue-bold.svg"
import moment from "moment"

const BlogSingleTextBox = ({ style, slug, title, subTitleBlack, publishDate, readTime }) => {
  return (
    <div style={style || {}} className="blog-single--text-box">
      <div className="text-wrapper">
        <Text className="blog-single--title w-700" color="indigo-color" variant="title3">{title}</Text>
        <div className="date-time-box">
          <Text className="date-time-box--text">{moment(publishDate).format("MMM D, YYYY")}</Text>
          {readTime && ( <>
              <div className="date-time-box--dot" />
              <Text className="date-time-box--text">{readTime} min</Text>
            </>
          )}
        </div>
        <Text className="blog-single--detail w-300" color="indigo-color" variant="body4">{subTitleBlack}</Text>
      </div>
      <Link to={`/blog/${slug}`} className="blog-single-btn">
        <img src={arrowIconBold} alt="arrow-icon-blue" className="blog-single--icon" />
      </Link>
    </div>
  )
}

BlogSingleTextBox.propTypes = {}

export default BlogSingleTextBox
