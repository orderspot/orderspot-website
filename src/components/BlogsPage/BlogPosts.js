import React, { useState, useEffect } from 'react';
import BlogTItleBlock from "./BlogTItleBlock";
import BlogSingle from "./BlogSingle";
import { Link } from 'gatsby';
import { handleGetBgColor } from "../../constants/commonUtils";

const BlogPosts = ({ category, allBlogPosts }) => {
  const [state, setState] = useState({
    postsOfThisCategory: [],
    firstTwoPostsOfThisCategory: []
  });
  const { postsOfThisCategory, firstTwoPostsOfThisCategory } = state;
  
  const {
    slug,
    categoryText,
    categoryTitle
  } = category.node;
  
  useEffect(() => {
    const filteredPosts = allBlogPosts.filter((blog) => blog.node.category.slug === slug);
    const firstTwoPostsOfThisCategory = filteredPosts.slice(0, 2);
    setState({ ...state, postsOfThisCategory: filteredPosts, firstTwoPostsOfThisCategory });
  }, []);
  if ( postsOfThisCategory.length === 0 ) return null;
  return (
    <section className="blog-posts">
      <BlogTItleBlock
        title={categoryTitle}
        text={categoryText}
        style={handleGetBgColor()}
      />
      <div className="row">
        {firstTwoPostsOfThisCategory.map((post) => {
          return <BlogSingle key={post.node.slug} post={post}/>;
        })}
      </div>
      {postsOfThisCategory.length > 2 &&
      <div className="see-more-wrapper">
        <Link to={`/category/${slug}`} variant="body7" className="see-more-wrapper--text w-700">See More</Link>
      </div>
      }
    </section>
  );
};

BlogPosts.propTypes = {};

export default BlogPosts;
