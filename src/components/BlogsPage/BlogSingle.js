import React from "react"
import BlogSingleTextBox from "./BlogSingleTextBox"
import _get from "lodash/get"
import { graphql, useStaticQuery } from "gatsby"
import BackgroundImage from "gatsby-background-image"

const BlogSingle = ({ post }) => {
  const { featuredImage, title, slug, subTitleBlack, publishDate, readTime } = post.node
  const data = useStaticQuery(query)
  const blogImg = _get(featuredImage, "fluid", data.qrBgImageFile.qrBgImage.fluid)
  return (
    <div className="col-xl-6 col-lg-6 col-md-6 blog-single--container">
      <div className="blog-single--wrapper">
        <div className="blog-single--img-wrapper">
          <BackgroundImage
            Tag="section"
            className="blog-single--img-box"
            fluid={blogImg}
            backgroundColor={`#040e18`}
          />
        </div>
        
        <div className="blog-single-text-wrapper">
          <BlogSingleTextBox
            subTitleBlack={subTitleBlack}
            publishDate={publishDate}
            title={title}
            slug={slug}
            readTime={readTime}
          />
        </div>
      </div>
    </div>
  )
}

const query = graphql`
  query{
    qrBgImageFile:file(relativePath: {eq: "temp/bg.jpg"}){
      qrBgImage:childImageSharp{
        fluid{
          ...GatsbyImageSharpFluid
        }
      }
    }
  }
`

BlogSingle.propTypes = {}

export default BlogSingle
