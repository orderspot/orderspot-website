import React from 'react';
import Text from "../common/Text/Text";
import PropTypes from 'prop-types';

const BlogTItleBlock = ({ text, title, style }) => {
  
  return (
    <div className="row">
      <div className="col-md-12 blog-title-block--wrapper">
        <div style={style} className="blog-title-box">
          <Text className="blog-title-box--main-text" variant="title4" color="indigo-color">{title}</Text>
          <Text className="blog-title-box--sub-text w-400" variant="body7" color="indigo-3">{text}</Text>
        </div>
      </div>
    </div>
  );
};

BlogTItleBlock.propTypes = {
  bgColor: PropTypes.string,
  text: PropTypes.string.isRequired,
};

export default BlogTItleBlock;
