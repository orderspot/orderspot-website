import React from 'react';
import BlogSingle from "./BlogSingle";
import { useMediaQuery } from "react-responsive";

const PostsForMobileAndTablet = ({ allBlogPosts }) => {
  const isSmallDevive = useMediaQuery({ maxWidth: 950 });
  const renderContent = (post, index) => {
    if ( isSmallDevive ) return <BlogSingle key={post.node.slug} post={post}/>;
    if ( index === 0 ) return <BlogSingle key={post.node.slug} post={post} blogVersion={1}/>;
    if ( index === 1 ) return <BlogSingle key={post.node.slug} post={post} blogVersion={2}/>;
    return <BlogSingle key={post.node.slug} post={post}/>;
  };
  return (
    <section className="row">
      {allBlogPosts.map((post, index) => {
        return renderContent(post, index);
      })}
    </section>
  );
};

PostsForMobileAndTablet.propTypes = {};

export default PostsForMobileAndTablet;
