import React from 'react';
import BlogPosts from "./BlogPosts";

const AllBlogs = ({ allBlogPosts, allCategories }) => {
  return (
    <div className="container">
      <section className="all-blogs">
        <div className="container">
            {allCategories.map((category, index) => <BlogPosts
            key={category.node.slug}
            category={category}
            index={index}
            allBlogPosts={allBlogPosts}/>
          )}
        </div>
      </section>
    </div>
  );
};

AllBlogs.propTypes = {};

export default AllBlogs;
