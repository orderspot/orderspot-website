import React, { useContext } from "react"
import Navbar from "./common/Navbar/Navbar"
import Footer from "./common/Footer/Footer"
import PropTypes from "prop-types"
import SEO from "./common/seo"
import OsDialog from "./common/OsDialog/OsDialog"
import LoginFormAndLoginGoogle from "./Solutions/LoginFormAndLoginGoogle"
import AlertDialog from "./common/AlertDialog/AlertDialog"
import DialogContext from "../context/DialogContext"
import AlertDialogContext from "../context/AlertDialogContext"
// import LaunchStoreBtn from "./common/LaunchStoreBtn/LaunchStoreBtn"

const Layout = ({ isHeaderFixed, children, className }) => {
  const useDialog = useContext(DialogContext) || {}
  const useAlert = useContext(AlertDialogContext) || {}
  const closeModal = useDialog.actions ? useDialog.actions.closeModal : {}
  const isModalVisible = useDialog.state ? useDialog.state.isModalVisible : {}
  const closeDialog = useAlert.actions ? useAlert.actions.closeDialog : {}
  const isDialogOpen = useAlert.state ? useAlert.state.isDialogOpen : {}
  return (
    <div className={className + (isHeaderFixed ? " is-header-fixed" : "")}>
      <SEO />
      {/* <LaunchStoreBtn /> */}
      <OsDialog
        className="login-google-form-dialog"
        handleClose={closeModal}
        isOpen={isModalVisible}
      >
        <LoginFormAndLoginGoogle
          title="Ready to drive online sales?"
          subText="Unsure what to expect? Don’t worry our restaurant success manager will reach out via email within 1-2 business days!"
        />
      </OsDialog>
      <AlertDialog handleClose={closeDialog} isOpen={isDialogOpen} />
      <Navbar/>
      {children}
      <Footer />
    </div>
  )
}

Layout.propTypes = {
  isHeaderFixed: PropTypes.bool,
  children: PropTypes.node.isRequired,
  className: PropTypes.string,
}

Layout.defaultProps = {
  isHeaderFixed: false,
}

export default Layout
