import React from 'react';
import PropTypes from 'prop-types';
import InputField from "../InputField/InputField";
import Button from "../Button/Button";

const SingleInputField = (props) => {
  const {
    btnClassName,
    btnText,
    inputName,
    inputValue,
    inputId,
    inputPlaceholder,
    handleInputChange,
    wrapperClassName
  } = props;
  const cn = wrapperClassName ? `single-input-field ${wrapperClassName}` : "single-input-field";
  return (
    <section className={cn}>
      <InputField
        value={inputValue}
        name={inputName}
        placeholder={inputPlaceholder}
        id={inputId}
        handleChange={handleInputChange}
      />
      <div className="single-input-field--btn-wrapper">
        <Button className={btnClassName} text={btnText}/>
      </div>
    </section>
  );
};

SingleInputField.propTypes = {
  btnClassName: PropTypes.string,
  btnText: PropTypes.string.isRequired,
  inputName: PropTypes.string.isRequired,
  inputValue: PropTypes.string.isRequired,
  inputId: PropTypes.string.isRequired,
  inputPlaceholder: PropTypes.string.isRequired,
  handleInputChange: PropTypes.func.isRequired,
  wrapperClassName: PropTypes.string.isRequired,
};

export default SingleInputField;
