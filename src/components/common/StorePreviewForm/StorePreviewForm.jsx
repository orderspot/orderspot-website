import React, { useContext } from 'react';
import { Formik } from 'formik';
import { Context as StoreContext } from '../../../context/StoreContext';
import { navigate } from 'gatsby';
import * as Yup from 'yup';

import Text from "../Text/Text";
import InputField from "../InputField/InputField";
import Button from "../Button/Button";
import { MENU_PREVIEW_PAGE } from "../../../constants/constants";

import { gtagPluginEventTracker } from "../../../utils/googleAnalyticsHelper";
import {ENUM_EVENT_CATEGORIES, ENUM_EVENT_ACTIONS} from "../../../constants/enumEvents";

const emptyObject = {};
const StorePreviewForm = ({ notShowTitle }) => {
  const { handleSubmit } = useContext(StoreContext) || emptyObject;
  
  const handleNavigateUser = () => {
    navigate(MENU_PREVIEW_PAGE);
  };
  
  const storePreviewFormSchema = Yup.object().shape({
    restaurantName: Yup.string()
      .required('Enter your Restaurant Name'),
    email: Yup.string()
      .email()
      .required("Enter your Email")
  });

  const handleSubmitWithEvent = (values, handleNavigateUser) => {
    const {restaurantName, email} = values;
    gtagPluginEventTracker({
      category: ENUM_EVENT_CATEGORIES.MAIN_PAGE,
      action: ENUM_EVENT_ACTIONS.CLICK,
      label: `Preview_${restaurantName}_${email}`
    })
    handleSubmit(values, handleNavigateUser);
  }
  
  return (
    <Formik
      initialValues={{ restaurantName: "", email: "" }}
      onSubmit={(values) => handleSubmitWithEvent(values, handleNavigateUser)}
      validationSchema={storePreviewFormSchema}
    >
      {
        ({
           values,
           handleChange,
           handleSubmit,
           errors
         }) => {
          const { restaurantName, email } = values;
          
          return (
            <form onSubmit={handleSubmit} className="store-preview-form">
              {!notShowTitle &&
              <Text variant="body4" className="form-wrapper--title w-700 bm-4" color="indigo-color">
                Preview my online menu
              </Text>
              }
              <div className="input-wrapper">
                <InputField
                  label="Your Restaurant Name"
                  id="restaurantName"
                  value={restaurantName}
                  name="restaurantName"
                  placeholder="Stone Bar & Grill"
                  handleChange={handleChange}
                  helperText={errors.restaurantName}
                  error={Boolean(errors.restaurantName)}
                  type="text"
                  isHotjarAllow
                />
              </div>
              
              <div className="input-wrapper">
                <InputField
                  label="Email"
                  id="email"
                  value={email}
                  name="email"
                  placeholder="Enter email address"
                  handleChange={handleChange}
                  helperText={errors.email}
                  error={Boolean(errors.email)}
                  type={email}
                  isHotjarAllow
                />
              </div>
              <div className="form-wrapper--btn">
                <Button
                  className="full-width-btn"
                  text="Preview Menu"
                  type="submit"
                />
              </div>
            </form>
          );
        }
      }
    </Formik>
  );
};

StorePreviewForm.propTypes = {};

export default StorePreviewForm;
