import React, { useContext } from "react"
import Modal from "react-bootstrap/Modal"
import PropTypes from "prop-types"
import AlertDialogContext from "../../../context/AlertDialogContext"
import Button from "../Button/Button"

const AlertDialog = ({ handleClose }) => {
  const useAlert = useContext(AlertDialogContext) || {}

  const isDialogOpen = useAlert.state ? useAlert.state.isDialogOpen : {}
  const dialogText = useAlert.state ? useAlert.state.dialogText : {}

  return (
    <Modal
      size="sm"
      show={isDialogOpen}
      onHide={handleClose}
      id="alertDialogModal"
    >
      <Modal.Header closeButton></Modal.Header>
      <Modal.Body>
        <p>{dialogText}</p>
        <Button onClick={handleClose} text="OK" />
      </Modal.Body>
    </Modal>
  )
}

AlertDialog.propTypes = {
  handleClose: PropTypes.func.isRequired,
}

AlertDialog.defaultProps = {
  dialogText: "We're in progress!",
}

export default AlertDialog
