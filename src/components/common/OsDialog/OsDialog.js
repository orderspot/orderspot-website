import React, { useContext } from "react"
import Modal from "react-bootstrap/Modal"
import PropTypes from "prop-types"
import crossIconBlue from "../../../assets/icons/cross-icon-blue.svg"
import DialogContext from "../../../context/DialogContext"

const OsDialog = ({ children, handleClose, className }) => {
  const useDialog = useContext(DialogContext) || {}
  const isModalVisible = useDialog.state ? useDialog.state.isModalVisible : {}
  const cn = className ? `os-dialog ${className}` : "os-dialog"
  return (
    <Modal className={cn} show={isModalVisible} onHide={handleClose}>
      <button onClick={handleClose} className="os-dialog--close-icon">
        <img src={crossIconBlue} alt="close" />
      </button>
      {children}
    </Modal>
  )
}

OsDialog.propTypes = {
  children: PropTypes.node.isRequired,
  handleClose: PropTypes.func.isRequired,
  className: PropTypes.string,
}

export default OsDialog
