import React from "react"
import PropTypes from "prop-types"
import Form from "react-bootstrap/Form"
import styled from "styled-components"

const InputField = props => {
  const {
    id,
    label,
    type,
    placeholder,
    helperText,
    handleChange,
    value,
    name,
    error,
    isRequired,
    isSolution,
    isHotjarAllow,
  } = props

  const cn = error ? "input-field error" : "input-field"
  return (
    <Form.Group className={cn} controlId={id}>
      <Form.Label className={isSolution ? "is-solution" : ""}>
        {label} {isRequired ? <LableRequiredIcon>*</LableRequiredIcon> : ""}
      </Form.Label>
      <Form.Control
        name={name}
        value={value}
        onChange={handleChange}
        type={type}
        placeholder={placeholder}
        required={isRequired}
        data-hj-allow={isHotjarAllow}
      />
      {helperText && (
        <Form.Text className="helper-text">{helperText}</Form.Text>
      )}
    </Form.Group>
  )
}

const LableRequiredIcon = styled.span`
  color: #ff5a5a;
  position: absolute;
  top: -2px;
`

const RequiredLabel = styled(Form.Label)`
  font-size: 1.8rem !important;
  color: #2c2c46 !important;
`

InputField.propTypes = {
  id: PropTypes.string.isRequired,
  label: PropTypes.string,
  placeholder: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  type: PropTypes.string,
  helperText: PropTypes.node,
  handleChange: PropTypes.func.isRequired,
  name: PropTypes.string.isRequired,
  isRequired: PropTypes.bool,
  isSolution: PropTypes.bool,
  isHotjarAllow: PropTypes.bool,
}

InputField.defaultProps = {
  type: "text",
  helperText: "",
  disabled: false,
  isRequired: false,
  isSolution: false,
}

export default InputField
