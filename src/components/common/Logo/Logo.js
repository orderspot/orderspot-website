import React from "react"
import PropTypes from "prop-types"
import { graphql, useStaticQuery } from "gatsby"
import Img from "gatsby-image"
import blueHorizontalLogo from "../../../assets/orderspot-logo@2x.png"

const Logo = ({ logo, alt }) => {
  const data = useStaticQuery(query)
  const getLogo = (logo) => {
    switch (logo) {
      case "os-horizontal-white":
        return data.horizontalLogoWhiteFile.horizontalLogoWhite.fluid
      case "hcm-logo":
        return data.hcmLogoFile.hcmLogo.fluid
      case "emc-logo":
        return data.emcLogoFile.emcLogo.fluid
      case "firewings-logo":
        return data.firewingsLogoFile.firewingsLogo.fluid
      case "la-logo":
        return data.laLogoFile.laLogo.fluid
      default:
        return data.logoHorizontalFile.logoHorizontal.fluid
    }
  }
  
  return (
    <div className={`${getLogoClassName(logo)}`}>
      {logo ?
        <Img
          fluid={getLogo(logo)}
          alt="Orderspot"
        />
        : <img src={blueHorizontalLogo} alt="Orderspot" />}
    </div>
  )
}

const getLogoClassName = (logo) => {
  switch (logo) {
    case "os-horizontal-white":
      return "os-logo-horizontal-white"
    case "hcm-logo":
      return "hcm-logo"
    case "emc-logo":
      return "emc-logo"
    case "firewings-logo":
      return "firewings-logo"
    case "la-logo":
      return "la-logo"
    default:
      return "logo-horizontal"
  }
}

Logo.propTypes = {
  logo: PropTypes.string,
  size: PropTypes.string,
  alt: PropTypes.string
}

const query = graphql`
       query {
    logoHorizontalFile:file(relativePath: { eq: "orderspot-logo@2x.png" }) {
      logoHorizontal: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    hcmLogoFile:file(relativePath: { eq: "logos/hcm-logo.png" }) {
      hcmLogo: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    emcLogoFile:file(relativePath: { eq: "logos/emc-logo.png" }) {
      emcLogo: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    firewingsLogoFile:file(relativePath: { eq: "logos/firewings-logo.png" }) {
      firewingsLogo: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    laLogoFile:file(relativePath: { eq: "logos/la-logo.png" }) {
      laLogo: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    horizontalLogoWhiteFile:file(relativePath: { eq: "logos/horizontal-logo-white.png" }) {
      horizontalLogoWhite: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
  }
    `

export default Logo
