import React from "react"
import Modal from "react-modal"
import styled from "styled-components"

import { YOUTUBE_LINK } from "../../../constants/constants"

const customStyles = {
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
    padding: 0,
    backgroundColor: "none",
    overflow: "visible",
  },
}

const VideoModal = ({ isOpen, closeFunc }) => {
  React.useEffect(() => {
    Modal.setAppElement("#___gatsby")
  }, [])
  return (
    <Modal
      isOpen={isOpen}
      style={customStyles}
      shouldCloseOnOverlayClick={true}
      contentLabel="Example Modal"
    >
      <CloseButton onClick={closeFunc} />
      <VideoModalContainer>
        <VideoWrapper>
          <iframe
            width="720"
            height="405"
            src="https://www.youtube.com/embed/_k7xiXtWWEk"
            frameBorder="0"
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen
          ></iframe>
        </VideoWrapper>
      </VideoModalContainer>
    </Modal>
  )
}

const CloseButton = styled.div`
  position: absolute;
  right: 0;
  top: -42px;
  width: 32px;
  height: 32px;
  opacity: 1;
  cursor: pointer;

  &:before,
  &:after {
    position: absolute;
    left: 15px;
    content: " ";
    height: 33px;
    width: 2px;
    background-color: #fff;
  }

  &:before {
    transform: rotate(45deg);
  }
  &:after {
    transform: rotate(-45deg);
  }
`

const VideoModalContainer = styled.div`
  width: 80vw;
`

const VideoWrapper = styled.div`
  position: relative;
  padding-bottom: 56.25%;
  padding-top: 25px;
  height: 0;

  iframe {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
  }
`

export default VideoModal
