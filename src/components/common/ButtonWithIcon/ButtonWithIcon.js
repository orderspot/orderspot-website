import React from 'react';
import Text from "../Text/Text";

const ButtonWithIcon = ({ icon, text, textClassName, onClick }) => {
  
  const cn = textClassName ? `${textClassName} w-700` : "w-700";
  return (
    <button className="play-btn" onClick={onClick}>
      <img className="play-icon" src={icon} alt={"play"}/>
      <Text variant="body7" className={cn} color="indigo-color">{text}</Text>
    </button>
  );
};

ButtonWithIcon.propTypes = {};

export default ButtonWithIcon;
