import React from 'react';
import Text from "../Text/Text";
import dotsImg1 from "../../../assets/left-side-dots.svg";
import dotsImg2 from "../../../assets/right-side-dots.svg";
import PropTypes from 'prop-types';

const InfoBlock = (props) => {
  const {
    hideBgDots,
    blockBgColor,
    mainTextColor,
    subTextColor,
    mainText,
    subText
  } = props;
  return (
    <div style={{ backgroundColor: blockBgColor }} className="info-block">
      <Text variant="title1" className="info-block--text" color={mainTextColor}>{mainText}</Text>
      <Text variant="body2" className="info-block--text small-text w-500" color={subTextColor}>
        {subText}
      </Text>
      
      {!hideBgDots && (
        <>
          <img className="info-block-img1" src={dotsImg1} alt="dots"/>
          <img className="info-block-img2" src={dotsImg2} alt="dots"/>
        </>
      )}
    </div>
  );
};

InfoBlock.propTypes = {
  hideBgDots: PropTypes.bool,
  blockBgColor: PropTypes.string,
  mainTextColor: PropTypes.string,
  subTextColor: PropTypes.string,
  mainText: PropTypes.string,
  subText: PropTypes.string,
};

InfoBlock.defaultProps = {
  blockBgColor: "#EBB18C",
  mainTextColor: "indigo-color",
  subTextColor: "indigo-color",
};

export default InfoBlock;
