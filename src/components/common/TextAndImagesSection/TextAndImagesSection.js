import React from "react"
import Text from "../Text/Text"
import PropTypes from "prop-types"
import { useMediaQuery } from "react-responsive"
import { TABLET_PORTRAIT } from "../../../constants/constants"
import styled from "styled-components"
import Img from "gatsby-image"

const TextAndImagesSection = ({
                                title,
                                text,
                                lists,
                                isImageFirst,
                                imageOptions,
                                className
                              }) => {
  const isTabletPortrait = useMediaQuery({ maxWidth: TABLET_PORTRAIT })
  const isTabletLandscape = useMediaQuery({ minWidth: TABLET_PORTRAIT })
  const isPc = useMediaQuery({
    minWidth: 1200
  })
  const isTablet = useMediaQuery({
    minWidth: 768,
    maxWidth: 1199
  })
  const imgMargin = isImageFirst ? 0 : "auto"
  const renderContent = isImageFirst => {
    if ( !isImageFirst && !isTabletPortrait ) {
      return (
        <>
          <div className="text-and-images-section--text-wrapper col-lg-5 col-md-12">
            <div className="text-image--text-box">
              <Text
                variant="title1"
                color="indigo-color"
                className="text-image--title"
              >
                {title}
              </Text>
              <Text color="indigo-color" className="text-image--text w-500">
                {text}
              </Text>
              <ul className="list-text-box">
                {lists.map((list, index) => (
                  <li key={index} className="list-text-box--list-item">
                    {list}
                  </li>
                ))}
              </ul>
            </div>
          </div>
          <div className="col-lg-7 col-md-12 slide-image-wrapper">
            <ImageContainer
              data-sal="slide-down"
              data-sal-duration="900"
              data-sal-delay="100"
              data-sal-easing="ease"
              imageOptions={imageOptions}
            >
              <Img fluid={isPc ? imageOptions.pc.src : imageOptions.t.src} alt="feature-img" />
            </ImageContainer>
          </div>
        </>
      )
    }
    return (
      <>
        <div className="col-lg-7 col-md-12 slide-image-wrapper is-image-first">
          <ImageContainer
            data-sal="slide-down"
            data-sal-duration="700"
            data-sal-delay="100"
            data-sal-easing="ease"
            imageOptions={imageOptions}
          >
            <Img fluid={
              isPc
                ? imageOptions.pc.src
                : isTabletLandscape
                ? imageOptions.t.src
                : imageOptions.m.src
            }
                 alt="feature-img"
            />
          </ImageContainer>
        </div>
        <div className="col-lg-5 col-md-12 text-and-images-section--text-wrapper">
          <div className="text-image--text-box">
            <Text
              variant="title1"
              color="indigo-color"
              className="text-image--title"
            >
              {title}
            </Text>
            <Text color="indigo-color" className="text-image--text w-500">
              {text}
            </Text>
            <ul className="list-text-box">
              {lists.map((list, index) => (
                <li key={index} className="list-text-box--list-item">
                  {list}
                </li>
              ))}
            </ul>
          </div>
        </div>
      </>
    )
  }
  const cn = className
    ? `text-and-images-section ${className}`
    : "text-and-images-section"
  return (
    <section className={cn}>
      <div className="container">
        <div
          className={
            "row text-and-image-section-wrap" +
            ( isImageFirst ? " image-first" : "" )
          }
        >
          {renderContent(isImageFirst)}
        </div>
      </div>
    </section>
  )
}

const ImageContainer = styled.div`
  @media screen and (max-width: ${TABLET_PORTRAIT - 1}px) {
    width: 100%;
  }
  @media screen and (min-width: ${TABLET_PORTRAIT}px) and (max-width: 1199px) {
    width: ${props => props.imageOptions.t.width};
  }
  @media screen and (min-width: 1200px) {
    width: ${props => props.imageOptions.pc.width};
  }
`

TextAndImagesSection.propTypes = {
  title: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  lists: PropTypes.array.isRequired,
  children: PropTypes.node,
  isImageFirst: PropTypes.bool,
  imageOptions: PropTypes.object.isRequired,
  cn: PropTypes.string
}

TextAndImagesSection.defaultProps = {
  lists: []
}

export default TextAndImagesSection
