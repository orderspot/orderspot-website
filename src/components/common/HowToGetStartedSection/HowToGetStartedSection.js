import React from "react"
import Text from "../Text/Text"
import { useMediaQuery } from "react-responsive"
import { osBoxImg, selectPlanImg, signupFormImg, signupFormImgMob } from "../../../constants/images"
import { graphql, useStaticQuery } from "gatsby"
import Img from "gatsby-image"

const HowToGetStartedSection = ({ isHomepage }) => {
  const isMobile = useMediaQuery({ maxWidth: 600 })
  const features = ["No credit cards or checks", "Say bye to lock-in contracts", "Get your free QR Poster"]
  const data = useStaticQuery(query)
  return (
    <section className={isHomepage ? "how-to-get-started homepage" : "how-to-get-started not-home-page"}>
      <div className="container">
        {isHomepage ? (
          <div className="top-title">
            <Text className="top-title--text desktop-title" variant="title2" color="indigo-color">How it Works</Text>
            <Text className="top-title--text tablet-title" variant="title2" color="indigo-color">How to Get
              Started</Text>
          </div>
        ) : (
          <>
            <div className="top-title-2">
              <Text variant="title2" color="indigo-color">How to <span
                className="top-title-2--signup"><span>signup</span></span> for
                Orderspot</Text>
            </div>
            <div className="getting-started-advantages preview-page">
              {features.map(text => (
                <div key={text} className="advantage">
                  <div className="big-dot" />
                  <Text variant="body2" className="advantage--text w-400" color="indigo-color">{text}</Text>
                </div>
              ))}
            </div>
          </>
        )}
        
        <div className="row">
          <div style={style} className="col-md-4">
            <div className="step-wrapper">
              <div className="step-wrapper--img-wrapper">
                <div className="signup-form-wrapper">
                  <div className="select-plan-img">
                    <Img
                      fluid={isMobile ? data.signupFormImgMobFile.signupFormImgMob.fluid : data.signupFormImgFile.signupFormImg.fluid}
                      alt="sign up" />
                  </div>
                </div>
              </div>
              <Text variant="body2" color="indigo-color" className="step-wrapper--text w-700">
                {isMobile && "1. "} <span className="blue-text">Sign up </span>to be an Orderspot partner</Text>
            </div>
          </div>
          <div style={style} className="col-md-4">
            <div className="step-wrapper">
              <div className="step-wrapper--img-wrapper">
                <div className="select-plan-img plans-img">
                  <Img fluid={data.selectPlanImgFile.selectPlanImg.fluid} alt="select plan" />
                </div>
              </div>
              <Text variant="body2" color="indigo-color" className="step-wrapper--text w-700">
                {isMobile && "2. "} <span className="blue-text">Select the best plan </span>for your restaurant</Text>
            </div>
          </div>
          <div style={style} className="col-md-4">
            <div className="step-wrapper">
              <div className="step-wrapper--img-wrapper">
                <div className="select-plan-img box-imgs">
                  <Img fluid={data.osBoxImgFile.osBoxImg.fluid} alt="orderspot box" />
                </div>
              </div>
              <Text variant="body2" color="indigo-color" className="step-wrapper--text w-700">
                {isMobile && "3. "} <span className="blue-text">Get your Orderspot box </span>shipped to your
                door</Text>
            </div>
          </div>
        </div>
      
      </div>
    </section>
  )
}

const style = {
  display: "flex",
  alignItems: "center",
  justifyContent: "center"
}

const query = graphql`
       query {
    osBoxImgFile:file(relativePath: { eq: "home-page/orderspot-box-img.png" }) {
      osBoxImg: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
     selectPlanImgFile:file(relativePath: { eq: "home-page/pricing-img.png" }) {
      selectPlanImg: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    signupFormImgFile:file(relativePath: { eq: "home-page/signup-form-img.png" }) {
      signupFormImg: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
     signupFormImgMobFile:file(relativePath: { eq: "home-page/sign-up-form-img-mob.png" }) {
      signupFormImgMob: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    
    }`

HowToGetStartedSection.propTypes = {}

export default HowToGetStartedSection
