import React, { useContext } from "react"
import PropTypes from "prop-types"
import Text from "../Text/Text"
import { Link } from "gatsby"
import { useMediaQuery } from "react-responsive"
import Drawer from "./Drawer"
import Logo from "../Logo/Logo"
import Button from "../Button/Button"
import LinkWithDropdown from "./LinkWithDropdown"
import scrollTo from "gatsby-plugin-smoothscroll"
import {
  productLinks,
  resourcesLinks,
  solutionLinks,
} from "../../../constants/links"
import {
  HOME_PAGE,
  TABLET_PORTRAIT,
  SIGN_IN_LINK,
  CTA_TYPO,
  FORMSTACK_REGISTRATION_FORM,
} from "../../../constants/constants"
import phoneIcon from "../../../assets/icons/phone-icon-black.svg"
import hamburgerImg from "../../../assets/icons/hamburger-menu-black.svg"
import DialogContext from "../../../context/DialogContext"

import {
  gtagPluginEventTracker,
  googleAdsEventConversion,
} from "../../../utils/googleAnalyticsHelper"
import {
  ENUM_EVENT_CATEGORIES,
  ENUM_EVENT_ACTIONS,
} from "../../../constants/enumEvents"

const Navbar = () => {
  const [isOpen, setOpen] = React.useState(false)
  const openSignInPageWithEvent = () => {
    gtagPluginEventTracker({
      category: ENUM_EVENT_CATEGORIES.HEADER,
      action: ENUM_EVENT_ACTIONS.CLICK,
      label: `${ENUM_EVENT_CATEGORIES.HEADER}_SignIn`,
    })
    window.open(SIGN_IN_LINK, "_blank")
  }
  const useDialog = useContext(DialogContext) || {}
  const cn = isOpen ? "hamburger-img rotated" : "hamburger-img"

  const openPartnerWithEvent = () => {
    gtagPluginEventTracker({
      category: ENUM_EVENT_CATEGORIES.HEADER,
      action: ENUM_EVENT_ACTIONS.CLICK,
      label: `${ENUM_EVENT_CATEGORIES.HEADER}_GetStarted`,
    })
    if (window.location.pathname === "/") {
      scrollTo("#formstackEmbedSection")
    } else {
      window.location.href = window.location.origin + "#formstackEmbedSection"
    }
    googleAdsEventConversion()
    // window.open(FORMSTACK_REGISTRATION_FORM, "_blank")
  }
  const eventLogoClick = () => {
    gtagPluginEventTracker({
      category: ENUM_EVENT_CATEGORIES.HEADER,
      action: ENUM_EVENT_ACTIONS.CLICK,
      label: `${ENUM_EVENT_CATEGORIES.HEADER}_Logo`,
    })
  }

  if (useMediaQuery({ minWidth: TABLET_PORTRAIT }))
    return (
      <>
        <nav className={`navbar`}>
          <div className="container">
            <div className="logo-phone-wrapper">
              <Link to={HOME_PAGE} onClick={eventLogoClick}>
                <Logo />
              </Link>
            </div>
            <div className="link-wrapper">
              <LinkWithDropdown
                linkTitle="Solutions"
                links={solutionLinks}
                style={{ width: "30.8rem", padding: "2.9rem 2.7rem" }}
              />
              <LinkWithDropdown
                linkTitle="Resources"
                links={resourcesLinks}
                style={{ width: "23rem", padding: "5.3rem 4.8rem" }}
              />
            </div>

            <div className="buttons-wrapper">
              <button
                className="page-navigation-link sign-in-link"
                onClick={openSignInPageWithEvent}
              >
                Sign In
              </button>
              <Button onClick={openPartnerWithEvent} text={CTA_TYPO} />
            </div>
          </div>
        </nav>
      </>
    )
  return (
    <>
      <div className={`hamburger-menu`}>
        <button
          className="hamburger-menu--btn"
          onClick={() => setOpen(!isOpen)}
        >
          <img src={hamburgerImg} className={cn} alt="hamburger-menu" />
        </button>
      </div>
      <nav className={`navbar`}>
        <Drawer open={isOpen} handleClose={() => setOpen(false)} />
        <Link to={HOME_PAGE}>
          <Logo />
        </Link>
        <button id="mobileGetStartedButton" onClick={openPartnerWithEvent}>
          {CTA_TYPO}
        </button>
      </nav>
    </>
  )
}

export default Navbar
