import React from "react"
import { Collapse } from "@material-ui/core"
import { Link } from "gatsby"
import PropTypes from "prop-types"
import Text from "../Text/Text"
import smallArrowDownIconBlue from "../../../assets/icons/small-arrow-down-blue-icon.svg"
import smallArrowDownIconBlack from "../../../assets/icons/small-arrow-down-black-icon.svg"

const DrawerLink = ({ open, handleToggleCollapse, link }) => {
  const { text, subLinks, id } = link
  const textColor = open ? "os-blue-color" : "indigo-color"
  const icon = open ? smallArrowDownIconBlue : smallArrowDownIconBlack
  const iconClassName = open ? "icon-img rotated-arrow" : "icon-img"

  if (!subLinks)
    return (
      <div className="drawer-link-wrapper">
        <div
          className="link-icon 123"
          onClick={link.callback ? link.callback : () => {}}
        >
          <Text color={textColor} variant="body2" className="w-500">
            {text}
          </Text>
        </div>
      </div>
    )

  return (
    <div className="drawer-link-wrapper">
      <button onClick={() => handleToggleCollapse(id)} className="link-icon">
        <Text color={textColor} variant="body2" className="w-500">
          {text}
        </Text>
        <img src={icon} className={iconClassName} alt="arrow-icon" />
      </button>

      <Collapse in={open} timeout="auto" unmountOnExit>
        {subLinks.map(link => (
          <Link key={link.id} to={link.url} className="drawer-link">
            {link.text}
          </Link>
        ))}
      </Collapse>
    </div>
  )
}

DrawerLink.propTypes = {
  open: PropTypes.bool.isRequired,
  handleToggleCollapse: PropTypes.func.isRequired,
  link: PropTypes.object.isRequired,
}

export default DrawerLink
