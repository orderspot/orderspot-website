import React, { useContext } from "react"
import { Link } from "gatsby"
import PropTypes from "prop-types"
import AlertDialogContext from "../../../context/AlertDialogContext"

const LinkWithDropdown = ({ linkTitle, style, links }) => {
  const useDialog = useContext(AlertDialogContext) || {}
  const openDialog = useDialog.actions.openDialog
  const checkLinkExist = (e, link) => {
    if (link.notReady) {
      e.preventDefault()
      openDialog({ dialogText: "We're in progress!" })
    }
  }
  return (
    <div className="link-with-dropdown">
      <span className="link-with-dropdown--title">{linkTitle}</span>
      <div style={style || {}} className="dropdown">
        {links.map((link, index) => (
          <Link
            key={index}
            className="dropdown-navigation-link"
            onClick={event => checkLinkExist(event, link)}
            to={link.url}
          >
            {link.text}
          </Link>
        ))}
      </div>
    </div>
  )
}

LinkWithDropdown.propTypes = {
  linkTitle: PropTypes.string.isRequired,
  style: PropTypes.object,
  links: PropTypes.array,
}

LinkWithDropdown.defaultProps = {
  links: [],
}

export default LinkWithDropdown
