import React, { useContext } from "react"
import Drawer from "@material-ui/core/Drawer"
import Text from "../Text/Text"
import { resourcesLinks, solutionLinks } from "../../../constants/links"
import DrawerLink from "./DrawerLink"
import DialogContext from "../../../context/DialogContext"
import scrollTo from "gatsby-plugin-smoothscroll"
import {
  SIGN_IN_LINK,
  CTA_TYPO,
  FORMSTACK_REGISTRATION_FORM,
} from "../../../constants/constants"
import { googleAdsEventConversion } from "../../../utils/googleAnalyticsHelper"

const SideDrawer = ({ open, handleClose }) => {
  const useDialog = useContext(DialogContext) || {}
  // const openModal = useDialog.actions ? useDialog.actions.openModal : {}
  const openPartnerPage = () => {
    googleAdsEventConversion()
    if (window.location.pathname === "/") {
      scrollTo("#formstackEmbedSection")
    } else {
      window.location.href = window.location.origin + "#formstackEmbedSection"
    }
    // window.open(FORMSTACK_REGISTRATION_FORM, "_blank")
  }
  const [isCollapsed, setCollapsed] = React.useState(null)
  const handleToggleCollapse = id => {
    if (isCollapsed === id) setCollapsed(null)
    else setCollapsed(id)
  }

  const drawerLinks = [
    // { id: "1", text: "Plans" },
    { id: "2", text: "Solutions", subLinks: solutionLinks },
    { id: "3", text: "Resources", subLinks: resourcesLinks },
    {
      id: "4",
      text: "Sign In",
      callback: () => {
        window.open(SIGN_IN_LINK, "_blank")
      },
    },
    { id: "5", text: CTA_TYPO, callback: openPartnerPage },
  ]

  return (
    <Drawer
      className="navbar-drawer"
      anchor="right"
      open={open}
      onClose={handleClose}
    >
      <div className="navbar-drawer--wrapper">
        {drawerLinks.map(link => (
          <DrawerLink
            key={link.id}
            handleToggleCollapse={handleToggleCollapse}
            open={isCollapsed === link.id}
            link={link}
          />
        ))}
      </div>
    </Drawer>
  )
}

Drawer.propTypes = {}

export default SideDrawer
