import React from "react"
import propTypes from "prop-types"
import styled from "styled-components"

const CloseButton = ({ onClick }) => {
  return <Button onClick={onClick} />
}

const Button = styled.button`
  position: absolute;
  background-color: transparent;
  border: 0;

  @media screen and (min-width: 576px) {
    top: 3.8rem;
    right: 5.4rem;
    width: 2.4rem;
    height: 2.4rem;
  }

  @media screen and (max-width: 575px) {
      
  }

  &:focus {
    outline: 0;
  }

  &:before,
  &:after {
    position: absolute;
    top: 0%;
    left: 50%;
    content: " ";
    height: 100%;
    width: 0.2rem;
    background-color: #2f47ff;
  }
  &:before {
    transform: rotate(45deg);
  }
  &:after {
    transform: rotate(-45deg);
  }
`

export default CloseButton
