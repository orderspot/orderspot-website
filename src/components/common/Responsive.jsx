import PropTypes from 'prop-types';
import { useMediaQuery } from 'react-responsive';

//AVAILABLE PROPS
// deviceType:  tablet || desktop || mobile   ==> pass the deviceType as a prop with the following three value.
// Pass name on which we want to render the component.
// NOTE: if we pass tablet as deviceType prop value then the component will also show on the desktop.

const Responsive = ({ deviceType, children }) => {
  const responsiveObject = {
    desktop: useMediaQuery({ minWidth: 1240 }),
    tablet: useMediaQuery({ maxWidth: 1199 }),
    mobile: useMediaQuery({ maxWidth: 950 }),
  };
  
  return responsiveObject[deviceType] && children;
};

Responsive.propTypes = {
  children: PropTypes.node.isRequired,
};

Responsive.defaultProps = {
  deviceType: "mobile"
};

export default Responsive;
