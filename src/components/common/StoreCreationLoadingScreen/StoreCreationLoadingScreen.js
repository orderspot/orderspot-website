import React from "react"
import Text from "../Text/Text"
import tickIcon from "../../../assets/icons/tick-icon-green-round-circle.svg"
// import { storeLoadingLoader } from "../../../constants/images"
import { ProgressBar } from "react-bootstrap"

const StoreCreationLoadingScreen = ({ progress, getClassName }) => {
  const benefits = ["Store name available", "including sample store", "Creating your menu preview"]
  return (
    <section className="store-creation-loading-screen">
      <div className="store-creation-loading-screen--wrapper">
        {/*<img src={storeLoadingLoader} alt="Loading..." className="store-loading-loader" />*/}
        <div className="store-creation--loader">
          <ProgressBar className={getClassName()} striped now={progress} />
        </div>
        
        <div className="store-creation-loading-screen--text-box">
          <Text align="center" variant="title2" color="indigo-color" className="store-creation-loading-screen--title">Creating
            your
            Orderspot Store!</Text>
          <div className="store-creation-loading-screen--benefits">
            {benefits.map(text => (
              <div key={text} className="store-creation-loading-screen--benefit">
                <img src={tickIcon} className="tickIcon" alt={"available"} />
                <Text variant="body4" className="benefit--text" color="indigo-color">{text}</Text>
              </div>
            ))}
          </div>
        </div>
      </div>
    </section>
  )
}

StoreCreationLoadingScreen.propTypes = {}

export default StoreCreationLoadingScreen
