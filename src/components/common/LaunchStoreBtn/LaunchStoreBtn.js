import React, { useContext, useState } from "react"
import { motion, AnimatePresence } from "framer-motion"

import crossIcon from "../../../assets/icons/cross-icon-white.svg"
import DialogContext from "../../../context/DialogContext"

const LaunchStoreBtn = () => {
  const useDialog = useContext(DialogContext) || {}
  const openModal = useDialog.actions ? useDialog.actions.openModal : {}
  const [isShow, setIsShow] = useState(true)
  
  const handleCloseButton = () => {
    setIsShow(false)
  }
  
  return (
    <AnimatePresence>
      {isShow && ( <motion.div
        initial={{ y: "100vh" }}
        animate={{ y: 0 }}
        transition={{ duration: 1 }}
        exit={{ y: "100vh" }}
        className="launch-store-btn--wrapper"
      >
        <motion.div
          whileHover={{ scale: 1.05 }}
          // whileTap={{ scale: 0.95 }}
          className="launch-store-btn"
        >
          <div className="launch-store-btn--btn" onClick={openModal}/>
          <span className="launch-store-btn--text">Launch my store!</span>
          <img onClick={handleCloseButton} className="launch-store-btn--icon" src={crossIcon} alt={"close"} />
        </motion.div>
      </motion.div> )}
    </AnimatePresence>
  )
}

LaunchStoreBtn.propTypes = {}

export default LaunchStoreBtn
