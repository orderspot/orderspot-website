import React from 'react';
import PropTypes from 'prop-types';

const Text = ({ color, align, variant, children, className }) => {
  //CLASSNAMES
  // heading-1 || heading-2 || title-1 || title-2 || title-3|| title-4 || body-1 || body-2 (default) || body-3 || body-4 || body-5 || body-6 || body-7 || body-8 || body-9
  // left || right || center || justify
  // color os-blue-color (default) || indigo-color || indigo-3 || indigo-4 || black-2 || blue-2 || candy-2 || headline_indigo || secondary-brand-merigold || black-text || white-text
  // also supported custom className
  // Available classNames for the TextComponent
  // m[side]-[number]  mb-1, mt-1, mr-1, ml-1 ==== 5
  
  const cn = className ? `${className} ${getClassName(color, align, variant)}` : getClassName(color, align, variant);
  if ( variant === "heading1" || variant === "heading2" )
    return <h2 className={cn}>{children}</h2>;
  return <p className={cn}>{children}</p>;
};

const getClassName = (color, align, variant) => {
  let className;
  
  switch (color) {
    case 'indigo-color':
      className = "indigo-color";
      break;
    case "indigo-3":
      className = "indigo-3";
      break;
    case "indigo-4":
      className = "indigo-4";
      break;
    case 'black-2':
      className = "black-2";
      break;
    case 'blue-2':
      className = 'blue-2';
      break;
    case 'candy-2':
      className = "candy-2";
      break;
    case 'headline_indigo':
      className = "headline_indigo";
      break;
    case "secondary-brand-merigold":
      className = "secondary-brand-merigold";
      break;
    case "black-text":
      className = "black-text";
      break;
    case "white-text":
      className = "white-text";
      break;
    default:
      className = "os-blue-color";
  }
  
  switch (align) {
    case 'center':
      className = `${className} ${"center"}`;
      break;
    case 'right':
      className = `${className} ${"right"}`;
      break;
    default:
      className = `${className} ${"left"}`;
  }
  
  switch (variant) {
    case 'heading1':
      className = `${className} ${"heading-1"}`;
      break;
    case 'heading2':
      className = `${className} ${"heading-2"}`;
      break;
    case 'title1':
      className = `${className} ${"title-1"}`;
      break;
    case 'title2':
      className = `${className} ${"title-2"}`;
      break;
    case 'title3':
      className = `${className} ${"title-3"}`;
      break;
    case 'title4':
      className = `${className} ${"title-4"}`;
      break;
    case 'body2':
      className = `${className} ${"body-2"}`;
      break;
    case 'body3':
      className = `${className} ${"body-3"}`;
      break;
    case 'body4':
      className = `${className} ${"body-4"}`;
      break;
    case 'body5':
      className = `${className} ${"body-5"}`;
      break;
    case 'body6':
      className = `${className} ${"body-6"}`;
      break;
    case 'body7':
      className = `${className} ${"body-7"}`;
      break;
    case 'body8':
      className = `${className} ${"body-8"}`;
      break;
    case 'body9':
      className = `${className} ${"body-9"}`;
      break;
    default:
      className = `${className} ${"body-1"}`;
  }
  
  return className;
};

Text.propTypes = {
  color: PropTypes.string,
  variant: PropTypes.string,
  align: PropTypes.string,
  children: PropTypes.node,
  className: PropTypes.string,
};


export default Text;
/*
* 18px
* 13px
* 20px
* 14px
* 15px
* 22px
*weights
* 500
* 600
* 300
* */
