import React from 'react';
import Text from "../Text/Text";

const SignupFeatures = () => {
  
  return (
    <div className="signup-features">
      <div className="signup-features--text-box">
        <Text className="signup-features--title">Schedule a time to chat!</Text>
        <div className="signup-features--features">
          <div className="signup-features--feature">
            <div className="signup-features--dot"/>
            <Text className="signup-features--text">NO setup costs</Text>
          </div>
          <div className="signup-features--feature">
            <div className="signup-features--dot"/>
            <Text className="signup-features--text">NO credit cards</Text>
          </div>
          <div className="signup-features--feature">
            <div className="signup-features--dot"/>
            <Text className="signup-features--text">NO lock-in contracts</Text>
          </div>
        </div>
      </div>
    </div>
  );
};

SignupFeatures.propTypes = {};

export default SignupFeatures;
