import React from "react"
import Logo from "../Logo/Logo"
import instagramIcon from "../../../assets/icons/instagram-icon.svg"
import facebookIcon from "../../../assets/icons/facebook-icon.svg"
import linkedIn from "../../../assets/icons/linked-in-icon.svg"
import Text from "../Text/Text"
import { PHONE_NUM } from "../../../constants/constants"
import {
  legalLinks,
  resourcesLinks,
  solutionLinks,
  ENUM_EXTERNAL_LINKS,
} from "../../../constants/links"
import { useMediaQuery } from "react-responsive"
import { Collapse } from "@material-ui/core"
import arrowWhite from "../../../assets/icons/arrow-right-white-icon.svg"
import { TABLET_PORTRAIT } from "../../../constants/constants"
import { gtagPluginEventTracker } from "../../../utils/googleAnalyticsHelper"
import {
  ENUM_EVENT_CATEGORIES,
  ENUM_EVENT_ACTIONS,
} from "../../../constants/enumEvents"

import { Link } from "gatsby"

const Footer = () => {
  const [isCollapsed, setCollapsed] = React.useState(null)

  const handleToggleCollapse = id => {
    if (isCollapsed === id) setCollapsed(null)
    else setCollapsed(id)
  }

  const externalLinkEvent = linkName => {
    gtagPluginEventTracker({
      category: ENUM_EVENT_CATEGORIES.FOOTER,
      action: ENUM_EVENT_ACTIONS.CLICK,
      label: `${linkName}_link`,
    })
  }

  const trackPhoneEvent = () => {
    gtagPluginEventTracker({
      category: ENUM_EVENT_CATEGORIES.FOOTER,
      action: ENUM_EVENT_ACTIONS.CLICK,
      label: `click_footer_phone_call`,
    })
  }

  const WebsiteInfo = () => {
    return (
      <div className="privacy-policy-reserved-section">
        <div className="email-phoneNumber">
          <Text color="white-text" variant="body7" className="email-text w-700">
            <a href="mailto:contact@orderspot.co">contact@orderspot.co</a>
          </Text>
          <div className="dot hide-on-mobile" />
          <Text color="white-text" variant="body7" className="phone-text w-700">
            <a href={`tel:+1 ${PHONE_NUM}`} onClick={trackPhoneEvent}>
              +1 {PHONE_NUM}
            </a>
          </Text>
        </div>

        <div className="privacy-policy-legal hide-on-mobile">
          <Link to="/privacy-policy">Privacy Policy</Link>
          <div className="dot hide-on-mobile" />
          <Link to="/terms-of-use">Terms of Use</Link>
        </div>

        <Text
          color="white-text"
          className="copyright-text w-400"
          variant="body9"
        >
          &copy;2020 Orderspot. All rights reserved.
        </Text>
      </div>
    )
  }

  return (
    <section className="footer">
      <div className="container container-wrapper">
        <div className="row">
          <div className="col-xl-3 col-lg-3 col-md-12 social-icon-logo-wrapper">
            <Logo logo="os-horizontal-white" />
            <div className="social-icons">
              <a
                href={ENUM_EXTERNAL_LINKS.INSTAGRAM}
                target="_blank"
                onClick={() => {
                  externalLinkEvent("instagram")
                }}
              >
                <img
                  src={instagramIcon}
                  alt="icon"
                  className="social-icon--img"
                />
              </a>
              <a
                href={ENUM_EXTERNAL_LINKS.FACEBOOK}
                target="_blank"
                onClick={() => {
                  externalLinkEvent("facebook")
                }}
              >
                <img
                  src={facebookIcon}
                  alt="icon"
                  className="social-icon--img"
                />
              </a>
              <a
                href={ENUM_EXTERNAL_LINKS.LINKEDIN}
                target="_blank"
                onClick={() => {
                  externalLinkEvent("linkedIn")
                }}
              >
                <img src={linkedIn} alt="icon" className="social-icon--img" />
              </a>
            </div>
          </div>
          {/* <div className="col-xl-2 col-lg-2 col-md-12">
            <LinksWithTitle
              handleToggleCollapse={handleToggleCollapse}
              id={1}
              open={isCollapsed === 1}
              links={productLinks}
              title="Products"
            />
          </div> */}
          <div className="col-xl-2 col-lg-2 col-md-12">
            <LinksWithTitle
              handleToggleCollapse={handleToggleCollapse}
              id={2}
              open={isCollapsed === 2}
              links={solutionLinks}
              title="Solution"
            />
          </div>
          <div className="col-xl-2 col-lg-2 col-md-12">
            <LinksWithTitle
              handleToggleCollapse={handleToggleCollapse}
              id={3}
              open={isCollapsed === 3}
              links={resourcesLinks}
              title="Resources"
            />
          </div>
          <div className="col-xl-2 col-lg-2 col-md-12">
            <LinksWithTitle
              handleToggleCollapse={handleToggleCollapse}
              id={4}
              open={isCollapsed === 4}
              links={legalLinks}
              title="Legal"
            />
          </div>
          <div className="col-lg-12 col-md-12">
            <WebsiteInfo />
          </div>
        </div>
      </div>
    </section>
  )
}

const LinksWithTitle = ({
  title,
  links = [],
  handleToggleCollapse,
  id,
  open,
}) => {
  const isMobile = useMediaQuery({ maxWidth: TABLET_PORTRAIT })
  const iconClassName = open ? "white-icon-img rotated-arrow" : "white-icon-img"
  if (isMobile)
    return (
      <div className="links-title-wrapper">
        <button
          onClick={() => handleToggleCollapse(id)}
          className="links-title-wrapper--text-icon-wrapper"
        >
          <Text className="w-500 bm-6" color="white-text" variant="body7">
            {title}
          </Text>
          <img src={arrowWhite} alt="white-arrow" className={iconClassName} />
        </button>
        <Collapse in={open} timeout="auto" unmountOnExit>
          {links.map(link => (
            <Link key={link.id} className="links-with-title-link" to={link.url}>
              {link.text}
            </Link>
          ))}
        </Collapse>
      </div>
    )
  return (
    <div className="links-title-wrapper">
      <Text
        className="links-title-wrapper--title w-500"
        color="white-text"
        variant="body7"
      >
        {title}
      </Text>
      {links.map(link => (
        <a key={link.id} className="links-with-title-link" href={link.url}>
          {link.text}
        </a>
      ))}
    </div>
  )
}

Footer.propTypes = {}

export default Footer
