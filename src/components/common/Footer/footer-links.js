export const plansLinks = []

export const solutionsLinks = [
  { id: "7", url: "#footer-links", text: "Orderspot for Small Business " },
  {
    id: "8",
    url: "#footer-links",
    text: "Orderspot for Medium Sized Enterprises",
  },
]

export const resourcesLinks = [
  { id: "13", url: "#footer-links", text: "FAQ" },
  { id: "14", url: "#footer-links", text: "Blog" },
]

export const legalLinks = [
  { id: "21", url: "#footer-links", text: "Terms of Use" },
  { id: "22", url: "privacy-policy", text: "Privacy Policy" },
]
