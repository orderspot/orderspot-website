import React from "react"
import Text from "../Text/Text"
import fbIcon from "../../../assets/icons/facebook-white-icon.svg"
import twitterIcon from "../../../assets/icons/twitter-white-icon.svg"
import instagramIcon from "../../../assets/icons/instagram-white-icon.svg"
import linkedInIcon from "../../../assets/icons/linked-in-white-icon.svg"

import { ENUM_EXTERNAL_LINKS } from "../../../constants/links"

const SocialIcons = () => {
  return (
    <section className="social-icons">
      <Text
        variant="body4"
        className="social-icons--title w-700"
        color="indigo-color"
      >
        Share
      </Text>
      <a href={ENUM_EXTERNAL_LINKS.FACEBOOK} target="_blank">
        <div className="social-icon-wrapper">
          <img
            src={fbIcon}
            alt="facebook"
            className="social-icon-wrapper-img fb-icon"
          />
        </div>
      </a>
      <a href={ENUM_EXTERNAL_LINKS.INSTAGRAM} target="_blank">
        <div className="social-icon-wrapper">
          <img
            src={instagramIcon}
            alt="instagram"
            className="social-icon-wrapper-img insta-icon"
          />
        </div>
      </a>
      <a href={ENUM_EXTERNAL_LINKS.LINKEDIN} target="_blank">
        <div className="social-icon-wrapper">
          <img
            src={linkedInIcon}
            alt="linkedIn"
            className="social-icon-wrapper-img linkedin-icon"
          />
        </div>
      </a>
      <a href={ENUM_EXTERNAL_LINKS.TWITTER} target="_blank">
        <div className="social-icon-wrapper">
          <img
            src={twitterIcon}
            alt="twitter"
            className="social-icon-wrapper-img twitter-icon"
          />
        </div>
      </a>
    </section>
  )
}

SocialIcons.propTypes = {}

export default SocialIcons
