import React from 'react';
import Text from "../Text/Text";
import SingleInputField from "../SingleInputField/SingleInputField";

const WeeklyTips = () => {
  const handleChange = () => {
  };
  return (
    <section className="weekly-tips">
      <Text variant="title3" align="center" color="indigo-color" className="w-700">Weekly Tips from Restaurant
        Owners</Text>
      <Text variant="title3" align="center" color="indigo-color" className="w-700">Straight to your Inbox!</Text>
      
      <SingleInputField
        wrapperClassName={"weekly-tips--form-wrapper"}
        btnText={"Start Free Trial"}
        handleInputChange={handleChange}
        inputId={"weekly-tips"}
        inputName={"weeklyTips"}
        inputPlaceholder={"myemail@example.com"}
        inputValue={""}
        btnClassName="weekly-tips--btn"
      />
    </section>
  );
};

WeeklyTips.propTypes = {};

export default WeeklyTips;
