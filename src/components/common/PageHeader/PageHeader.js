import React from "react"
import PropTypes from "prop-types"
import Text from "../Text/Text"
import BackgroundImage from "gatsby-background-image"

const PageHeader = ({ title, children, headerBgImg, removeBackdrop, backgroundColor }) => {
  return (
    <BackgroundImage
      Tag="section"
      className="page-header"
      fluid={headerBgImg}
      backgroundColor={backgroundColor || "#C0C0C0"}
    >
      {!removeBackdrop && <div className="page-header--backdrop-filter" />}
      {children || (
        <div className="header-main-text--box">
          <h1 className="header-main--title">{title}</h1>
          <Text className="hader-main--sub w-700">Stories that empower owners with the tools to innovate</Text>
        </div>
      )}
    </BackgroundImage>
  )
}

PageHeader.propTypes = {
  title: PropTypes.string,
  children: PropTypes.node,
  headerBgImg: PropTypes.object
}

export default PageHeader
