import React from 'react';
import arrowIcon from '../../../assets/icons/big-arrow-right-blue.svg';
import PropTypes from 'prop-types';

const Button = ({ text, style, className, type, onClick, isIcon }) => {
  const cn = className ? `os-btn ${className}` : "os-btn";
  if ( isIcon )
    return (
      <button onClick={onClick} type={type} style={style || {}} className={`${cn} icon--btn`}>
        <span>{text}</span>
        <img src={arrowIcon} className="btn-icon" alt="icon"/>
      </button>
    );
  return (
    <button onClick={onClick} type={type} style={style || {}} className={cn}>{text}</button>
  );
};

Button.propTypes = {
  text: PropTypes.string.isRequired,
  style: PropTypes.object,
  className: PropTypes.string,
  onclick: PropTypes.func,
};


export default Button;
