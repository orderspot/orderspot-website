import React from "react"
import PropTypes from "prop-types"
import styled from "styled-components"

const Input = ({
  value,
  onChange,
  type,
  placeholder,
  isSmall,
  isHotjarAllow,
  isRequired,
  ...props
}) => {
  return (
    <InputComponent
      isSmall={isSmall}
      value={value}
      onChange={onChange}
      type={type}
      placeholder={placeholder}
      data-hj-allow={isHotjarAllow}
      required={isRequired}
    />
  )
}

const InputComponent = styled.input`
  display: block;
  width: 100%;
  background-color: #fff;

  border-radius: 0.4rem;
  flex-basis: 69%;

  @media screen and (max-width: 767px) {
    font-size: 1.4rem;
    height: 5rem;
    line-height: 5rem;
    padding: 0 1.4rem;
    border: 0.15rem solid #e1e1e5;
  }

  @media screen and (min-width: 768px) and (max-width: 1199px) {
    font-size: 1.5rem;
    line-height: 5.6rem;
    height: 5.6rem;
    padding: 0 2.2rem;
    border: 0.2rem solid #e1e1e5;
  }

  @media screen and (min-width: 1200px) {
    font-size: ${props => (props.isSmall ? "1.5rem" : "1.6rem")};
    line-height: ${props => (props.isSmall ? "5.6rem" : "6.4rem")};
    height: ${props => (props.isSmall ? "5.6rem" : "6.4rem")};
    padding: 0 2.3rem;
    border: 0.3rem solid #e1e1e5;
  }

  &::placeholder {
    color: #e1e1e5;
  }

  &:focus {
    outline: inherit;
  }
`

Input.propTypes = {
  value: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  type: PropTypes.string,
  placeholder: PropTypes.string,
  isSmall: PropTypes.bool,
  isHotjarAllow: PropTypes.bool,
  isRequired: PropTypes.bool,
}

Input.defaultProps = {
  type: "text",
  placeholder: "",
  isSmall: false,
  isRequired: false,
}

export default Input
