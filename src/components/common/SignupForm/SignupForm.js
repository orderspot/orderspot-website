import React from "react"
import Text from "../Text/Text"
import InputField from "../InputField/InputField"
import Button from "../Button/Button"
import Form from "react-bootstrap/Form"
import tickIcon from "../../../assets/icons/tick-icon.svg"
import crossIcon from "../../../assets/icons/cross-icon.svg"

const SignupForm = ({ children }) => {
  const [isRequested, setIsRequested] = React.useState(false)
  const handleChange = () => {
    setIsRequested(true)
  }
  
  const helperText = <Text className="signup-form-input-helper-text" color="indigo-3" variant="body9">
    Choose a name for your online menu. Most people use their restaurant name. <br /> <span
    style={{ color: "#2c2c46" }}>Don’t worry,</span> you
    can change your store
    name later!
  </Text>
  
  const checkboxLabel = (
    <Text color="indigo-color" className="signup-form--text w-400" variant="body9">I agree to Orderspot’s</Text>
  )
  
  const isAvailable = true
  
  return (
    <section className="signup-form">
      {children}
      <Text variant="title4" align="center" className="signup-form--main-text w-700" color="indigo-color">
        Ready to start building seamless mobile experiences with Orderspot?
      </Text>
      <Text variant="body4" align="center" className="signup-form--sub-text w-500" color="indigo-color">
        Our restaurant success manager will reach out via email in <span>1-2 business days</span>
      </Text>
      <form className="signup-form--form">
        
        <div className="signup-form--input-wrapper">
          <InputField
            label="Email"
            id="email"
            value=""
            name="email"
            placeholder="Email"
            handleChange={handleChange}
          />
        </div>
        
        <div className="signup-form--input-wrapper">
          <InputField
            label="Password"
            id="password"
            value=""
            name="password"
            placeholder="Password"
            type="password"
            handleChange={handleChange}
          />
        </div>
        
        <div className={`signup-form--input-wrapper os-domain-input ${!isAvailable ? "error" : ""}`}>
          <InputField
            label="Orderspot Domain"
            id="domain"
            value=""
            name="orderspotDomain"
            placeholder="joespizzeria"
            handleChange={handleChange}
            helperText={helperText}
          />
          
          <div className="base-url--text-box">
            <Text variant="body7" className="w-500 hide-on-mobile"
                  color="headline_indigo">https://partner.orderspot.app/</Text>
            <Text className="base-url--mob-text hide-on-tablet" color="headline_indigo">
              <span>https://partner.</span>
              <span>orderspot.app/</span>
            </Text>
          </div>
          
          {isRequested && <div className="available-unavailable-img-wrapper">
            {isAvailable ?
              <img src={tickIcon} className="tick-icon" alt="available" /> :
              <img src={crossIcon} className="cross-icon" alt="unavailable" />}
          </div>}
        </div>
        
        <div className="signup-form--checkbox">
          <Form.Check inline label={checkboxLabel} type="checkbox"
                      id={`inline-checkbox-1`} />
          <button className="checkbox--links">Terms, Privacy Policy.</button>
        </div>
        
        <div className="form-wrapper--btn">
          <Button
            className="full-width-btn"
            text="Create my store"
            type="submit"
            style={{ width: "58.4rem", height: "7.4rem", fontWeight: 700, fontSize: "2rem" }}
          />
        </div>
      </form>
      <Text variant="body7" className="schedule-demo--text tm-5 w-500" align="center">Schedule a demo</Text>
    </section>
  )
}

SignupForm.propTypes = {}

export default SignupForm
