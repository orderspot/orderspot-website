import React from "react"
import { Link } from "gatsby"
import Layout from "../components/Layout"
import Text from "../components/common/Text/Text"
import { useMediaQuery } from "react-responsive"
import bgImagePc from "../assets/404/404-image-pc@2x.png"
import bgImageM from "../assets/404/404-image-m@3x.png"

const NotFound = () => {
  const isMobile = useMediaQuery({
    maxWidth: 719,
  })
  return (
    <Layout className="not-found-page" isHeaderFixed={true}>
      <section className="not-fount-page-container">
        <img
          src={isMobile ? bgImageM : bgImagePc}
          alt="background image for 404"
        />
        <p className="main-text">
          Oh no!
          <br />
          We couldn’t find that page!
        </p>
        <p className="description-text">
          Select “Go Home” below to be directed to the Home page
        </p>
        <Link to="/" className="go-home">
          Go Home
        </Link>
      </section>
    </Layout>
  )
}

NotFound.propTypes = {}

export default NotFound
