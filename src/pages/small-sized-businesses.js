import React, { useEffect } from "react"
import Layout from "../components/Layout"
import RestaurantsAndBars from "../components/SmallMediumSizedBusinessesPage/RestaurantsAndBars"
import TextAndImagesSection from "../components/common/TextAndImagesSection/TextAndImagesSection"
import Features from "../components/SmallMediumSizedBusinessesPage/Features"
import WhatOurPartnerSaysComponent from "../components/SmallMediumSizedBusinessesPage/WhatOurPartnerSaysComponent"
import LoginFormAndLoginGoogle from "../components/Solutions/LoginFormAndLoginGoogle"
import { gtagPluginEventTracker } from "../utils/googleAnalyticsHelper"
import {
  ENUM_EVENT_CATEGORIES,
  ENUM_EVENT_ACTIONS
} from "../constants/enumEvents"
import { graphql } from "gatsby"

const SmallSizedBusinesses = ({ data }) => {
  
  const featureOptions = [
    {
      title: "Easy to Use",
      text:
        "Built to make orders management painless and efficient for busy restaurant staff",
      img: data.featureImg1File.featureImg1.fluid
    },
    {
      title: "No More 3rd Party Fees",
      text:
        "A zero-commission service that lets you own your profits and customer relationships",
      img: data.featureImg2File.featureImg2.fluid
    },
    {
      title: "Unparalleled Service",
      text:
        "Dedicated restaurant success manager to guide you from activation to launch!",
      img: data.featureImg3File.featureImg3.fluid
    }
  ]
  
  return (
    <Layout className="small-sized-businesses-page" isHeaderFixed>
      <RestaurantsAndBars rightImg={data.barsImgFile.barsImg.fluid} />
      <Features features={featureOptions} />
      <TextAndImagesSection
        title="Drive sales with mobile ordering"
        text="Provide convenient ordering channels for your guests. Empower your staff with an easy to use orders management tool."
        lists={[
          "Brand your ordering App with seamless guest payments",
          "See results with a checkout process designed to convert",
          "Automate payments straight to your bank account"
        ]}
        imageOptions={{
          pc: {
            src: data.driveSalesDFile.driveSalesD.fluid,
            width: "76.3rem"
          },
          t: {
            src: data.driveSalesTFile.driveSalesT.fluid,
            width: "53.6rem"
          },
          m: {
            src: data.driveSalesMFile.driveSalesM.fluid
          }
        }}
        className="drive-sales-section"
      />
      <TextAndImagesSection
        title="Market Smarter with digital menus"
        text="Collect guest information, build marketing channels a and promote your events and promotions with a smart digital menu"
        lists={[
          "Create SMS & Email Lists for restaurant marketing",
          "Automate your menu engineering to maximize sales",
          "Program happy hours, specials and promotions"
        ]}
        imageOptions={{
          pc: {
            src: data.marketSmarterDFile.marketSmarterD.fluid,
            width: "70.5rem"
          },
          t: {
            src: data.marketSmarterTFile.marketSmarterT.fluid,
            width: "52.6rem"
          },
          m: {
            src: data.marketSmarterMFile.marketSmarterM.fluid
          }
        }}
        isImageFirst={true}
        className="market-smarter-section"
      />
      <TextAndImagesSection
        title="Build Customer Loyalty with Data"
        text="Provide a stellar customer experience that drives customer retention and loyalty"
        lists={[
          "Segment customers based on their preferences",
          "Use Item Analytics to optimize operations",
          "Manage guest experience to drive customer visits"
        ]}
        imageOptions={{
          pc: {
            src: data.buildCustomerDFile.buildCustomerD.fluid,
            width: "70.9rem"
          },
          t: {
            src: data.buildCustomerTFile.buildCustomerT.fluid,
            width: "54rem"
          },
          m: {
            src: data.buildCustomerMFile.buildCustomerM.fluid
          }
        }}
      />
      <WhatOurPartnerSaysComponent
        partnerImg={data.partnerImgFile.partnerImg.fluid}
        pageName="small"
      />
      <div className="hide-on-mobile">
        <LoginFormAndLoginGoogle />
      </div>
    </Layout>
  )
}

//graph ql query for fetching images from local file system
export const query = graphql`
  query {
    barsImgFile: file(
      relativePath: {
        eq: "small-medium-sized-businesses/small-medium-sized-businesses-img.png"
      }
    ) {
      barsImg: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    featureImg1File: file(
      relativePath: { eq: "small-medium-sized-businesses/feature-1.png" }
    ) {
      featureImg1: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    featureImg2File: file(
      relativePath: { eq: "small-medium-sized-businesses/feature-2.png" }
    ) {
      featureImg2: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    featureImg3File: file(
      relativePath: { eq: "small-medium-sized-businesses/feature-3.png" }
    ) {
      featureImg3: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    driveSalesDFile: file(
      relativePath: {
        eq: "small-medium-sized-businesses/small-medium-slide-pc-1@2x.png"
      }
    ) {
      driveSalesD: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    driveSalesTFile: file(
      relativePath: {
        eq: "small-medium-sized-businesses/small-medium-slide-t-1@2x.png"
      }
    ) {
      driveSalesT: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    driveSalesMFile: file(
      relativePath: {
        eq: "small-medium-sized-businesses/small-medium-slide-m-1@3x.png"
      }
    ) {
      driveSalesM: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    marketSmarterDFile: file(
      relativePath: {
        eq: "small-medium-sized-businesses/small-medium-slide-pc-2@2x.png"
      }
    ) {
      marketSmarterD: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    marketSmarterTFile: file(
      relativePath: {
        eq: "small-medium-sized-businesses/small-medium-slide-t-2@2x.png"
      }
    ) {
      marketSmarterT: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    marketSmarterMFile: file(
      relativePath: {
        eq: "small-medium-sized-businesses/small-medium-slide-m-2@3x.png"
      }
    ) {
      marketSmarterM: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    buildCustomerDFile: file(
      relativePath: {
        eq: "small-medium-sized-businesses/small-medium-slide-pc-3@2x.png"
      }
    ) {
      buildCustomerD: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    buildCustomerTFile: file(
      relativePath: {
        eq: "small-medium-sized-businesses/small-medium-slide-t-3@2x.png"
      }
    ) {
      buildCustomerT: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    buildCustomerMFile: file(
      relativePath: {
        eq: "small-medium-sized-businesses/small-medium-slide-m-3@3x.png"
      }
    ) {
      buildCustomerM: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    partnerImgFile: file(
      relativePath: { eq: "small-medium-sized-businesses/shrimpshack.png" }
    ) {
      partnerImg: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
  }
`

export default SmallSizedBusinesses
