import React, { useEffect } from "react"
import Layout from "../components/Layout"
import TextAndImagesSection from "../components/common/TextAndImagesSection/TextAndImagesSection"
import Features from "../components/SmallMediumSizedBusinessesPage/Features"
import HospitalityVenuePartnerSays from "../components/Solutions/HospitalityVenuePartnerSays"
import ImageWithSchedule from "../components/Solutions/ImageWithSchedule"
import ReadyToDrive from "../components/Solutions/ReadyToDrive"

import { HOSPITALITY_VENUES_ASSET } from "../constants/images"
import { gtagPluginEventTracker } from "../utils/googleAnalyticsHelper"
import {
  ENUM_EVENT_CATEGORIES,
  ENUM_EVENT_ACTIONS,
} from "../constants/enumEvents"
import { graphql } from "gatsby"

const HospitalityVenues = ({ data }) => {
  const featureOptions = [
    {
      title: "Branded Ordering System",
      text:
        "Your brand matters. Make sure your brand is front and center across all customer touchpoints.",
      img: data.featureImg1File.featureImg1.fluid,
    },
    {
      title: "Made for Hotels",
      text:
        "A system that can work seamlessly with your hotel operations- hotel firewalls, PMS integrations, and guest folios included.",
      img: data.featureImg2File.featureImg2.fluid,
    },
    {
      title: "White Glove\nService",
      text:
        "24/7 service to take you from design consultation all the way to installation and launch",
      img: data.featureImg3File.featureImg3.fluid,
    },
  ]
  return (
    <Layout className="small-sized-businesses-page" isHeaderFixed={true}>
      <ImageWithSchedule
        title="Hospitality Venues"
        textOptions={{
          text: `Your guests expect an elevated experience at your property. Build seamless experiences that let guests order anywhere on your property.`,
          pc: {
            marginTop: "2.2",
          },
          t: {
            marginTop: "3.4",
          },
        }}
        scheduleDemoOptions={{
          pc: {
            marginTop: "5.8",
          },
          t: {
            marginTop: "3.4",
          },
          m: {
            marginTop: "2.4",
          },
        }}
        padding={{
          pc: { top: "23rem", bottom: "17.7rem" },
          t: { top: "11.6rem", bottom: "12.4rem" },
        }}
        margin={{
          pc: { top: "3.6rem", bottom: "26.4rem" },
          t: { top: "3rem", bottom: "12.4rem" },
          m: { top: "5.3rem", bottom: "5.7rem" },
        }}
        imageOptions={{
          pc: {
            width: "65.5",
            height: "66.6",
            marginLeft: "2.4",
            src: data.topImageDFile.topImageD.fluid,
          },
          t: {
            width: "45.1",
            height: "54.1",
            marginLeft: "3.5",
            src: data.topImageTFile.topImageT.fluid,
          },
          m: {
            marginTop: ".8rem",
            marginBottom: "1.4rem",
            src: data.topImageMFile.topImageM.fluid,
          },
        }}
      />
      <Features features={featureOptions} />
      <TextAndImagesSection
        title="Drive Sales at your Venue"
        text="Unleash points of sale in hard to reach parts of your property. See check runners turn into brand ambassadors for your hotel property"
        lists={[
          "Brand your ordering App with seamless guest payments",
          "See results with a checkout process designed to convert ",
          "Automate payments straight to your bank account",
        ]}
        imageOptions={{
          pc: {
            src: data.driveSalesDFile.driveSalesD.fluid,
            width: "71.3rem",
          },
          t: {
            src: data.driveSalesTFile.driveSalesT.fluid,
            width: "52.6rem",
          },
          m: {
            src: data.driveSalesMFile.driveSalesM.fluid,
          },
        }}
      />
      <TextAndImagesSection
        title="Build Customer Loyalty with Data"
        text="Provide a stellar customer experience that drives customer retention and loyalty"
        lists={[
          "Segment customers based on their preferences",
          "Use Item Analytics to optimize operations",
          "Manage guest experience to drive customer visits",
        ]}
        imageOptions={{
          pc: {
            src: data.buildCustomerDFile.buildCustomerD.fluid,
            width: "71.4rem",
          },
          t: {
            src: data.buildCustomerTFile.buildCustomerT.fluid,
            width: "51.3rem",
          },
          m: {
            src: data.buildCustomerMFile.buildCustomerM.fluid,
          },
        }}
        isImageFirst={true}
      />
      <TextAndImagesSection
        title="PMS Integrations"
        text="Made in partnership with hospitality partners, Orderspot is tailored for hotels. Integrations & installations included."
        lists={[
          "Let guests post payments to guest folios",
          "White glove installation for on-premise systems",
          "Integrate with your Property Management System",
        ]}
        imageOptions={{
          pc: {
            src: data.pmsIntegrationsDFile.pmsIntegrationsD.fluid,
            width: "72.1rem",
          },
          t: {
            src: data.pmsIntegrationsTFile.pmsIntegrationsT.fluid,
            width: "55.2rem",
          },
          m: {
            src: data.pmsIntegrationsMFile.pmsIntegrationsM.fluid,
          },
        }}
      />
      <HospitalityVenuePartnerSays
        PartnerImagePc={data.partnerImageDFile.partnerImageD.fluid}
        PartnerImageT={data.partnerImageTFile.partnerImageT.fluid}
        PartnerImageM={data.partnerImageMFile.partnerImageM.fluid}
      />
      <ReadyToDrive />
    </Layout>
  )
}

export const query = graphql`
  query {
    topImageDFile: file(
      relativePath: { eq: "solution/hospitality-venues-pc@2x.png" }
    ) {
      topImageD: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    topImageTFile: file(
      relativePath: { eq: "solution/hospitality-venues-t@2x.png" }
    ) {
      topImageT: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    topImageMFile: file(
      relativePath: { eq: "solution/hospitality-venues-m@3x.png" }
    ) {
      topImageM: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    featureImg1File: file(
      relativePath: { eq: "solution/feature-hospitality-pc-1@2x.png" }
    ) {
      featureImg1: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    featureImg2File: file(
      relativePath: { eq: "solution/feature-hospitality-pc-2@2x.png" }
    ) {
      featureImg2: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    featureImg3File: file(
      relativePath: { eq: "solution/feature-hospitality-pc-3@2x.png" }
    ) {
      featureImg3: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    driveSalesDFile: file(
      relativePath: {
        eq: "solution/textAndImageSection/hospitality-slide-1-pc@2x.png"
      }
    ) {
      driveSalesD: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    driveSalesTFile: file(
      relativePath: {
        eq: "solution/textAndImageSection/hospitality-slide-1-t@2x.png"
      }
    ) {
      driveSalesT: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    driveSalesMFile: file(
      relativePath: {
        eq: "solution/textAndImageSection/hospitality-slide-1-m@3x.png"
      }
    ) {
      driveSalesM: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    buildCustomerDFile: file(
      relativePath: {
        eq: "solution/textAndImageSection/hospitality-slide-2-pc@2x.png"
      }
    ) {
      buildCustomerD: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    buildCustomerTFile: file(
      relativePath: {
        eq: "solution/textAndImageSection/hospitality-slide-2-t@2x.png"
      }
    ) {
      buildCustomerT: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    buildCustomerMFile: file(
      relativePath: {
        eq: "solution/textAndImageSection/hospitality-slide-2-m@3x.png"
      }
    ) {
      buildCustomerM: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    pmsIntegrationsDFile: file(
      relativePath: {
        eq: "solution/textAndImageSection/hospitality-slide-3-pc@2x.png"
      }
    ) {
      pmsIntegrationsD: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    pmsIntegrationsTFile: file(
      relativePath: {
        eq: "solution/textAndImageSection/hospitality-slide-3-t@2x.png"
      }
    ) {
      pmsIntegrationsT: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    pmsIntegrationsMFile: file(
      relativePath: {
        eq: "solution/textAndImageSection/hospitality-slide-3-m@3x.png"
      }
    ) {
      pmsIntegrationsM: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    partnerImageDFile: file(
      relativePath: { eq: "solution/hospitality-venues-partner-pc@2x.png" }
    ) {
      partnerImageD: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    partnerImageTFile: file(
      relativePath: { eq: "solution/hospitality-venues-partner-t@2x.png" }
    ) {
      partnerImageT: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    partnerImageMFile: file(
      relativePath: { eq: "solution/hospitality-img-mobile.png" }
    ) {
      partnerImageM: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
  }
`

export default HospitalityVenues
