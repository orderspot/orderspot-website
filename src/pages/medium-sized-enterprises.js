import React, { useEffect } from "react"
import Layout from "../components/Layout"
import TextAndImagesSection from "../components/common/TextAndImagesSection/TextAndImagesSection"
import Features from "../components/SmallMediumSizedBusinessesPage/Features"
import WhatOurPartnerSaysComponent from "../components/SmallMediumSizedBusinessesPage/WhatOurPartnerSaysComponent"
import ImageWithSchedule from "../components/Solutions/ImageWithSchedule"
import ReadyToDrive from "../components/Solutions/ReadyToDrive"

import { SMALL_MEDIUM_SIZED_ASSET } from "../constants/images"
import { gtagPluginEventTracker } from "../utils/googleAnalyticsHelper"
import {
  ENUM_EVENT_CATEGORIES,
  ENUM_EVENT_ACTIONS,
} from "../constants/enumEvents"
import { graphql } from "gatsby"

const MediumSizedEnterprise = ({ data }) => {

  const featureOptions = [
    {
      title: "Streamlined System",
      text:
        "Built to make orders management painless and efficient for busy restaurant staff",
      img: data.featureImg1File.featureImg1.fluid,
    },
    {
      title: "No More 3rd Party Fees",
      text:
        "A zero-commission service that lets you own your profits and customer relationships",
      img: data.featureImg2File.featureImg2.fluid,
    },
    {
      title: "White Glove Service",
      text:
        "24/7 service to take you from design consultation all the way to installation and launch",
      img: data.featureImg3File.featureImg3.fluid,
    },
  ]
  return (
    <Layout className="small-sized-businesses-page" isHeaderFixed={true}>
      <ImageWithSchedule
        title="Enterprises & Franchises"
        textOptions={{
          text: `Customized online ordering system for each branch location. Get dine-in, takeout, and delivery services for your enterprise, along with customized marketing and sales tools.`,
          pc: {
            marginTop: "2.2",
          },
          t: {
            marginTop: "1.7",
          },
        }}
        scheduleDemoOptions={{
          pc: {
            marginTop: "2.8",
          },
          t: {
            marginTop: "3.1",
            maxWidth: "44.7",
          },
          m: {
            marginTop: "2.4",
          },
        }}
        padding={{
          pc: { top: "17.8rem", bottom: "33.8rem" },
          t: { top: "11rem", bottom: "12.2rem" },
        }}
        margin={{
          pc: { top: "8.6rem", bottom: "8.6rem" },
          t: { top: "6.7rem", bottom: "8.2rem" },
          m: { top: "5.4rem", bottom: "8.6rem" },
        }}
        imageOptions={{
          pc: {
            width: "69.5",
            height: "72.3",
            marginLeft: "-2.4",
            src: data.topImageDFile.topImageD.fluid,
          },
          t: {
            width: "47.3",
            height: "49.3",
            marginLeft: "-4.2",
            src: data.topImageTFile.topImageT.fluid,
          },
          m: {
            marginTop: "2rem",
            marginBottom: "1.8rem",
            src: data.topImageMFile.topImageM.fluid,
          },
        }}
      />
      <Features features={featureOptions} />
      <TextAndImagesSection
        title="Drive sales with mobile ordering"
        text="Manage orders for all your properties on one dashboard
        Empower your staff with an easy to use orders management tool."
        lists={[
          "Brand your ordering App with seamless guest payments",
          "See results with a checkout process designed to convert",
          "Automate payments straight to your bank account",
        ]}
        imageOptions={{
          pc: {
            src: data.driveSalesDFile.driveSalesD.fluid,
            width: "72.73rem",
          },
          t: {
            src: data.driveSalesTFile.driveSalesT.fluid,
            width: "53.6rem",
          },
          m: {
            src: data.driveSalesMFile.driveSalesM.fluid,
          },
        }}
        className="drive-sales-section"
      />
      <TextAndImagesSection
        title="Market Smarter with digital menus"
        text="Manage guest information, build marketing channels a and promote your events and promotions with a smart digital menu for all restaurant locations at once."
        lists={[
          "Create SMS & Email Lists for restaurant marketing",
          "Automate your menu engineering to maximize sales",
          "Program happy hours, specials and promotions",
        ]}
        imageOptions={{
          pc: {
            src: data.marketSmarterDFile.marketSmarterD.fluid,
            width: "68.4rem",
          },
          t: {
            src: data.marketSmarterTFile.marketSmarterT.fluid,
            width: "52.6rem",
          },
          m: {
            src: data.marketSmarterMFile.marketSmarterM.fluid,
          },
        }}
        isImageFirst={true}
        className="market-smarter-section"
      />
      <TextAndImagesSection
        title="Build Customized Dashboards"
        text="Display customer, sales, and transaction data for your team to drive growth"
        lists={[
          "Identify customer VIPs based on their preferences",
          "Find star items using Item Analytics",
          "Optimize menus to drive higher order volume",
        ]}
        imageOptions={{
          pc: {
            src: data.buildCustomerDFile.buildCustomerD.fluid,
            width: "71.35rem",
          },
          t: {
            src: data.buildCustomerTFile.buildCustomerT.fluid,
            width: "54.42rem",
          },
          m: {
            src: data.buildCustomerMFile.buildCustomerM.fluid,
          },
        }}
      />
      <WhatOurPartnerSaysComponent
        partnerImg={data.partnerImgFile.partnerImg.fluid}
        pageName="medium"
      />
      <ReadyToDrive />
    </Layout>
  )
}

export const query = graphql`
  query {
    topImageDFile: file(
      relativePath: { eq: "solution/medium-sized-top-pc@2x.png" }
    ) {
      topImageD: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    topImageTFile: file(
      relativePath: { eq: "solution/medium-sized-top-t@2x.png" }
    ) {
      topImageT: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    topImageMFile: file(
      relativePath: { eq: "solution/medium-sized-top-m@3x.png" }
    ) {
      topImageM: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    featureImg1File: file(
      relativePath: { eq: "small-medium-sized-businesses/feature-1.png" }
    ) {
      featureImg1: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    featureImg2File: file(
      relativePath: { eq: "small-medium-sized-businesses/feature-2.png" }
    ) {
      featureImg2: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    featureImg3File: file(
      relativePath: { eq: "small-medium-sized-businesses/medium-feature3.png" }
    ) {
      featureImg3: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    driveSalesDFile: file(
      relativePath: {
        eq: "small-medium-sized-businesses/small-medium-slide-pc-1@2x.png"
      }
    ) {
      driveSalesD: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    driveSalesTFile: file(
      relativePath: {
        eq: "small-medium-sized-businesses/small-medium-slide-t-1@2x.png"
      }
    ) {
      driveSalesT: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    driveSalesMFile: file(
      relativePath: {
        eq: "small-medium-sized-businesses/small-medium-slide-m-1@3x.png"
      }
    ) {
      driveSalesM: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    marketSmarterDFile: file(
      relativePath: {
        eq: "small-medium-sized-businesses/small-medium-slide-pc-2@2x.png"
      }
    ) {
      marketSmarterD: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    marketSmarterTFile: file(
      relativePath: {
        eq: "small-medium-sized-businesses/small-medium-slide-t-2@2x.png"
      }
    ) {
      marketSmarterT: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    marketSmarterMFile: file(
      relativePath: {
        eq: "small-medium-sized-businesses/small-medium-slide-m-2@3x.png"
      }
    ) {
      marketSmarterM: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    buildCustomerDFile: file(
      relativePath: {
        eq: "small-medium-sized-businesses/small-medium-slide-pc-3@2x.png"
      }
    ) {
      buildCustomerD: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    buildCustomerTFile: file(
      relativePath: {
        eq: "small-medium-sized-businesses/small-medium-slide-t-3@2x.png"
      }
    ) {
      buildCustomerT: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    buildCustomerMFile: file(
      relativePath: {
        eq: "small-medium-sized-businesses/small-medium-slide-m-3@3x.png"
      }
    ) {
      buildCustomerM: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    partnerImgFile: file(
      relativePath: { eq: "small-medium-sized-businesses/fire-wings.png" }
    ) {
      partnerImg: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
  }
`

export default MediumSizedEnterprise
