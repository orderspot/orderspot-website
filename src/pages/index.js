import React, { useEffect } from "react"
import ContactLessDining from "../components/HomePage/ContactLessDining"
import RestaurantsWithLogos from "../components/HomePage/RestaurantsWithLogos"
import FeaturesSection from "../components/HomePage/FeaturesSection"
import CustomerReviewsCarouselSection from "../components/HomePage/CustomerReviewsCarouselSection"
import WeDoEverythingSection from "../components/HomePage/WeDoEverythingSection"
import OrderTypesSection from "../components/HomePage/OrderTypesSection"
import ItemIncludedSection from "../components/HomePage/ItemIncludedSection"
import HowToGetStartedSection from "../components/common/HowToGetStartedSection/HowToGetStartedSection"
import PlanSection from "../components/MainPage/PlanSection"
import ReadyToGetStartedSection from "../components/HomePage/ReadyToGetStarted"
import OrderspotResources from "../components/HomePage/OrderspotResources"
import FormstackEmbedSection from "../components/HomePage/FormstackEmbedSection"
import Layout from "../components/Layout"
import { graphql } from "gatsby"
import { gtagPluginEventTracker } from "../utils/googleAnalyticsHelper"
import {
  ENUM_EVENT_CATEGORIES,
  ENUM_EVENT_ACTIONS,
} from "../constants/enumEvents"
import Modal from "react-modal"

export default function Home(props) {
  return (
    <Layout className="home-page" isHeaderFixed={true}>
      <ContactLessDining
        phoneImg={props.data.file.childImageSharp.fluid}
        dineInBgOneMob={props.data.bgImgOneFile.bgImgOne.fluid}
        dineInBgTwoMob={props.data.bgImgTwoFile.bgImgTwo.fluid}
      />
      <RestaurantsWithLogos />
      <FeaturesSection
        orderImg={props.data.orderImgFile.orderImg.fluid}
        menuEditImg={props.data.menuEditImgFile.menuEditImg.fluid}
        womanImg={props.data.womanImgFile.womanImg.fluid}
      />
      <CustomerReviewsCarouselSection
        customer1Img={props.data.customer1File.customer1.fluid}
        customer2Img={props.data.customer2File.customer2.fluid}
      />
      <WeDoEverythingSection />

      <OrderTypesSection
        contactLessMenuImg={
          props.data.contactlessMenuFile.contactlessMenu.fluid
        }
        itemWithCart={props.data.itemWithCartFile.itemWithCart.fluid}
        seamLessImg={props.data.seamLessDiveFile.seamLessDive.fluid}
        appleWithGooglePayBtn={
          props.data.pickupAndDeliveryFile.pickupAndDelivery.fluid
        }
        nextGenImg={props.data.menuEditImgWomanFile.menuEditImgWoman.fluid}
        orderTypeMenuEditImg={
          props.data.menuEditImgNewFile.menuEditImgNew.fluid
        }
      />
      <ItemIncludedSection
        tableQRImage={props.data.tableQRImageFile.tableQRImage.fluid}
        standQRImage={props.data.standQRImageFile.standQRImage.fluid}
        orderTabletImg={props.data.orderTabletImgFile.orderTabletImg.fluid}
        printerActivationImg={
          props.data.printerActivationImgFile.printerActivationImg.fluid
        }
      />
      <HowToGetStartedSection isHomepage={true} />
      <PlanSection />
      <FormstackEmbedSection />
      <ReadyToGetStartedSection />

      <OrderspotResources isHomepage={true} />
    </Layout>
  )
}

export const query = graphql`
  query {
    file(relativePath: { eq: "home-page/phone-img.png" }) {
      childImageSharp {
        fluid(maxWidth: 659, maxHeight: 596) {
          ...GatsbyImageSharpFluid
        }
      }
    }
    bgImgOneFile: file(relativePath: { eq: "home-page/dine-in-bg-skin.png" }) {
      bgImgOne: childImageSharp {
        fluid(maxWidth: 659, maxHeight: 596) {
          ...GatsbyImageSharpFluid
        }
      }
    }
    bgImgTwoFile: file(relativePath: { eq: "home-page/dine-in-bg-2.png" }) {
      bgImgTwo: childImageSharp {
        fluid(maxWidth: 659, maxHeight: 596) {
          ...GatsbyImageSharpFluid
        }
      }
    }
    orderImgFile: file(relativePath: { eq: "order-img.png" }) {
      orderImg: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    menuEditImgFile: file(relativePath: { eq: "menu-edit-img.png" }) {
      menuEditImg: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    womanImgFile: file(relativePath: { eq: "woman-img.png" }) {
      womanImg: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    customer1File: file(relativePath: { eq: "home-page/shrimpshack-pc.png" }) {
      customer1: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    customer2File: file(relativePath: { eq: "home-page/firewings-pc.png" }) {
      customer2: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    contactlessMenuFile: file(
      relativePath: { eq: "home-page/contact-less-menu-img.png" }
    ) {
      contactlessMenu: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    seamLessDiveFile: file(
      relativePath: { eq: "home-page/seam-less-dive-in-img.png" }
    ) {
      seamLessDive: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    menuEditImgWomanFile: file(
      relativePath: { eq: "home-page/menu-edit-img.png" }
    ) {
      menuEditImgWoman: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    menuEditImgNewFile: file(
      relativePath: { eq: "home-page/menu-edit-img-new.png" }
    ) {
      menuEditImgNew: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    pickupAndDeliveryFile: file(
      relativePath: { eq: "home-page/pickup-and-delivery-item1@2x.png" }
    ) {
      pickupAndDelivery: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    itemWithCartFile: file(
      relativePath: { eq: "home-page/item-with-cart-img.png" }
    ) {
      itemWithCart: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    tableQRImageFile: file(
      relativePath: { eq: "home-page/tabletop-qr-signage-complimentary-1.png" }
    ) {
      tableQRImage: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    standQRImageFile: file(
      relativePath: { eq: "home-page/tabletop-qr-signage-complimentary-2.png" }
    ) {
      standQRImage: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    orderTabletImgFile: file(
      relativePath: { eq: "home-page/order-management-tablet-img.png" }
    ) {
      orderTabletImg: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    printerActivationImgFile: file(
      relativePath: { eq: "home-page/kitchen-printer-for-activation-img.png" }
    ) {
      printerActivationImg: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    signupFormPcImageFile: file(
      relativePath: { eq: "home-page/bg-for-main-page@2x.png" }
    ) {
      signupFormPcImage: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
  }
`
