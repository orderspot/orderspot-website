import React, { useEffect } from "react"
import Layout from "../components/Layout"
import AllBlogs from "../components/BlogsPage/AllBlogs"
import { graphql } from "gatsby"
// import WeeklyTips from "../components/common/WeeklyTips/WeeklyTips";
import PageHeader from "../components/common/PageHeader/PageHeader"

import { gtagPluginEventTracker } from "../utils/googleAnalyticsHelper"
import {
  ENUM_EVENT_CATEGORIES,
  ENUM_EVENT_ACTIONS,
} from "../constants/enumEvents"

const Blog = ({ data }) => {
  const allBlogPosts = data.allContentfulBlogpost.edges
  const allCategories = data.allContentfulAllCategories.edges
  const blogBg = data.blogBgFile.blogBg.fluid
  return (
    <Layout className="blogs" isHeaderFixed={true}>
      <PageHeader headerBgImg={blogBg} title={"BLOG"} />
      <AllBlogs allCategories={allCategories} allBlogPosts={allBlogPosts} />
      {/*<WeeklyTips/>*/}
    </Layout>
  )
}

Blog.propTypes = {}

export const query = graphql`
  query {
    allContentfulBlogpost {
      edges {
        node {
          category {
            slug
          }
          title
          excerpt
          slug
          readTime
          publishDate
          subTitleBlack
          featuredImage {
            fluid {
              base64
              sizes
              aspectRatio
              srcSet
              src
            }
          }
        }
      }
    }
    allContentfulAllCategories {
      edges {
        node {
          slug
          categoryText
          categoryTitle
        }
      }
    }
    blogBgFile: file(relativePath: { eq: "blog-header-img.png" }) {
      blogBg: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
  }
`

export default Blog
