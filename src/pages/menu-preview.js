import React, { useEffect } from "react"
import MenuPreviewPage from "../components/MenuPreviewPage/MenuPreviewPage"
import { gtagPluginEventTracker } from "../utils/googleAnalyticsHelper"
import {
  ENUM_EVENT_CATEGORIES,
  ENUM_EVENT_ACTIONS,
} from "../constants/enumEvents"
import { graphql } from "gatsby"

const MenuPreview = ({ data }) => {
  return (
    <>
      <MenuPreviewPage
        menuPreviewPageHeaderBgImg={data.qrBgImageFile.qrBgImage.fluid}
      />
    </>
  )
}

export const query = graphql`
  query {
    qrBgImageFile: file(relativePath: { eq: "temp/bg.jpg" }) {
      qrBgImage: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
  }
`

export default MenuPreview
