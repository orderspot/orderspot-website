import React, { useEffect } from "react"
import Layout from "../components/Layout"
import { gtagPluginEventTracker } from "../utils/googleAnalyticsHelper"
import {
  ENUM_EVENT_CATEGORIES,
  ENUM_EVENT_ACTIONS,
} from "../constants/enumEvents"

const TermsOfUse = () => {
  return (
    <Layout className="terms-of-use-page" isHeaderFixed>
      <div className="container content-wrapper">
        <h1 className="heading">Orderspot TERMS AND CONDITIONS</h1>
        <div className="w-richtext">
          <ol role="list" className="main-ol">
            <li>
              <strong className="title">SAAS SERVICES AND SUPPORT</strong>
              <ol>
                <li>
                  Subject to the terms of this Agreement, Company will use
                  commercially reasonable efforts to provide Customer the
                  Services in accordance with the Service Level Terms attached
                  hereto as Exhibit B. As part of the registration process,
                  Customer will identify an administrative user name and
                  password for Customer’s Company account (“
                  <strong>User Credentials</strong>”).&nbsp; Company reserves
                  the right to refuse registration of, or cancel passwords it
                  deems inappropriate. User Credentials are the property of
                  Company; Customer will maintain the User Credentials as
                  confidential and not transfer or share the User Credentials
                  with any third party.&nbsp; Customer shall immediately notify
                  Company of any unauthorized use or disclosure of the User
                  Credentials.&nbsp; Customer is responsible for all activity
                  under Customer’s account whether or not
                  authorized.&nbsp;&nbsp;
                </li>
                <li>
                  Subject to the terms hereof and payment of all applicable
                  Fees, Company will provide Customer with reasonable technical
                  support services in accordance with Company’s standard
                  practice.
                </li>
              </ol>
            </li>

            <li>
              <strong className="title">
                RESTRICTIONS AND RESPONSIBILITIES
              </strong>
              <ol>
                <li>
                  <strong>
                    <em>
                      Customer will not, directly or indirectly: reverse
                      engineer, decompile, disassemble or otherwise attempt to
                      discover the source code, object code or underlying
                      structure, ideas, know-how or algorithms relevant to the
                      Services or any software, documentation or data related to
                      the Services (“Software”); modify, translate, or create
                      derivative works based on the Services or any Software
                      (except to the extent expressly permitted by Company or
                      authorized within the Services); use the Services or any
                      Software for timesharing or service bureau purposes or
                      otherwise for the benefit of a third; or remove any
                      proprietary notices or labels.&nbsp; With respect to any
                      Software that is distributed or provided to Customer for
                      use on Customer premises or devices, Company hereby grants
                      Customer a non-exclusive, non-transferable,
                      non-sublicensable license to use such Software during the
                      Term only in connection with the Services.&nbsp;
                    </em>
                  </strong>
                </li>
                <li>
                  Further, Customer may not remove or export from the United
                  States or allow the export or re-export of the Services,
                  Software or anything related thereto, or any direct product
                  thereof in violation of any restrictions, laws or regulations
                  of the United States Department of Commerce, the United States
                  Department of Treasury Office of Foreign Assets Control, or
                  any other United States or foreign agency or authority.&nbsp;
                  As defined in FAR section 2.101, the Software and
                  documentation are “commercial items” and according to DFAR
                  section 252.227-7014(a)(1) and (5) are deemed to be
                  “commercial computer software” and “commercial computer
                  software documentation.”&nbsp; Consistent with DFAR section
                  227.7202 and FAR section 12.212, any use modification,
                  reproduction, release, performance, display, or disclosure of
                  such commercial software or commercial software documentation
                  by the U.S. Government will be governed solely by the terms of
                  this Agreement and will be prohibited except to the extent
                  expressly permitted by the terms of this Agreement.
                </li>
                <li>
                  Customer represents, covenants, and warrants that Customer
                  will use the Services only in compliance with Company’s
                  standard published policies then in effect (the “
                  <strong>Policy</strong>
                  ”) and all applicable laws and regulations.&nbsp; Customer
                  hereby agrees to indemnify and hold harmless Company against
                  any damages, losses, liabilities, settlements and expenses
                  (including without limitation costs and attorneys’ fees) in
                  connection with any claim or action that arises from an
                  alleged violation of the foregoing, from the Customer Data (as
                  defined in Section 3.1 below), from Customer’s breach of its
                  representations and warranties hereunder, or otherwise from
                  Customer’s use of Services. Although Company has no obligation
                  to monitor Customer’s use of the Services, Company may do so
                  and may prohibit any use of the Services it believes may be
                  (or alleged to be) in violation of the foregoing.
                </li>
                <li>
                  Customer shall be responsible for obtaining and maintaining
                  any equipment and ancillary services needed to connect to,
                  access or otherwise use the Services, including, without
                  limitation, modems, hardware, servers, software, operating
                  systems, networking, web servers and the like (collectively, “
                  <strong>Equipment</strong>
                  ”).&nbsp; Customer shall also be responsible for maintaining
                  the security of the Equipment, Customer account, passwords
                  (including but not limited to administrative and user
                  passwords) and files, and for all uses of Customer account or
                  the Equipment with or without Customer’s knowledge or consent.
                </li>
              </ol>
            </li>

            <li>
              <strong className="title">
                <em>CONFIDENTIALITY; PROPRIETARY RIGHTS</em>
              </strong>
              <ol>
                <li>
                  <strong>
                    <em>
                      Each party (the “Receiving Party”) understands that the
                      other party (the “Disclosing Party”) has disclosed or may
                      disclose business, technical or financial information
                      relating to the Disclosing Party’s business (hereinafter
                      referred to as “Proprietary Information” of the Disclosing
                      Party).&nbsp; Proprietary Information of Company includes
                      non-public information regarding features, functionality
                      and performance of the Service.&nbsp; Proprietary
                      Information of Customer includes non-public data provided
                      by Customer to Company to enable the provision of the
                      Services (“Customer Data”). The Receiving Party agrees:
                      (i) to take reasonable precautions to protect such
                      Proprietary Information, and (ii) not to use (except in
                      performance of the Services or as otherwise permitted
                      herein) or divulge to any third person any such
                      Proprietary Information.&nbsp; The Disclosing Party agrees
                      that the foregoing shall not apply with respect to any
                      information after five (5) years following the disclosure
                      thereof or any information that the Receiving Party can
                      document (a) is or becomes generally available to the
                      public, or (b) was in its possession or known by it prior
                      to receipt from the Disclosing Party, or (c) was
                      rightfully disclosed to it without restriction by a third
                      party, or (d) was independently developed without use of
                      any Proprietary Information of the Disclosing Party or (e)
                      is required to be disclosed by law.&nbsp;&nbsp;
                    </em>
                  </strong>
                </li>
                <li>
                  <strong>
                    <em>
                      Customer grants Company the royalty-free, non-exclusive
                      worldwide license to store, process, reproduce, transmit,
                      and otherwise use any Customer Data for purposes of
                      providing the Services, developing, maintaining,
                      supporting or improving the Services, and performing its
                      obligations and exercising its rights under this
                      Agreement. Company represents and warrants that the
                      Customer Data does not infringe or violate any
                      intellectual property, privacy or other rights or any
                      person or third party, does not violate any applicable
                      law, rule, regulation or order, does not constitute
                      obscene, defamatory, or illegal matter, does not contain
                      and does not contain any viruses, Trojan Horses,
                      ransomware, malware or any other malicious or destructive
                      software, and does not contain any “personal health
                      information,” as defined under the Health Insurance
                      Portability and Accountability Act of the United States of
                      America.
                    </em>
                  </strong>
                </li>
                <li>
                  <strong>
                    <em>
                      Company shall own and retain all right, title and interest
                      in and to (a) the Services and Software, all improvements,
                      enhancements or modifications thereto, (b) any software,
                      applications, inventions or other technology developed in
                      connection with Implementation Services or support, (c)
                      all intellectual property rights related to any of the
                      foregoing in subsections (a) and (b), and (d) any data
                      that is based on or derived from the Customer Data and
                      provided to Customer as part of the
                      Services.&nbsp;&nbsp;&nbsp;&nbsp;
                    </em>
                  </strong>
                </li>
                <li>
                  <strong>
                    <em>
                      Notwithstanding anything to the contrary herein, Company
                      shall have the right to collect and analyze data and other
                      information relating to the provision, use and performance
                      of various aspects of the Services and related systems and
                      technologies (including, without limitation, information
                      concerning Customer Data and data derived
                      therefrom)(“Resultant Data”), and&nbsp; Company will be
                      free (during and after the term hereof) to (i) use such
                      Resultant Data to improve and enhance the Services and for
                      other development, diagnostic and corrective purposes in
                      connection with the Services and other Company offerings,
                      and (ii) disclose such Resultant Data solely in aggregate
                      or other de-identified form in connection with its
                      business. No rights or licenses are granted except as
                      expressly set forth herein.
                    </em>
                  </strong>
                </li>
                <li>
                  Customer may have the opportunity to present to Company its
                  recommendations or feedback for new features, functionality,
                  or other improvements to the Services (“
                  <strong>Feedback</strong>
                  ”).&nbsp; The parties agree that all Feedback is and shall be
                  given voluntarily.&nbsp; Feedback, even if designated as
                  confidential by Customer, shall not, absent a separate written
                  agreement, create any confidentiality obligation for
                  Company.&nbsp; Customer will not provide Company with any
                  Feedback that Customer is not authorized or permitted to
                  provide to Company.&nbsp; Customer shall be free to use,
                  disclose, reproduce, license or otherwise distribute, and
                  exploit the Feedback provided to it as it sees fit, entirely
                  without obligation or payment or restriction of any kind on
                  account of intellectual property rights or otherwise.
                </li>
                <li>
                  <strong>
                    <em>
                      In the course of providing the Services to Customer and
                      performing its obligations under this Agreement, Company
                      may process information that identifies, relates to,
                      describes, is capable of being associated with, or could
                      reasonably be linked, directly or indirectly, with a
                      particular consumer or household, or any other information
                      that is defined as personal information under applicable
                      law (“Personal Information”). All Personal Information
                      will be processed, accessed, used, maintained, collected,
                      shared or disclosed by Company only as is necessary and
                      for the sole purpose for Company to perform the Services
                      to Customer under the Agreement.&nbsp; Company will not
                      sell, rent, or otherwise disclose Personal Information to
                      any third party in exchange for monetary or other valuable
                      consideration. For the avoidance of doubt Company
                      acknowledges and agrees that it is a service provider
                      under the California Consumer Privacy Act of 2018, as
                      amended from time to time (the “CCPA”).&nbsp; Customer
                      represents and warrants that it has complied with all
                      applicable laws and provided all required notices and
                      obtained all necessary consents for Company to process
                      Personal Information under this Agreement.
                    </em>
                  </strong>
                </li>
              </ol>
            </li>

            <li>
              <strong className="title">PAYMENT OF FEES</strong>
              <ol>
                <li>
                  Customer will pay Company the then applicable fees described
                  in the Order Form for the Services and Implementation Services
                  in accordance with the terms therein and any Statement of Work
                  (the “<strong>Fees</strong>”).&nbsp; If Customer’s use of the
                  Services exceeds the Service Capacity set forth on the Order
                  Form or otherwise requires the payment of additional fees (per
                  the terms of this Agreement), Customer shall be billed for
                  such usage and Customer agrees to pay the additional fees in
                  the manner provided herein{" "}
                  <strong>
                    <em>
                      [S. Pink: I’m not sure I see how such additional fees will
                      be paid?&nbsp; Is there a formula?]
                    </em>
                  </strong>
                  .&nbsp; Company reserves the right to change the Fees or
                  applicable charges and to institute new charges and Fees at
                  the end of the Initial Service Term or then-current renewal
                  term, upon thirty (30) days prior notice to Customer (which
                  may be sent by email). If Customer believes that Company has
                  billed Customer incorrectly, Customer must contact Company no
                  later than 60 days after the closing date on the first billing
                  statement in which the error or problem appeared, in order to
                  receive an adjustment or credit.&nbsp; Failure to notify
                  Company of any errors shall foreclose Customer from contesting
                  the billing statement. Inquiries should be directed to
                  Company’s customer support department.
                </li>
                <li>
                  Company may choose to bill through an invoice, in which case,
                  full payment for invoices issued in any given month must be
                  received by Company thirty (30) days after the mailing date of
                  the invoice.&nbsp; Unpaid amounts are subject to a finance
                  charge of 1.5% per month on any outstanding balance, or the
                  maximum permitted by law, whichever is lower, plus all
                  expenses of collection and may result in immediate termination
                  of Service. Customer shall be responsible for all taxes
                  associated with Services other than U.S. taxes based on
                  Company’s net income.&nbsp;&nbsp;
                </li>
              </ol>
            </li>

            <li>
              <strong className="title">TERM AND TERMINATION</strong>
              <ol role="list">
                <li>
                  Subject to earlier termination as provided below, this
                  Agreement is for the Initial Service Term as specified in the
                  Order Form, and shall be automatically renewed for additional
                  periods of the same duration as the Initial Service Term
                  (collectively, the “<strong>Term</strong>”), unless either
                  party requests termination at least thirty (30) days prior to
                  the end of the then-current term.
                </li>
                <li>
                  In addition to any other remedies it may have, either party
                  may also terminate this Agreement upon thirty (30) days’
                  notice (or without notice in the case of nonpayment), if the
                  other party materially breaches any of the terms or conditions
                  of this Agreement.&nbsp; Customer will pay in full for the
                  Services up to and including the last day on which the
                  Services are provided. Upon any termination, Company will make
                  all Customer Data available to Customer for electronic
                  retrieval for a period of thirty (30) days, but thereafter
                  Company may, but is not obligated to, delete stored Customer
                  Data. All sections of this Agreement which by their nature
                  should survive termination will survive termination,
                  including, without limitation, accrued rights to payment,
                  confidentiality obligations, warranty disclaimers, and
                  limitations of liability.&nbsp;
                </li>
              </ol>
            </li>

            <li>
              <strong className="title">WARRANTY AND DISCLAIMER</strong>
              <p>
                Company shall use reasonable efforts consistent with prevailing
                industry standards to maintain the Services in a manner which
                minimizes errors and interruptions in the Services and shall
                perform the Implementation Services in a professional and
                workmanlike manner.&nbsp; Services may be temporarily
                unavailable for scheduled maintenance or for unscheduled
                emergency maintenance, either by Company or by third-party
                providers, or because of other causes beyond Company’s
                reasonable control, but Company shall use reasonable efforts to
                provide advance notice in writing or by e-mail of any scheduled
                service disruption.&nbsp; HOWEVER, COMPANY DOES NOT WARRANT THAT
                THE SERVICES WILL BE UNINTERRUPTED OR ERROR FREE; NOR DOES IT
                MAKE ANY WARRANTY AS TO THE RESULTS THAT MAY BE OBTAINED FROM
                USE OF THE SERVICES.
                <strong>
                  <em>&nbsp; </em>
                </strong>
                EXCEPT AS EXPRESSLY SET FORTH IN THIS SECTION, THE SERVICES AND
                IMPLEMENTATION SERVICES ARE PROVIDED “AS IS” AND COMPANY
                DISCLAIMS ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING, BUT NOT
                LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
                FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT.&nbsp;&nbsp;
              </p>
            </li>

            <li>
              <strong className="title">LIMITATION OF LIABILITY</strong>
              <p>
                NOTWITHSTANDING ANYTHING TO THE CONTRARY, EXCEPT FOR BODILY
                INJURY OF A PERSON, COMPANY AND ITS SUPPLIERS (INCLUDING BUT NOT
                LIMITED TO ALL EQUIPMENT AND TECHNOLOGY SUPPLIERS), OFFICERS,
                AFFILIATES, REPRESENTATIVES, CONTRACTORS AND EMPLOYEES SHALL NOT
                BE RESPONSIBLE OR LIABLE WITH RESPECT TO ANY SUBJECT MATTER OF
                THIS AGREEMENT OR TERMS AND CONDITIONS RELATED THERETO UNDER ANY
                CONTRACT, NEGLIGENCE, STRICT LIABILITY OR OTHER THEORY: (A) FOR
                ERROR OR INTERRUPTION OF USE OR FOR LOSS OR INACCURACY OR
                CORRUPTION OF DATA OR COST OF PROCUREMENT OF SUBSTITUTE GOODS,
                SERVICES OR TECHNOLOGY OR LOSS OF BUSINESS; (B) FOR ANY
                INDIRECT, EXEMPLARY, INCIDENTAL, SPECIAL OR CONSEQUENTIAL
                DAMAGES; (C) FOR ANY MATTER BEYOND COMPANY’S REASONABLE CONTROL;
                OR (D) FOR ANY AMOUNTS THAT, TOGETHER WITH AMOUNTS ASSOCIATED
                WITH ALL OTHER CLAIMS, EXCEED THE FEES PAID BY CUSTOMER TO
                COMPANY FOR THE SERVICES UNDER THIS AGREEMENT IN THE 12 MONTHS
                PRIOR TO THE ACT THAT GAVE RISE TO THE LIABILITY, IN EACH CASE,
                WHETHER OR NOT COMPANY HAS BEEN ADVISED OF THE POSSIBILITY OF
                SUCH DAMAGES.&nbsp;
              </p>
            </li>

            <li>
              <strong className="title">MISCELLANEOUS</strong>
              <p>
                If any provision of this Agreement is found to be unenforceable
                or invalid, that provision will be limited or eliminated to the
                minimum extent necessary so that this Agreement will otherwise
                remain in full force and effect and enforceable.&nbsp; This
                Agreement is not assignable, transferable or sublicensable by
                Customer except with Company’s prior written consent.&nbsp;
                Company may transfer and assign any of its rights and
                obligations under this Agreement without consent.&nbsp; This
                Agreement is the complete and exclusive statement of the mutual
                understanding of the parties and supersedes and cancels all
                previous written and oral agreements, communications and other
                understandings relating to the subject matter of this Agreement,
                and that all waivers and modifications must be in a writing
                signed by both parties, except as otherwise provided
                herein.&nbsp; No agency, partnership, joint venture, or
                employment is created as a result of this Agreement and Customer
                does not have any authority of any kind to bind Company in any
                respect whatsoever.&nbsp; In any action or proceeding to enforce
                rights under this Agreement, the prevailing party will be
                entitled to recover costs and attorneys’ fees.&nbsp; All notices
                under this Agreement will be in writing and will be deemed to
                have been duly given when received, if personally delivered;
                when receipt is electronically confirmed, if transmitted by
                facsimile or e-mail; the day after it is sent, if sent for next
                day delivery by recognized overnight delivery service; and upon
                receipt, if sent by certified or registered mail, return receipt
                requested.&nbsp; This Agreement shall be governed by the laws of
                the State of California without regard to its conflict of laws
                provisions. The exclusive venue for any action arising under or
                relating to this Agreement shall be a court of competent
                jurisdiction in the County of Los Angeles, California; the
                parties consent to personal jurisdiction of such courts. The
                parties shall work together in good faith to issue at least one
                mutually agreed upon press release within 90 days of the
                Effective Date, and Customer otherwise agrees to reasonably
                cooperate with Company to serve as a reference account upon
                request.
              </p>
            </li>
          </ol>
        </div>
      </div>
    </Layout>
  )
}

export default TermsOfUse
