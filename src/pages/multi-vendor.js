import React, { useEffect } from "react"
import Layout from "../components/Layout"
import TextAndImagesSection from "../components/common/TextAndImagesSection/TextAndImagesSection"
import Features from "../components/SmallMediumSizedBusinessesPage/Features"
import MultiVendorPartnerSays from "../components/Solutions/MultiVendorPartnerSays"
import ImageWithSchedule from "../components/Solutions/ImageWithSchedule"
import ReadyToDrive from "../components/Solutions/ReadyToDrive"
import { gtagPluginEventTracker } from "../utils/googleAnalyticsHelper"
import {
  ENUM_EVENT_CATEGORIES,
  ENUM_EVENT_ACTIONS,
} from "../constants/enumEvents"
import { graphql } from "gatsby"

const MultiVendor = ({ data }) => {
  const featureOptions = [
    {
      title: "Branded Ordering System",
      text:
        "Your brand matters. Make sure your brand is front and center across all customer touchpoints.",
      img: data.featureImg1File.featureImg1.fluid,
    },
    {
      title: "Made for Multi-Vendor",
      text:
        "Built to let guests orders from multiple vendors to boost sales at food halls, stadiums, events and markets.",
      img: data.featureImg2File.featureImg2.fluid,
    },
    {
      title: "White Glove Service",
      text:
        "24/7 service to take you from design consultation all the way to installation and launch",
      img: data.featureImg3File.featureImg3.fluid,
    },
  ]
  return (
    <Layout className="multi-vendor-page" isHeaderFixed={true}>
      <ImageWithSchedule
        title={`Food Halls &\nMulti-Vendor`}
        textOptions={{
          text:
            "Provide a memorable, branded online ordering experience that drives customer revisits to your property. Built in marketing and insights.  All in one place.",
          pc: {
            marginTop: "3.6",
          },
          t: {
            marginTop: "3.4",
          },
        }}
        scheduleDemoOptions={{
          pc: {
            marginTop: "5.9",
          },
          t: {
            marginTop: "3.4",
          },
          m: {
            marginTop: "2.5",
          },
        }}
        padding={{
          pc: { top: "14.9rem", bottom: "19.9rem" },
          t: { top: "4.3rem", bottom: "16rem" },
        }}
        margin={{
          pc: { top: "6.6rem", bottom: "12.2rem" },
          t: { top: "7rem", bottom: "6.28rem" },
          m: { top: "3.3rem", bottom: "4.8rem" },
        }}
        imageOptions={{
          pc: {
            width: "63.05",
            height: "67.8",
            marginLeft: "4.8",
            src: data.topImageDFile.topImageD.fluid,
          },
          t: {
            width: "47.74",
            height: "55.62",
            marginLeft: ".5",
            src: data.topImageTFile.topImageT.fluid,
          },
          m: {
            marginTop: "2rem",
            marginBottom: "1.8rem",
            src: data.topImageMFile.topImageM.fluid,
          },
        }}
      />
      <Features features={featureOptions} />
      <TextAndImagesSection
        title="Drive sales with mobile ordering"
        text="Shorten diner wait times and table turnover with an easy to use orders management system"
        lists={[
          "Branded Ordering App with seamless guest payments",
          "Customizable Orders Management System optimized for busy venues with long wait times",
          "Automated Payments routed straight to each vendor’s account",
        ]}
        imageOptions={{
          pc: {
            src: data.driveSalesDFile.driveSalesD.fluid,
            width: "72.4rem",
          },
          t: {
            src: data.driveSalesTFile.driveSalesT.fluid,
            width: "54rem",
          },
          m: {
            src: data.driveSalesMFile.driveSalesM.fluid,
          },
        }}
      />
      <TextAndImagesSection
        title="Market Smart with digital menus"
        text="Collect guest information, build marketing channels a and promote your venue’s events and promotions with a smart digital menu"
        lists={[
          "Create SMS & Email Lists for restaurant marketing",
          "Automate your menu engineering to maximize sales",
          "Program happy hours, specials and promotions",
        ]}
        imageOptions={{
          pc: {
            src: data.marketSmarterDFile.marketSmarterD.fluid,
            width: "73.9rem",
          },
          t: {
            src: data.marketSmarterTFile.marketSmarterT.fluid,
            width: "55rem",
          },
          m: {
            src: data.marketSmarterMFile.marketSmarterM.fluid,
          },
        }}
        isImageFirst={true}
      />
      <TextAndImagesSection
        title="Build Customer Loyalty with Data"
        text="Provide a stellar customer experience that drives customer retention and loyalty"
        lists={[
          "Segment customers based on their preferences",
          "Use Item Analytics to optimize operations",
          "Manage guest experience to drive customer visits",
        ]}
        imageOptions={{
          pc: {
            src: data.buildCustomerDFile.buildCustomerD.fluid,
            width: "74.1rem",
          },
          t: {
            src: data.buildCustomerTFile.buildCustomerT.fluid,
            width: "54rem",
          },
          m: {
            src: data.buildCustomerMFile.buildCustomerM.fluid,
          },
        }}
      />
      <MultiVendorPartnerSays />
      <ReadyToDrive />
    </Layout>
  )
}

export const query = graphql`
  query {
    topImageDFile: file(
      relativePath: { eq: "solution/multi-vendor-top-pc@2x.png" }
    ) {
      topImageD: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    topImageTFile: file(
      relativePath: { eq: "solution/multi-vendor-top-t@2x.png" }
    ) {
      topImageT: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    topImageMFile: file(
      relativePath: { eq: "solution/multi-vendor-top-m@3x.png" }
    ) {
      topImageM: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    featureImg1File: file(
      relativePath: { eq: "solution/feature-multi-vendor-pc-1@2x.png" }
    ) {
      featureImg1: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    featureImg2File: file(
      relativePath: { eq: "solution/feature-multi-vendor-pc-2@2x.png" }
    ) {
      featureImg2: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    featureImg3File: file(
      relativePath: { eq: "solution/feature-multi-vendor-pc-3@2x.png" }
    ) {
      featureImg3: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    driveSalesDFile: file(
      relativePath: {
        eq: "solution/textAndImageSection/multi-vendor-slide-pc-1@2x.png"
      }
    ) {
      driveSalesD: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    driveSalesTFile: file(
      relativePath: {
        eq: "solution/textAndImageSection/multi-vendor-slide-t-1@2x.png"
      }
    ) {
      driveSalesT: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    driveSalesMFile: file(
      relativePath: {
        eq: "solution/textAndImageSection/multi-vendor-slide-m-1@3x.png"
      }
    ) {
      driveSalesM: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    marketSmarterDFile: file(
      relativePath: {
        eq: "solution/textAndImageSection/multi-vendor-slide-pc-2@2x.png"
      }
    ) {
      marketSmarterD: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    marketSmarterTFile: file(
      relativePath: {
        eq: "solution/textAndImageSection/multi-vendor-slide-t-2@2x.png"
      }
    ) {
      marketSmarterT: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    marketSmarterMFile: file(
      relativePath: {
        eq: "solution/textAndImageSection/multi-vendor-slide-m-2@3x.png"
      }
    ) {
      marketSmarterM: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    buildCustomerDFile: file(
      relativePath: {
        eq: "solution/textAndImageSection/multi-vendor-slide-pc-3@2x.png"
      }
    ) {
      buildCustomerD: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    buildCustomerTFile: file(
      relativePath: {
        eq: "solution/textAndImageSection/multi-vendor-slide-t-3@2x.png"
      }
    ) {
      buildCustomerT: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    buildCustomerMFile: file(
      relativePath: {
        eq: "solution/textAndImageSection/multi-vendor-slide-m-3@3x.png"
      }
    ) {
      buildCustomerM: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
  }
`

export default MultiVendor
