import React, { useEffect } from "react"
import Layout from "../components/Layout"

import { gtagPluginEventTracker } from "../utils/googleAnalyticsHelper"
import {
  ENUM_EVENT_CATEGORIES,
  ENUM_EVENT_ACTIONS,
} from "../constants/enumEvents"

const PrivacyPolicy = () => {
  
  return (
    <Layout className="privacy-policy-page" isHeaderFixed>
      <div className="container content-wrapper">
        <h1 className="heading">Orderspot Privacy Policy</h1>
        <div className="w-richtext">
          <p>
            EFFECTIVE DATE: April 23, 2019
            <br />
          </p>
          <p>
            Venturist Incorporated (“OrderSpot”, “we”, “us” or “our”) values
            your privacy. &nbsp;In this Privacy Policy (“Policy”), we describe
            how we collect, use, and disclose information that we obtain about
            visitors to our website at www.orderspot.app (the “Site”) and the
            food, beverage and meal ordering services available through our
            web-based mobile menu and dashboard (“Platform”) and associated
            mobile application (“App”)(the App and Platform will be referred
            together as the “Services”), and how we use and disclose that
            information. &nbsp;
          </p>
          <p>
            By visiting the Site, or using any of our Services, you agree that
            your personal information will be handled as described in this
            Policy. Your use of our Site or Services, and any dispute over
            privacy, is subject to this Policy and the Terms of Use, including
            its applicable limitations on damages and provisions for the
            resolution of disputes.
          </p>
          <p>
            <strong>Description of Services</strong>
          </p>
          <p>
            Orderspot is a service that allows users to order food, beverages
            and meals from their favorite restaurants and eating establishments.
            &nbsp;In connection with this Service, we need to collect certain
            personal information from the both the customer purchasing the meal
            (“Customer”) and the restaurants, venues and vendors that are
            supplying the meals that Customers purchased (collectively,
            “Vendors”). &nbsp; Customers and Vendors will also be referred to as
            “you” and “your” in this Policy. This Policy describes the personal
            information collected from both Customers and Vendors.
          </p>
          <p>
            <strong>The Information We Collect About You</strong>
          </p>
          <p>
            We collect information about you directly from you and from third
            parties, as well as automatically through your use of our Site or
            Services. &nbsp;
          </p>
          <p>
            <strong>
              <em>Information We Collect Directly From You.</em>
            </strong>{" "}
            You may browse certain areas of the Site without registering with us
            or providing us personal information. &nbsp;If you register for our
            Services, then you must provide the following information:
          </p>
          <p>
            <strong>Customers</strong>: Customers must provide their name, email
            address and phone number when they download the App and register for
            an account. &nbsp;They must also provide credit card information to
            pay for meals that they order, which information is provided to our
            payment processor, Stripe, and will be handled in accordance with
            their privacy policy. &nbsp;We also track your order history.
            &nbsp;We use this information to provide the Services, issue and
            track orders that you place, contact you about your orders and offer
            you promotions along with our Vendors, and for other purposes
            described in this Policy.
          </p>
          <p>
            <strong>Vendors</strong>: Vendors must provide their business name,
            contact name, email address and phone number when they register for
            the Platform and download the App. &nbsp;They also provide banking
            information to facilitate the transfer of funds received from
            Customers less any fees due OrderSpot. &nbsp;We use this information
            to provide the Services, issue and track orders that are placed by
            Customers, contact you about orders and co-promotions along to
            Customers, and for other purposes described in this Policy.
          </p>
          <p>
            In addition, if you are providing personal information for third
            parties in connection with using our Services, you are responsible
            for ensuring that you have all required permissions and consents to
            provide such personal information to us for use in connection with
            the Services and that our use of such personal information to
            provide the Services does not violate any applicable law, rule,
            regulation or order.
          </p>
          <p>
            <strong>
              <em>Information We Collect Automatically.</em>
            </strong>{" "}
            We may automatically collect the following information about your
            use of our Site or Services through cookies and other technologies:
            your domain name; your browser type and operating system; web pages
            you view; links you click; your IP address; the length of time you
            visit our Site &nbsp;or use our Services; and the referring URL, or
            the webpage that led you to our Site. &nbsp;We may combine this
            information with other information that we have collected about you,
            including, where applicable, your user name, name, and other
            personal information &nbsp;Please see the section “Our Use of
            Cookies and Other Tracking Mechanisms” below for more information.
          </p>
          <p>
            <strong>
              <em>Geolocation.</em>
            </strong>{" "}
            &nbsp;If you have provided permission through your mobile device to
            allow us to collect location information through the App, we may
            obtain your physical location information in terms of latitude and
            longitude from technologies like GPS, Wi-Fi, or cell tower
            proximity. You are able to withdraw your permission for us to
            acquire such physical location information from your mobile device
            through your mobile device settings, although we do not control this
            process. If you have questions about how to disable your mobile
            device's location services, we recommend you contact your mobile
            device service provider or the mobile device manufacturer.
          </p>
          <p>
            <strong>How We Use Your Information</strong>
          </p>
          <p>
            In addition to the purposes described above, We use your
            information, including personal information, for the following
            general purposes:
          </p>
          <ul role="list">
            <li>
              <strong>Provide our Services </strong>
            </li>
          </ul>
          <p>
            We use your information to communicate with you about your use of
            our Site and Services, to respond to your inquiries, to fulfill,
            track and process orders, and for other customer service purposes.
          </p>
          <ul role="list">
            <li>
              <strong>Provide personalized services</strong>
            </li>
          </ul>
          <p>
            We use your information to tailor the content and information that
            we may send or display to you, to offer location customization, and
            personalized help and instructions, and to otherwise personalize
            your experiences while using the Site and Services.
          </p>
          <ul role="list">
            <li>
              <strong>Improve and develop our services</strong>
            </li>
          </ul>
          <p>
            We use your information to ensure our Site and Services are working
            as intended, to better understand how users access and use our Site
            and Services, both on an aggregated and individualized basis, to
            make improvements to our services, to develop new Services, and for
            other research and analytical purposes.
          </p>
          <ul role="list">
            <li>
              <strong>Offer promotions</strong>
            </li>
          </ul>
          <p>
            We use your information for marketing and promotional purposes. For
            example, we may use your information, such as your email address or
            phone number, to send you news and newsletters, special offers, and
            promotions, to conduct contests and sweepstakes, or to otherwise
            contact you about products or information we think may interest you
            (subject to your rights to opt-out of such communications as
            described below). &nbsp;Such promotions may be offered by OrderSpot
            or by one of the Vendors on our Platform. &nbsp;We also may use the
            information that we learn about you to assist us in advertising our
            Services on third party websites.
          </p>
          <p>
            <strong>How We Share Your Information</strong>
          </p>
          <p>
            We may share your information, including personal information, as
            follows:
          </p>
          <ul role="list">
            <li>
              <strong>
                <em>
                  Vendors. &nbsp;Consumers that place orders with Vendors on our
                  Platform understand and agree that their personal information
                  will be shared with those Vendors so that they can process and
                  provide meal orders. &nbsp;You also agree that these Vendors
                  may also use this information to send you marketing
                  communication and promotional offers either alone or together
                  with OrderSpot.
                </em>
              </strong>
            </li>
            <li>
              <strong>
                <em>
                  Consent. &nbsp;Where you have provided consent, we share your
                  information, including personal information, as described at
                  the time of consent, such as when you authorize a third party
                  application or website to access your OrderSpot account or
                  when you participate in promotional activities conducted by
                  OrderSpot partners or third parties.
                </em>
              </strong>
            </li>
            <li>
              <strong>
                <em>Affiliates</em>
              </strong>
              . We may disclose the information we collect from you to our
              affiliates or subsidiaries solely for the purpose of providing
              Services to you; however, if we do so, their use and disclosure of
              your personally identifiable information will be maintained by
              such affiliates and subsidiaries in accordance with this Policy.{" "}
            </li>
            <li>
              <strong>
                <em>Service Providers</em>
              </strong>
              . We may disclose the information we collect from you to third
              party vendors, service providers, contractors or agents who
              perform functions on our behalf.
            </li>
            <li>
              <strong>
                <em>Business Transfers</em>
              </strong>
              . If we are acquired by or merged with another company, if
              substantially all of our assets are transferred to another
              company, or as part of a bankruptcy proceeding, or are in
              negotiations for any of these types of transactions, we may
              transfer the information we have collected from you to the other
              company.
            </li>
            <li>
              <strong>
                <em>In Response to Legal Process</em>
              </strong>
              . We also may disclose the information we collect from you in
              order to comply with the law, a judicial proceeding, court order,
              or other legal process, such as in response to a subpoena.
            </li>
            <li>
              <strong>
                <em>To Protect Us and Others</em>
              </strong>
              . We also may disclose the information we collect from you where
              we believe it is necessary to investigate, prevent, or take action
              regarding illegal activities, suspected fraud, situations
              involving potential threats to the safety of any person,
              violations of our Terms of Use or this Policy, or as evidence in
              litigation in which we are involved.
            </li>
            <li>
              <strong>
                <em>Aggregate and De-Identified Information</em>
              </strong>
              . We may share aggregate or de-identified information about users
              and their use of the Services with third parties and publicly for
              marketing, advertising, research or similar purposes. &nbsp;
            </li>
          </ul>
          <p>
            <br />
          </p>
          <p>
            <strong>Our Use of Cookies and Other Tracking Mechanisms</strong>
          </p>
          <p>
            We and our service providers use cookies and other tracking
            mechanisms to track information about your use of our Site and
            Services. We may combine this information with other personal
            information we collect from you (and our third party service
            providers may do so on our behalf).
          </p>
          <p>
            Currently, our systems do not recognize browser “do-not-track”
            requests. You may, however, disable certain tracking as discussed in
            this section (e.g., by disabling cookies), but such disabling will
            impair use of the Site and Services.
          </p>
          <p>
            Cookies. &nbsp;Cookies are alphanumeric identifiers that we transfer
            to your computer’s hard drive through your web browser for
            record-keeping purposes. Some cookies allow us to make it easier for
            you to navigate our Site and Services, while others are used to
            enable a faster log-in process or to allow us to track your
            activities at our Site and Services. There are two types of cookies:
            session and persistent cookies.
          </p>
          <ul role="list">
            <li>
              Session Cookies. Session cookies exist only during an online
              session. They disappear from your computer when you close your
              browser or turn off your computer. We use session cookies to allow
              our systems to uniquely identify you during a session or while you
              are logged into the Site. This allows us to process your online
              transactions and requests and verify your identity, after you have
              logged in, as you move through our Site.
            </li>
            <li>
              Persistent Cookies. Persistent cookies remain on your computer
              after you have closed your browser or turned off your computer. We
              use persistent cookies to track aggregate and statistical
              information about user activity.
            </li>
          </ul>
          <p>
            <strong>
              <em>Disabling Cookies</em>
            </strong>
            . Most web browsers automatically accept cookies, but if you prefer,
            you can edit your browser options to block them in the future. The
            Help portion of the toolbar on most browsers will tell you how to
            prevent your computer from accepting new cookies, how to have the
            browser notify you when you receive a new cookie, or how to disable
            cookies altogether. Visitors to our Site who disable cookies will
            not be able to browse certain areas of the Site or use the Services.
          </p>
          <p>
            <strong>Third Party Analytics</strong>. We use automated devices and
            applications, such as HotJar, Google Analytics Mixpanel, Stripe to
            evaluate usage of our Site and our Services. We also may use other
            analytic means to evaluate our Services. We use these tools to help
            us improve our Services, performance and user experiences. These
            entities may use cookies and other tracking technologies to perform
            their services. We do not share your personal information with these
            third parties.
          </p>
          <p>
            <strong>Third-Party Links</strong>
          </p>
          <p>
            Our Site and Services may contain links to third-party websites,
            including Vendor sites. &nbsp;Any access to and use of such linked
            websites is not governed by this Policy, but instead is governed by
            the privacy policies of those third party websites. We are not
            responsible for the information practices of such third party
            websites.
          </p>
          <p>
            <strong>Security of My Personal Information</strong>
          </p>
          <p>
            We have implemented commercially reasonable precautions to protect
            the information we collect from loss, misuse, and unauthorized
            access, disclosure, alteration, and destruction. Please be aware
            that despite our efforts, no data security measures can guarantee
            100% security.
          </p>
          <p>
            You should take steps to protect against unauthorized access to your
            password, phone, and computer by, among other things, signing off
            after using a shared computer, choosing a robust password that
            nobody else knows or can easily guess, and keeping your log-in and
            password private. We are not responsible for any lost, stolen, or
            compromised passwords or for any activity on your account via
            unauthorized password activity.
          </p>
          <p>
            <strong>
              What Rights Do I Have Regarding My Personal Information
            </strong>
          </p>
          <p>
            You may request access, a copy, modification &nbsp;or deletion of
            personal information that you have submitted to us by contacting us
            at support@orderspot.co. &nbsp;We will use reasonable efforts to
            accommodate such requests to the extent required by law, provided
            that we may be required to retain personal information to comply
            with legal obligations, accounting requirements, or for other
            business purposes. &nbsp;We may request additional information to
            verify the identity of the requesting party before responding to a
            request. &nbsp;Please note that copies of information that you have
            updated, modified or deleted may remain viewable in cached and
            archived pages of the Site for a period of time.
          </p>
          <p>
            <strong>
              What Choices Do I Have Regarding Use of My Personal Information
              for Marketing?
            </strong>
          </p>
          <p>
            We may send periodic promotional or informational emails to you. You
            may opt-out of such communications by following the opt-out
            instructions contained in the e-mail. Please note that it may take
            up to 10 business days for us to process opt-out requests. If you
            opt-out of receiving emails about recommendations or other
            information we think may interest you, we may still send you e-mails
            about your account or any Services you have requested or received
            from us.
          </p>
          <p>
            <strong>
              What Choices Do I Have Regarding Use of My Personal Information
              for SMS Marketing?
            </strong>
          </p>
          <p>
            We may send periodic promotional or informational texts to you if
            you have opted in to receiving them. You may opt-out of such
            communications by following the opt-out instructions contained in
            the text. If you opt-out of receiving SMS notifications about
            recommendations or other information we think may interest you, we
            may still send you texts about your account or any Services you have
            requested or received from us.
          </p>
          <p>
            <strong>Location of Information</strong>
          </p>
          <p>
            <strong>
              Our Site and Services are offered from the United States. &nbsp;We
              store any information we collect in the United States. &nbsp; If
              you access the Services or Site from outside the United States,
              you agree to the transfer of your information to the United
              States, which may have less protections for your personal
              information than your jurisdiction of residence.
            </strong>
          </p>
          <p>
            <strong>Children Under 13</strong>
          </p>
          <p>
            Our Site and Services are not designed for children under 13.
            &nbsp;If we discover that a child under 13 has provided us with
            personal information, we will delete such information from our
            systems.
          </p>
          <p>
            <strong>Contact Us</strong>
          </p>
          <p>
            If you have questions about the privacy aspects of our Site or
            Services or would like to make a complaint, please contact us at
            support@orderspot.co.
          </p>
          <p>
            <strong>Changes to this Policy</strong>
          </p>
          <p>
            This Policy is current as of the Effective Date set forth above. We
            may change this Policy from time to time, so please be sure to check
            back periodically. We will post any changes to this Policy on the
            Site
            <strong>.</strong> If we make any changes to this Policy that
            materially affect our practices with regard to the personal
            information we have previously collected from you, we will endeavor
            to provide you with notice in advance of such change by highlighting
            the change on our Site or if you have an account with us, providing
            notice to the email address in your account (for this reason you
            should make sure to update your account information promptly if it
            changes.
          </p>
          <p>CALIFORNIA PRIVACY NOTICE</p>
          <p>
            In addition, under California Civil Code § 1798.83, California
            residents who have provided personal information to Orderspot may
            obtain information regarding Orderspot’s disclosures, if any, of
            personal information to third parties for third-party direct
            marketing purposes. Requests must be submitted to the following
            address: support@orderspot.co. Within 30 days of receiving such a
            request, we will provide a California Privacy Disclosure, which will
            include a list of certain categories of personal information
            disclosed during the preceding calendar year to third parties for
            their direct marketing purposes, along with the names and addresses
            of the third parties. This request may be made no more than once per
            calendar year.
          </p>
        </div>
      </div>
    </Layout>
  )
}

export default PrivacyPolicy
