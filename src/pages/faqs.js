import React, { useEffect } from "react"
import { matchSorter } from "match-sorter"
import Layout from "../components/Layout"
import PageHeader from "../components/common/PageHeader/PageHeader"
import HeaderTextBox from "../components/FaqsPage/HeaderTextBox"
import FaqsSection from "../components/FaqsPage/FaqsSection"
import QuestionSection from "../components/FaqsPage/QuestionSection"
import { scroller } from "react-scroll"

import { gtagPluginEventTracker } from "../utils/googleAnalyticsHelper"
import {
  ENUM_EVENT_CATEGORIES,
  ENUM_EVENT_ACTIONS,
} from "../constants/enumEvents"
import { graphql } from "gatsby"
import FaqsSearchResult from "../components/FaqsPage/FaqsSearchResult"
import { faqs } from "../components/FaqsPage/faqsData"

const Faqs = ({ data }) => {
  const [searchTerm, setSearchTerm] = React.useState("")
  const [searchedFaqs, setSearchedFaqs] = React.useState([])

  useEffect(() => {
    const filteredFaqs = matchSorter(faqs, searchTerm, { keys: ["title"] })
    setSearchedFaqs(filteredFaqs)
  }, [searchTerm])

  const blogBg = data.faqBgFile.faqBg.fluid
  const noSearchResultFaceImg = data.faqNoSearchFile.faqNoSearch.fluid

  const getSearchTerm = e => {
    if (e.target.value === "") setSearchTerm(e.target.value)
  }

  const handleSubmit = valueObj => {
    setSearchTerm(valueObj.searchTerm)
    scroller.scrollTo("search-result", {
      duration: 500,
      delay: 100,
      smooth: true,
      offset: -78,
    })
  }

  return (
    <Layout className="faqs-page" isHeaderFixed={true}>
      <PageHeader removeBackdrop headerBgImg={blogBg} backgroundColor={"#fff"}>
        <HeaderTextBox
          handleSubmit={handleSubmit}
          getSearchTerm={getSearchTerm}
        />
      </PageHeader>
      {searchTerm ? (
        <FaqsSearchResult
          searchTerm={searchTerm}
          noSearchResultFaceImg={noSearchResultFaceImg}
          searchedFaqs={searchedFaqs}
        />
      ) : (
        <FaqsSection />
      )}
      <QuestionSection />
    </Layout>
  )
}

export const query = graphql`
  query {
    faqBgFile: file(relativePath: { eq: "temp/faqs-header-bg-img.png" }) {
      faqBg: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
    faqNoSearchFile: file(relativePath: { eq: "not-found-face.png" }) {
      faqNoSearch: childImageSharp {
        fluid {
          ...GatsbyImageSharpFluid
        }
      }
    }
  }
`

Faqs.propTypes = {}

export default Faqs
