export const getStaySeconds = (enterDate, leaveDate) => {
  return Math.round((leaveDate.getTime() - enterDate.getTime()) / 1000)
}

export const gtagPluginEventTracker = ({ category, action, label }) => {
  if (process.env.IS_GA_RUNNING == "false") return
  let payload = {}
  if (category !== undefined) payload.event_category = category
  if (label !== undefined) payload.event_label = label
  window.gtag("event", action, payload)
}

export const googleAdsEventConversion = () => {
  if (process.env.IS_GA_RUNNING == "false") return

  var callback = function (url) {
    // if (typeof url != "undefined") {
    //   window.location = url
    // }
    console.log("google Ads conversion success")
  }
  window.gtag("event", "conversion", {
    send_to: "AW-666974500/Slo6CL23yvIBEKTyhL4C",
    value: 1.0,
    currency: "USD",
    event_callback: callback,
  })
  return false
}
