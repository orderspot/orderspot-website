import styled from "styled-components"

const ResponsiveStyledComponent = ({ elem, common, mobile, tablet, pc }) => {
  return styled(elem)`
    ${common}
    @media screen and (max-width: 767px) {
      ${mobile}
    }
    @media screen and (min-width: 768px) and (max-width: 1199px) {
      ${tablet}
    }
    @media screen and (min-width: 1200px) {
      ${pc}
    }
  `
}

export default ResponsiveStyledComponent
