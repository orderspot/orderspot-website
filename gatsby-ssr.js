import React from "react"
export const onRenderBody = (
  { setHeadComponents, setPostBodyComponents },
  pluginOptions
) => {
  setHeadComponents([
    <script
      key="Anti-flickeringSnippet"
      dangerouslySetInnerHTML={{
        __html: `
        (function(a,s,y,n,c,h,i,d,e){s.className+=' '+y;h.start=1*new Date;
        h.end=i=function(){s.className=s.className.replace(RegExp(' ?'+y),'')};
        (a[n]=a[n]||[]).hide=h;setTimeout(function(){i();h.end=null},c);h.timeout=c;
        })(window,document.documentElement,'async-hide','dataLayer',5500,
        {'OPT-K48B722':true});`,
      }}
    />,
    <script
      key="intercom_initial"
      dangerouslySetInnerHTML={{
        __html: `
          window.intercomSettings = {
            app_id: "lczo6s6t"
          };`,
      }}
    />,
    <script
      key="intercom_settings"
      dangerouslySetInnerHTML={{
        __html: `
        // We pre-filled your app ID in the widget URL: 'https://widget.intercom.io/widget/lczo6s6t'
        (function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',w.intercomSettings);}else{var d=document;var i=function(){i.c(arguments);};i.q=[];i.c=function(args){i.q.push(args);};w.Intercom=i;var l=function(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://widget.intercom.io/widget/lczo6s6t';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);};if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();
      `,
      }}
    />,
    <script
      key="formstack-script"
      dangerouslySetInnerHTML={{
        __html: `
        (function(t,e,n,r,a){var c,i=e[a]=e[a]||{init:function(t){function e(t){
      i[t]=function(){i.q.push([t,[].slice.call(arguments,0)])}}var n,r;i.q=[],
            n="addListener track".split(" ");for(r in n)e(n[r]);
      i.q.push(["init",[t||{}]])}},s=t.createElement(r);s.async=1,s.src=n,
          c=t.getElementsByTagName(r)[0],c.parentNode.insertBefore(s,c)
  })(document,window,"https://analytics.formstack.com/js/fsa.js","script","FSATracker");
      `,
      }}
    />,
    <script
      key="formstack-script-init"
      dangerouslySetInnerHTML={{
        __html: `
         (function(){
         window.FSATracker.init({"account":"975033", "endpoint":"https://analytics.formstack.com"});
  })();
      `,
      }}
    />,
    <script
      key="gAdConversionFunction"
      dangerouslySetInnerHTML={{
        __html: `function gtag_report_conversion(url) { var callback = function () { if (typeof(url) != 'undefined') { window.location = url; } }; gtag('event', 'conversion', { 'send_to': 'AW-666974500/Slo6CL23yvIBEKTyhL4C', 'value': 1.0, 'currency': 'USD', 'event_callback': callback }); return false; }`,
      }}
    />,
  ])
}