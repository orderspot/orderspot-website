import React from "react"
import { Provider as StoreProvider } from "./src/context/StoreContext"
import { DialogProvider } from "./src/context/DialogContext"
import { AlertDialogProvider } from "./src/context/AlertDialogContext"
import "./src/styles/global.scss"

export const wrapRootElement = ({ element, props }) => (
  <StoreProvider>
    <AlertDialogProvider>
      <DialogProvider>{element}</DialogProvider>
    </AlertDialogProvider>
  </StoreProvider>
)
