const path = require("path")

exports.createSchemaCustomization = ({ actions }) => {
  const { createTypes } = actions
  const typeDefs = `
    type ContentfulBlogpost implements Node {
      excerpt: String
    }
  `
  createTypes(typeDefs)
}


exports.createPages = async ({ graphql, actions }) => {
  const { createPage } = actions
  const response = await graphql(`
    query {
      allContentfulBlogpost {
        edges {
          node {
            slug
          }
        }
      }
    }
  `)
  response.data.allContentfulBlogpost.edges.forEach(edge => {
    createPage({
      path: `/blog/${edge.node.slug}`,
      component: path.resolve("./src/templates/dynamic-blog-page.js"),
      context: {
        slug: edge.node.slug
      }
    })
  })

//  Category blogs pages
  const categoryBlogsPages = await graphql(`
    query {
      allContentfulAllCategories {
    edges {
      node {
        slug
        categoryTitle
        categoryText
      }
    }
  }
  allContentfulBlogpost {
        edges {
          node {
            category {
             slug
            }
            title
            excerpt
            slug
            readTime
            publishDate
            subTitleBlack
           featuredImage {
            fluid {
            base64
            sizes
            aspectRatio
            srcSet
            src
          }
          }
          }
        }
      }
    }
  `)
  
  categoryBlogsPages.data.allContentfulAllCategories.edges.forEach(edge => {
    createPage({
      path: `/category/${edge.node.slug}`,
      component: path.resolve("./src/templates/dynamic-category-blog-page.js"),
      context: {
        slug: edge.node.slug,
        categoryTitle: edge.node.categoryTitle,
        categoryText: edge.node.categoryText,
        blogs: categoryBlogsPages.data.allContentfulBlogpost.edges
      }
    })
  })
}
